"""
FCS Common C4D Library Module
-----------------------------------------------------------------
Global Methods Functions Operations and Enums Types 
and Plugin Directory Folder & Folders
-----------------------------------------------------------------

Global Custom GUI's Utils Functions for Dialogs UI Elements, Tools Functions and etc...

These are c4d custom gui elements. They are ui elements for your dialogs.
Instead of repeating and building the same UI element in each dialog class 
as a function and then add to the c4d CreateLayout function, and having 
long lines of code, and getting lost with in code.
Since these custom gui elements are on they own and out these dialog classes.
They are basicaly instances when they get added to the UI with their attinbes 
that hold's the data that you are passing to it.
eg(ui_instance , func_instance are self)
"""
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
from os import urandom
import weakref
# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.utils import GeRayCollider
from c4d.plugins import CommandData, TagData, ObjectData
# Regex Import
import re
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json
# Python Plugin System Load Modules Dynamically With importlib.
#import importlib

from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib


# FCS Imports Modules .py Files.
#import fcs_json_sys_utils as jsonEditSystem
#import fcs_logger_sys_utils as fcslogSystem

#jsonEdit = jsonEditSystem.FCS_JsonSystem_Editor()
#fs_lib = fs_main_lib
c4dm = c4d
DebugMode = None
plugDIR = None
fcsLog = None
DirHelper = DirectoryHelperLib.DirectoryHepler()


#  Date System
DateT = str(datetime.date.today())


def ResPathInitalizePatcher(res, libpath):
    """
    Passing Plugin Res Dir to this module .py file
    for PLUGIN_DIR() class. 
    """
    global plugDIR
    plugDIR = PLUGIN_DIR(res, libpath)
    return True

# ---------------------------------------------------------------
#               FCS Plugin Directory Folder & Folders
# ---------------------------------------------------------------

# ---------------------------------------------------------------------
#       Colors, Enums, C4D Types, C4D Id Types and GUI Enums
# ---------------------------------------------------------------------
"""
NOTE!!!
To get the colors, there is a tool call (Cinema 4D Vector Color Picker)
created by Crewell Gould from FieldCreatorsStudios.
"""
# UI BG COLORS Tpyes
BG_C4D_DEFAULT_COL = c4d.COLOR_BG
BG_C4D_Trans_COL = c4d.COLOR_TRANS
BG_BLACK_COL = c4d.Vector(0, 0, 0)
BG_RED_COL = c4d.Vector(0.71875, 0, 0)
BG_LightDarker_COL = c4d.Vector(0.125, 0.125, 0.125)
BG_GREEN_COL = c4d.Vector(0.00390625, 0.5859375, 0.125)
BG_DARK = c4d.Vector(0.1015625, 0.09765625, 0.10546875)
BG_DEEP_DARKER = c4d.Vector(0.0625, 0.05859375, 0.0625)
BG_DARKER_COL = c4d.Vector(0.1484375, 0.1484375, 0.1484375)
BG_LitDarkBlue_Col = c4d.Vector(0.08203125, 0.1015625, 0.12109375)
BG_FCS_ORANGE_COL = c4d.Vector(0.99609375, 0.26953125, 0)
DARK_BLUE_TEXT_COL = c4d.Vector(0, 0.78125, 0.99609375)
DARK_RED_TEXT_COL = c4d.Vector(0.99609375, 0, 0)
BG_PUPLE_COL = c4d.Vector(0, 0.015625, 0.99609375)

# The UI Window Dialog Tpyes
DLG_TYPE1 = c4d.DLG_TYPE_ASYNC                          # 	Asynchronous dialog.
DLG_TYPE2 = c4d.DLG_TYPE_ASYNC_POPUPEDIT                #   Asynchronous dialog: popup dialog style (no menu bar, no window frame).
DLG_TYPE3 = c4d.DLG_TYPE_ASYNC_POPUP_RESIZEABLE         #   Asynchronous dialog: resizable popup dialog style (no menu bar).
DLG_TYPE4 = c4d.DLG_TYPE_MODAL                          #   Modal dialog.
DLG_TYPE5 = c4d.DLG_TYPE_MODAL_RESIZEABLE               #   Resizable modal dialog.
DLG_TYPE6 = c4d.DLG_TYPE_ASYNC_FULLSCREEN_WORK          #   Asynchronous dialog: fullscreen over desktop area.
DLG_TYPE7 = c4d.DLG_TYPE_ASYNC_FULLSCREEN_MONITOR       #   Asynchronous dialog: fullscreen over the whole monitor area.

# Get Cinema 4D Material
class C4dMat(object):
    """ Get C4D All Material Channels Types """

    def m_color(self, userMat):
        """ Get MATERIAL_COLOR_SHADER """
        if userMat[c4d.MATERIAL_USE_COLOR] == False:
            return
        else:
            return {'str':"Color", 'shader':userMat[c4d.MATERIAL_COLOR_SHADER]}
        return

    def m_alpha(self, userMat):
        """ Get MATERIAL_COLOR_SHADER """
        if userMat[c4d.MATERIAL_USE_ALPHA] == False:
            return
        else:
            return {'str':"Alpha", 'shader':userMat[c4d.MATERIAL_ALPHA_SHADER]}
        return 
    
    def m_diffusion(self, userMat):
        """ Get MATERIAL_COLOR_SHADER """
        if userMat[c4d.MATERIAL_USE_DIFFUSION] == False:
            return
        else:            
            return {'str':"Diffusion", 'shader':userMat[c4d.MATERIAL_DIFFUSION_SHADER]}
        return 

    def m_lum(self, userMat):
        """ Get MATERIAL_COLOR_SHADER """
        if userMat[c4d.MATERIAL_USE_LUMINANCE] == False: 
            return
        else:            
            return {'str':"Luminance", 'shader':userMat[c4d.MATERIAL_LUMINANCE_SHADER]}
        return

    def m_norm(self, userMat):
        """ Get MATERIAL_COLOR_SHADER """
        if userMat[c4d.MATERIAL_USE_NORMAL] == False:
            return
        else:
            return {'str':"Normal", 'shader':userMat[c4d.MATERIAL_NORMAL_SHADER]}
        return

    def m_trans(self, userMat):
        """ Get c4d.MATERIAL_TRANSPARENCY_SHADER """
        if userMat[c4d.MATERIAL_USE_TRANSPARENCY] == False:
            return
        else:
            return {'str':"Transparency", 'shader':userMat[c4d.MATERIAL_TRANSPARENCY_SHADER]}
        return

    def m_enviro(self, userMat):
        """ Get c4d.MATERIAL_ENVIRONMENT_SHADER """
        if userMat[c4d.MATERIAL_USE_ENVIRONMENT] == False:
            return
        else:
            return {'str':"Environment", 'shader':userMat[c4d.MATERIAL_ENVIRONMENT_SHADER]}
        return

    def m_bump(self, userMat):
        """ Get c4d.MATERIAL_BUMP_SHADER """
        if userMat[c4d.MATERIAL_USE_BUMP] == False:
            return
        else:            
            return {'str':"Bump", 'shader':userMat[c4d.MATERIAL_BUMP_SHADER]}
        return

    def m_spec(self, userMat):
        """ Get c4d.REFLECTION_LAYER_COLOR_TEXTURE """
        if userMat[c4d.MATERIAL_USE_REFLECTION] == False:
            return
        else:            
            # To Get Reflection or Spec map texture from the Reflectance Channel of the material.
            m_spec = userMat.GetReflectionLayerCount()
            for each_layer in xrange(0, m_spec):  
                layer = userMat.GetReflectionLayerIndex(each_layer)
                get_layer_col = userMat[layer.GetDataID() + c4d.REFLECTION_LAYER_COLOR_TEXTURE]
                return {'str':"Reflectance", 'shader':get_layer_col}   
        return

    def GetAllC4DMatChannels(self, userMat):
        # Get Material Shaders Texture Paths.
        c4dMatChannels = [  
                            self.m_color(userMat), 
                            self.m_alpha(userMat), 
                            self.m_diffusion(userMat), 
                            self.m_lum(userMat),
                            self.m_trans(userMat), 
                            self.m_enviro(userMat), 
                            self.m_norm(userMat), 
                            self.m_spec(userMat), 
                            self.m_bump(userMat) 
                         ]
        return c4dMatChannels


# ---------------------------------------------------------------------
#       Creating GUI Instance Functions UI Elements Operations 
#                          Hepler Methods. 
# ---------------------------------------------------------------------
# Create a UI Window Dialog GUI Base Class
class BaseWindowDialogUI(c4d.gui.GeDialog):
    """ The Base Window GUI Dialog Class """

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Base Window"
    windowDialogWidthSize = None
    windowDialogHeightSize = None
    windowDialogPluginID = 0

    def BuildUI(self):
        """
        Creating Window UI Layout Sturcture.
        """
        ui = self
        pass

    def UIsettings(self):
        """ To set UI elements values for when the window is initialized. """
        ui = self
        pass

    def UIfunctions(self, id):
        """ Excuting Commands for UI Elements Functions. """
        ui = self
        pass

    def openWindowUI(self, winDlgType):
        """ To Open the UI Dialog Window. """
        sWidth = self.windowDialogWidthSize 
        sHeight = self.windowDialogHeightSize   
        # xpos=-3, ypos=-3       
        return self.Open(dlgtype=winDlgType, pluginid=self.windowDialogPluginID, xpos=-2, ypos=-2, defaultw=sWidth, defaulth=sHeight, subid=0)
    
    def restoreWindowUI(self, sec_ref):
        """ To Restore the UI window. """
        return self.Restore(pluginid=self.windowDialogPluginID, secret=sec_ref)

    def closeWindowUI(self):
        """ To CLOSE the UI window. """
        return self.Close()

    def IfWindowDialogClose(self):
        
        return True

    """
    Main C4D GeDialog GUI Class Overrides.
    _________________________________________________
    """
    def __init__(self):
        """
        The __init__ is an Constuctor and help get or set data
        into the class and passes data on from the another class.
        """
        super(BaseWindowDialogUI, self).__init__()


    def CreateLayout(self):
        """
        Window GUI elements layout that display to the User.
        """
        self.BuildUI()
        return True

    def InitValues(self):
        """
        Called when the dialog is initialized by the GUI / GUI's startup values basically.
        """
        #fcsLog.DEBUG("F.C.S"+ self.windowMainTileName +" is Open Now!", False, DebugMode)
        self.SetTitle(self.windowMainTileName)
        self.UIsettings()
        return True

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id:
            fcsLog.INFO(str(id), False, DebugMode)        
        self.UIfunctions(id)
        return True

    def CoreMessage(self, id, msg):
        """
        Override this function if you want to react to Cinema 4D core messages.
        The original message is stored in msg
        """
        #if id == c4d.EVMSG_CHANGE:
        #    pass
        #self.UI_COREMESSAGES
        return True

    def DestroyWindow(self):
        """
        DestroyWindow Override this method - this function is called when the dialog is
        about to be closed temporarily, for example for layout switching.
        """
        self.IfWindowDialogClose()
        print( self.windowMainTileName + " Window Dialog Close.")
        pass

class CustomImageButtonGUI(object):
    """
    Create a Bitmap Button Custom GUI.

    `Code Example:`

    self.ImgButton = CustomImageButtonGUI(``self``)

    self.ImgButton.GUI(id=``1001``, ``**key_properties``)

    ` In Code : `

    self.ImgButton.GUI( ``[1001]``,  \<--------------------------------------| UI Widget Button [ID]
    
                        borderStyle = ``c4d.BORDER_IN`` ,   \<---------------|
                        icon =``IconPath`` ,                                \|**kwargs = **key_properties
                        backgroundCol = ``c4d.COLOR_TEXTFOCUS`` ,           \|These are key button properties that
                        clickable = ``True`` ,                              \|is for what you want the button 
                        tip = ``"<b>HelloWorld</b>/nHi"`` ,                 \|to have. Basically customize the 
                        toggleable = ``True``               \<---------------|the way you want it to be.
                       )
    """
    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get
        and passes data on from the another class also.
        """
        self.ui_instance = ui_instance
        super(CustomImageButtonGUI, self).__init__()

    # Bitmap Button Custom GUI.
    def GUI(self, ID, **key_properties):

        ui = self.ui_instance

        # Create GUI Button.
        CustomGui_UI = c4d.BaseContainer()                                  # Create a new container to store the button image.

        CustomGui_UI.SetFilename(ID, key_properties['icon'])                # Add this location info to the conatiner

        CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, c4d.BORDER_NONE) 

        CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, True)

        for setting, value in key_properties.items():                       # Adding settings from the dic that item setting is there in or added to it.

            if setting == 'borderStyle':
                BorderLook =  key_properties['borderStyle']
                CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, BorderLook)   # Sets the border flag to look like a button look. eg.( c4d.BORDER_NONE or c4d.BORDER_THIN_OUT )

            elif setting == 'toggleable':
                # ToggleBTN = btn_settings['toggleable']
                CustomGui_UI.SetBool(c4d.BITMAPBUTTON_TOGGLE, True)         # Toggle button, like a checkbox.

            elif setting == 'clickable':
                CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, key_properties['clickable'])         # Clickable button.

            elif setting == 'backgroundCol':
                AddBackgroundColor = key_properties['backgroundCol']
                CustomGui_UI.SetInt32(c4d.BITMAPBUTTON_BACKCOLOR, AddBackgroundColor) # int - Background color.

            elif setting == 'tip':
                TipInfo = key_properties['tip']
                if TipInfo == "":
                    return
                else:
                    CustomGui_UI.SetString(c4d.BITMAPBUTTON_TOOLTIP, TipInfo) # Add tooltios.path. eg.[self.SetString(c4d.BITMAPBUTTON_TOOLTIP, "<b>Bold Text</b><br>New line")]

        ui.SetBTN = ui.AddCustomGui(ID, c4d.CUSTOMGUI_BITMAPBUTTON, "", c4d.BFH_MASK, 0, 0, CustomGui_UI)
        ui.SetBTN.SetImage(key_properties['icon'], True)
        return True

# Create a Edit Text Field Filename Path Custom GUI.
class AddCustomFileNamePath_GUI(object):
    """ 
    Add a Custom FileName Path GUI Layout 
    
    
    Code Example:
    
    self.CUSTOM_FILENAME = g_lib.AddCustomFileName_GUI(self, res)
    
    
    self.CUSTOM_FILENAME.GUI(string_id=id, 
                             btn_id=id, 
                             size_w=190, 
                             size_l=10, 
                             custom_btn=None)
                             
    self.CUSTOM_FILENAME.SetStringData(string_id=id, 
                                       ld_title="JsonImages",
                                       ld_type=c4d.FILESELECTTYPE_IMAGES,
                                       ld_flags=c4d.FILESELECT_LOAD, 
                                       suffix=".json"
                                       mgs="Invaild File!\n File needs to be Json.")
                                                               
    self.CUSTOM_FILENAME.GetStringData(string_id=id)
                                    
    """
    
    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance
  
    def GUI(self, string_id, btn_id, size_w, size_l, custom_icon_btn):
        """ GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.AddEditText(string_id, c4d.BFH_SCALEFIT, size_w, size_l)
        if custom_icon_btn:
            Add_Image_Button(ui_instance=self.instance, button_id=btn_id, btn_settings=custom_icon_btn)
        else:
            self.instance.AddButton(btn_id, c4d.BFH_MASK, 0, size_l, "...")
        self.instance.GroupEnd()
        return True
    
    def ClearFileName(self, string_id):
        self.instance.SetString(string_id, "")
        return True

    def VaildFileName(self, string_id, userpath, suffix, mgs):

        data = None

        if suffix == "":

            data = userpath

        else:

            vaild_file = os.path.splitext(userpath)

            if vaild_file[1] != suffix:
                fcsLog.WARNING(mgs, True, DebugMode)
                self.ClearFileName(string_id)
                data = ""

            elif vaild_file[1] == suffix:
                data = userpath

        return data

    def VaildFileLocation(self, file_path):

        if os.path.exists(file_path):
            return True
        return False

    def GetStringData(self, string_id):
        """ Get from custom filename Input string data. """
        data = self.instance.GetString(string_id)
        return data

    def SetStringData(self, string_id, ld_title, ld_type, ld_flags, suffix, mgs):

        userpath = c4d.storage.LoadDialog(type=ld_type, 
                                          title=ld_title, 
                                          flags=ld_flags, 
                                          force_suffix=suffix )

        self.instance.SetString( string_id, self.VaildFileName(string_id, userpath, suffix, mgs) )

        return True                 

# Create a Edit Text Field Filename Path Custom GUI.
class AddCustomDirPath_GUI(object):
    """ 
    Add a Custom DIRECTORY Path GUI Layout 
    
    
    Code Example:
    
    self.CUSTOM_DIRPATH = g_lib.AddCustomFileName_GUI(self, res)
    
    
    self.CUSTOM_DIRPATH.GUI(string_id=id, 
                             btn_id=id, 
                             size_w=190, 
                             size_l=10, 
                             custom_btn=None)
                             
    self.CUSTOM_DIRPATH.SetStringData(string_id=id, 
                                       ld_title="App Path Name",
                                       ld_type=c4d.FILESELECTTYPE_ANYTHING)
                                                               
    self.CUSTOM_DIRPATH.GetStringData(string_id=id)
                                    
    """
    
    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance
  
    def GUI(self, string_id, btn_id, size_w, size_l, custom_icon_btn):
        """ GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.AddEditText(string_id, c4d.BFH_SCALEFIT, size_w, size_l)
        if custom_icon_btn:
            Add_Image_Button(ui_instance=self.instance, button_id=btn_id, btn_settings=custom_icon_btn)
        else:
            self.instance.AddButton(btn_id, c4d.BFH_MASK, 0, size_l, "...")
        self.instance.GroupEnd()
        return True
    
    def ClearFileName(self, string_id):
        self.instance.SetString(string_id, "")
        return True

    def GetStringData(self, string_id):
        """ Get from custom filename Input string data. """
        data = self.instance.GetString(string_id)
        return data

    def SetStringData(self, string_id, ld_title, ld_type):

        userpath = c4d.storage.LoadDialog(type=ld_type, 
                                          title=ld_title, 
                                          flags=c4d.FILESELECT_LOAD) #c4d.FILESELECT_DIRECTORY)

        self.instance.SetString( string_id, userpath)

        return True                 


class CustomFolderViewGUI(object):

    LIST_DIRECTORIES = []


    def __init__(self, ui_instance, ID):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance
        self.ViewID = ID

    def WidgetUiGenertorID(self, num):
        """ Generating Ids """
        for i in range(num):
            yield i+1000

    def CustomFolderLayoutGUI(self, stateId, groupId, item):
        ui = self.instance

        ui.GroupBegin(groupId, c4d.BFH_SCALEFIT, 2, 0, "")
        # Set Background Color of the Group.
        ui.SetDefaultColor(groupId, c4d.COLOR_BG, cLib.BG_DARK)

        ui.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")

        # The Folder Status Retangle State Color
        ui.GroupBegin(stateId, c4d.BFH_MASK, 1, 0, "")
        ui.SetDefaultColor(stateId, c4d.COLOR_BG, cLib.DARK_BLUE_TEXT_COL)
        # Note!!! Only using this Test widget to make the space in the group.
        ui.AddStaticText(0, c4d.BFH_CENTER, 5, 15, "", c4d.BORDER_WITH_TITLE_BOLD)
        ui.GroupEnd()

        # The Display Text Folder Name 
        ui.AddStaticText(0, c4d.BFH_SCALEFIT, 200, 15, item['FolderNAME'], c4d.BORDER_WITH_TITLE_BOLD)

        ui.GroupEnd()
 
        # The Folder Icon and Edit Hamburg Menu UI Icon
        ui.GroupBegin(0, c4d.BFH_RIGHT, 0, 1, "")
        btnSettings = {'btn_look':c4d.BORDER_NONE, 'clickable':True, 'tip':item['PATH'], 'icon':cLib.GetCustomImageIcon(cLib.LoadImageIcon('fsSdkIcons'), 22, 7, 79)}
        cLib.Add_Image_Button(ui, item['FolderID'], btnSettings)
        btnSettings2 = {'btn_look':c4d.BORDER_NONE, 'clickable':True, 'tip':item['PATH'], 'icon':cLib.GetCustomImageIcon(cLib.LoadImageIcon('fsSdkIcons'), 22, 43, 79)}
        cLib.Add_Image_Button(ui, 101, btnSettings2)    
        ui.GroupEnd()

        return ui.GroupEnd()

    def ViewGUI(self, wSize, lSize):
        """ Custom Folder Layout View GUI """
        ui = self.instance
        ui.ScrollGroupBegin(0, c4d.BFH_SCALEFIT, c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT, wSize, lSize) #View Scroll Group
        ui.GroupBorderNoTitle(c4d.BORDER_IN)

        ui.GroupBegin(self.ViewID, c4d.BFV_TOP|c4d.BFH_SCALEFIT, 1, 0, "") # View Group 
        ui.GroupEnd() # View GroupEnd

        ui.GroupEnd() # View Scroll GroupEnd    
        return True 

    def FolderViewDataUpdater(self, ItemsData):
        ui = self.instance
        ui.LayoutFlushGroup(self.ViewID) # Refresh Group UI

        getAmount = len(ItemsData)*2

        ItemsAmount = getAmount*getAmount*getAmount*getAmount
        print(ItemsAmount)

        IDs = self.WidgetUiGenertorID(ItemsAmount)

        for i in ItemsData:

            BaseUIIDs = { 'BaseID1':next(IDs)*1, 'BaseID2':next(IDs)*2, 'BaseID3':next(IDs)*3, 'BaseID4':next(IDs)*4 }


            item = {'FolderNAME':i['FolderNAME'], 'FolderID':BaseUIIDs['BaseID3'], 'PATH':i['PATH']}

            self.LIST_DIRECTORIES.append( McxTexPathData(i['FolderNAME'], BaseUIIDs['BaseID3'], i['PATH']) )

            self.CustomFolderLayoutGUI(stateId=BaseUIIDs['BaseID1'], groupId=BaseUIIDs['BaseID2'], item=item)

        ui.LayoutChanged(self.ViewID) # Update Group UI
        return True         

    def OpenFolder(self, widgetId):
        UI_FOLDER_BTN_ID = self.LIST_DIRECTORIES.get(str(widgetId))
        if UI_FOLDER_BTN_ID:
            if widgetId == int(UI_FOLDER_BTN_ID['FolderID']):
                print( "You Just Open {0} Folder".format(UI_FOLDER_BTN_ID['FolderNAME']))
                cLib.OpenDataFolder(UI_FOLDER_BTN_ID['PATH'])
            else:
                pass     
        return True

class CustomUiWidgetPropertyGetandSet(object):
    """ [``Get``] and [``Set``] UI Widget Parameters (WP/wp) Manager """

    def __init__(self, ui_ins):
        self.widgetData = ui_ins

    def set(self, UiId, UiWidgetInitValue=None, UiWidgetData=None):
        """ [``Set``] UI Widget to Value or else Reset to InitValue(IV)/(UiWidgetInitValue) that is given."""

        if UiId['WT'] == "Bool":

            def BoolCheckerHelper(data):
                value = None
                if ast.literal_eval(data):
                    value = True
                else:
                    value = False
                return value

            if UiWidgetData: 
                self.widgetData.SetBool(int(UiId['ID']), BoolCheckerHelper(UiWidgetData))
            else:
                self.widgetData.SetBool(int(UiId['ID']), UiWidgetInitValue)

        if UiId['WT'] == "String":
            if UiWidgetData:
                self.widgetData.SetString(UiId['ID'], UiWidgetData)
            elif UiWidgetData == None:
                getData = self.widgetData.GetString(UiId['ID'])
                self.widgetData.SetString(UiId['ID'], getData)           
            else:
                self.widgetData.SetString(UiId['ID'], UiWidgetInitValue)

        if UiId['WT'] == "Long":
            if UiWidgetData: 
                self.widgetData.SetLong(UiId['ID'], UiWidgetData)
            else:
                self.widgetData.SetLong(UiId['ID'], UiWidgetInitValue)

        if UiId['WT'] == "Int32":
            if UiWidgetData: 
                self.widgetData.SetInt32(UiId['ID'], UiWidgetData)
            else:
                self.widgetData.SetInt32(UiId['ID'], UiWidgetInitValue)

        if UiId['WT'] == "Float":
            if UiWidgetData: 
                self.widgetData.SetFloat(UiId['ID'], UiWidgetData)
            else:
                self.widgetData.SetFloat(UiId['ID'], UiWidgetInitValue)

        if UiId['WT'] == "Vector":
            if UiWidgetData:
                param = UiWidgetData.split(",")
                ConvertToC4dVector = c4d.Vector( float(param[0]), float(param[1]), float(param[2]) ) 
                self.widgetData.SetVector(UiId['ID'], ConvertToC4dVector)
            else:
                self.widgetData.SetVector(UiId['ID'], UiWidgetInitValue)  

        return True

    def get(self, UiId):
        """ [``Get``] UI Widget Value and Return Value """
        
        GetData = None

        if UiId['WT'] == "Bool":
            GetData = self.widgetData.GetBool(UiId['ID']) 

        if UiId['WT'] == "String":
            GetData = self.widgetData.GetString(UiId['ID'])

        return GetData


# Create ProgressBar GUI
class AddCustomProgressBar_GUI(object):
    """
    # Progress Bar GUI 

    # Add a Custom Progress Bar GUI Layout 

    # Code Example:
    
    self.CUSTOM_PROGRESSBAR = AddCustomProgressBar_GUI(ui_instance=self)
    
    # Note: If you dont want a string amount UI display then set (str_amount_id=None).
    self.CUSTOM_PROGRESSBAR.GUI(str_amount_id=id, 
                                progress_id=id, 
                                size_w=190, 
                                size_l=10 )

    # Note: If you dont want a custom color then set (col=None).                            
    self.CUSTOM_PROGRESSBAR.Run_ProgressBar(progressbar_ui_id=id, 
                                           currentItemNum=1, 
                                           amountOfItems=(Amount of Items eg:10), 
                                           col=g_lib.DARK_BLUE_TEXT_COL)  # DARK_BLUE_TEXT_COL # DARK_RED_TEXT_COL 
                                                                            color is a c4d.Vector(0,0,0). 
                                                                            eg: DARK_RED_TEXT_COL=c4d.Vector(0.99609375, 0, 0)

    self.CUSTOM_PROGRESSBAR.Stop_ProgressBar(progressbar_ui_id=id)
    """

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def GUI(self, str_amount_id, progress_id, size_w, size_l):
        """ Progress Bar GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
        self.instance.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        # ProgressBar
        self.instance.AddCustomGui(progress_id, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, size_w, size_l)
        if str_amount_id:
            # Static UI Text
            self.instance.AddSeparatorV(0, c4d.BFV_SCALEFIT)
            self.instance.AddStaticText(str_amount_id, c4d.BFH_MASK, 50, size_l, " 0%", c4d.BORDER_WITH_TITLE_BOLD)
        self.instance.GroupEnd()
        return True

    def Run_ProgressBar(self, str_amount_id, progressbar_ui_id, currentItemNum, amountOfItems, col):
        """ Progress Bar Running  """
        percent = float(currentItemNum)/amountOfItems*100
        # Set Data to PROGRESSBAR
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
        progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
        # this if you want a custom color
        if col:
            self.instance.SetDefaultColor(progressbar_ui_id, c4d.COLOR_PROGRESSBAR, col)    
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        # Return Percent String Data
        PercentData = str(int(percent))+"%"
        return self.instance.SetString(str_amount_id, " "+PercentData)
    
    def Stop_ProgressBar(self, progressbar_ui_id):
        """ Progress Bar Stop Running  """
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg.SetBool(c4d.BFM_STATUSBAR_PROGRESSON, False)
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        return True

# Create a QuickTab Custom GUI.
class AddCustomQuickTab_GUI(object):
    """
    This Custom GUI call a QuickTab is how to add and create a :
        - ``QuickTab Default Bar Title GUI``
        - ``QuickTab Fold Arrow Handle Bar Title GUI``
        - ``QuickTab Radio Tabs Bar Titles GUI ``
    """

    CUSTOMGUI_FLAG = c4d.CUSTOMGUI_QUICKTAB
    uiRadioTab = None

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def Add_BarTitle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Default Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color
        self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        return True   

    def Add_BarTitleArrowHandle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Fold Arrow Handle Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BARLAYERCOLOR] = ui_color
        # GUI Toggle System for QuickTab Bar Fold Icon Button.  
        # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
        # Handle as subgroup. Like bar mode, but with fold arrow icon. Implies QUICKTAB_BAR. Call QuickTabCustomGui.IsSelected()            
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
         # Set the Fold Handle to Open or Close.
        self.instance.ui = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        self.instance.ui.Select(0, True) # Set the Fold Handle to Open or Close.
        return True

    def Add_BarTabsRadio_GUI(self, bar_id, width, height, ui_color, list_tabs):
        """
        Create a QuickTab Radio Tabs Bar Titles Custom GUI.
        """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")
        self.instance.GroupSpace(0, 0)
        CustomGui_UI = c4d.BaseContainer()
        CustomGui_UI.FlushAll()
        CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, False)
        CustomGui_UI.SetBool(c4d.QUICKTAB_SHOWSINGLE, True)
        CustomGui_UI.SetBool(c4d.QUICKTAB_NOMULTISELECT, True)
        self.instance.uiRadioTab = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, CustomGui_UI)
        for tab in list_tabs:
            self.instance.uiRadioTab.AppendString(tab['id'], tab['str'], tab['state'])
        if ui_color:
            # c4d.COLOR_QUICKTAB_BG_ACTIVE
            # c4d.COLOR_QUICKTAB_TEXT_ACTIVE 
            # c4d.COLOR_QUICKTAB_TEXT_INACTIVE
            # c4d.COLOR_QUICKTAB_BG_INACTIVE
            self.instance.uiRadioTab.SetDefaultColor(bar_id, c4d.COLOR_QUICKTAB_BG_ACTIVE, ui_color)       
        self.instance.GroupEnd()
        return self.instance.uiRadioTab

    def TabRadioIsSelected(self, widgetUI, selectedID):
        state = None
        if widgetUI.IsSelected(selectedID):
            state = True
        else:
            state = False
        return state

# Create a Linkbox List InExcludeCustomGui Custom GUI.
class AddLinkBoxInExcludeList_GUI(object):
    """
    This GUI name is really call a c4d.gui.InExcludeCustomGui.
    / InExclude custom GUI (CUSTOMGUI_INEXCLUDE_LIST).
    Link: https://developers.maxon.net/docs/Cinema4DPythonSDK/html/modules/c4d/CustomDataType/InExcludeData/index.html
    """
    #inexclude = c4d.InExcludeData() # Create an InExcludeData class instance
    
    def __init__(self, ui_WidgetInstance):
        self.uiIns = ui_WidgetInstance

    def LinkListWidget(self, widgetID, widgetW, widgetH, enable_state_flags):
        """
        This GUI Widget
        """
        #First create a container that will hold the items we will allow to be dropped into the INEXCLUDE_LIST gizmo
        acceptedObjs = c4d.BaseContainer()
        acceptedObjs.InsData(c4d.Obase, "") # -> # Accept all objects / you change as desired
                                                # Take a look at c4d Objects Types in SDK.
                                                
        # Create another base container for the INEXCLUDE_LIST gizmo's settings and add the above container to it
        bc_IEsettings = c4d.BaseContainer()
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_SEND_SELCHANGE_MSG, True)
        bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_INIT_STATE, 1)
        """
        Its buttons with states for each object in the list container gui of the CUSTOMGUI_INEXCLUDE_LIST.
        feel free to enable this in the ui by seting the  enable_state_flags to True.
        """
        if enable_state_flags == True:
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_NUM_FLAGS, 2)
            # button 1
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_ON, 1039241) # -> Id Icon or Plugin Id
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_01_OFF, 1039241)
            # button 2
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_ON, 1039801)
            bc_IEsettings.SetData(c4d.IN_EXCLUDE_FLAG_IMAGE_02_OFF, 1036394)

        bc_IEsettings.SetData(c4d.DESC_ACCEPT, acceptedObjs)
        w_size = widgetW
        h_size = widgetH
        return self.uiIns.AddCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, w_size, h_size, bc_IEsettings)

    def Get_ObjectsLinkList(self, widgetID, ListOfData):
        """ Get Objects From LinkList View GUI Widget. """
        doc = c4d.documents.GetActiveDocument()
        amountOfObjects = None
        LinkList =  self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST)
        Get_ObjectListData = LinkList.GetData()
        objs = Get_ObjectListData.GetObjectCount()
        if objs:
            #currentNum = 1
            amountOfObjects = objs
            for i in xrange(objs):
                # Get Data From Objects
                objData = Get_ObjectListData.ObjectFromIndex(doc, i)
                objName_data = Get_ObjectListData.ObjectFromIndex(doc, i).GetName()
                objID_data = Get_ObjectListData.ObjectFromIndex(doc, i).GetGUID()
                Data = { "OBJ":objData, "OBJNAME":objName_data, "GUID":objID_data }
                ListOfData.append(Data)
        return ListOfData, amountOfObjects

    def SetDataToLinkBoxList(self, widgetID, Data):
        """ Set Objects to LinkList View GUI Widget."""
        inexclude = c4d.InExcludeData() # Create an InExcludeData class instance
        doc = c4d.documents.GetActiveDocument()
        LinkList = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_INEXCLUDE_LIST)
        if Data:            
            inexclude.InsertObject(Data, 1)
            LinkList.SetData(inexclude)
            print("ReAdded")
        else:
            pass
        return True

# Create a Linkbox (c4d.CUSTOMGUI_LINKBOX) GUI.
class AddCustomLinkBox_GUI(object):

    def __init__(self, ui_WidgetInstance):
        self.uiIns = ui_WidgetInstance

    def GetDataFromLinkBox(self, widgetID):
        DATA = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_LINKBOX).GetLink()
        objDATA = DATA
        objName = DATA.GetName()
        objGUID = DATA.GetGUID()
        return objDATA, objName, objGUID
        
    def SetDataFromLinkBox(self, widgetID, Data):
        LinkWidget = self.uiIns.FindCustomGui(widgetID, c4d.CUSTOMGUI_LINKBOX)
        LinkWidget.SetLink(Data)
        return True

# Create a TreeView Custom GUI and the class data function structure which is the TreeViewFunction_Data class.
def Add_TreeView_GUI(ui_instance, ui_id, group_id, tv_colums, get_folder, extension):
    """
    Create a TreeView Custom GUI and the class data function structure which is the TreeViewFunction_Data class.
    """
    ui_instance.LayoutFlushGroup(group_id) # Refresh Group UI

    column_id = {'id':101, 'col_name':"Files"}

    ##############################################################
    # Add the TreeView GUI to UI
    ##############################################################
    TreeViewCustomGui = c4d.BaseContainer()
    TreeViewCustomGui.SetLong(column_id['id'], c4d.LV_TREE)           # This helps the .SetLayout function.
    TreeViewCustomGui.SetLong(c4d.TREEVIEW_BORDER, c4d.BORDER_THIN_IN)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_HAS_HEADER, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_HIDE_LINES, False)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_MOVE_COLUMN, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_RESIZE_HEADER, True)
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_FIXED_LAYOUT, True)  # Don't allow Columns to be re-ordered
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_ALTERNATE_BG, True)  # Alternate Light/Dark Gray BG
    TreeViewCustomGui.SetBool(c4d.TREEVIEW_CURSORKEYS, True)    # Process Up/Down Arrow Keys
    ui_instance.AddCustomGui(ui_id, c4d.CUSTOMGUI_TREEVIEW, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 300, 100, TreeViewCustomGui)

    ################################################################
    # Setup the TreeView Layout and Set initial values.
    ################################################################
    # Find CustomGui TreeView
    SetTreeView = ui_instance.FindCustomGui(ui_id, c4d.CUSTOMGUI_TREEVIEW)
    if not SetTreeView:
       return False
    # Setup TreeView and it's global data TreeView functions class, which is call TreeViewFunction_Data class. 
    TreeView_FunctionDataClass = TreeViewFunction_Data(ext=extension, get_files=get_folder)
    SetTreeView.SetRoot(None, TreeView_FunctionDataClass, None)
    SetTreeView.SetLayout(tv_colums, TreeViewCustomGui)
    SetTreeView.SetHeaderText(column_id['id'], column_id['col_name'])   
    SetTreeView.Refresh()

    ui_instance.LayoutChanged(group_id) # Update Group UI
    return True
class ListFileItem():
    """A simple class for storing an item with a name, selection, & open status."""
    def __init__(self, name=""):
        self.name = name
        self.selected = False
        self.opened = True
class TreeViewFunction_Data(c4d.gui.TreeViewFunctions):
    """Data structure for a TreeView of the Files."""
    # The __init__ is an Constuctor and help get and passes data on from the another class.
    def __init__(self, ext, get_files):
        self._XML_files_items = []

        """
        Get the data from the gui tree view funtion that was pass down into the class.
        The gui function is called Add_TreeView_GUI. 
        """
        self.end_extension = ext
        self.end_ext = self.end_extension
        self.split_ext = " " + self.end_extension
        self.folder_with_files = get_files

        # Now load data and the object that are files from the folder.
        self.LoadObjects()        

    def LoadObjects(self):
        for file in os.listdir(self.folder_with_files):
            if file.endswith(self.end_ext):                
                # Get all files and split or cut off the extension from it.
                for files in file.splitlines():
                    fileName = files.split(self.split_ext)
                    xmlflie = ListFileItem(fileName[0])
                    # Add file to menu list by append.
                    self._XML_files_items.append(xmlflie) # [0] to remove extra characters.
        return True        

    def GetFirst(self, root, userdata):
        """Returns the first Material in the document."""
        if self._XML_files_items:
            return self._XML_files_items[0]

    def GetDown(self, root, userdata, XML_files):
        return None
  
    def GetNext(self, root, userdata, XML_files):
        """Get the next files in the lic4d.storage."""
        if XML_files in self._XML_files_items:
            obj_index = self._XML_files_items.index(XML_files)
            next_index = obj_index + 1
            if next_index < len(self._XML_files_items):
                return self._XML_files_items[next_index]

    def GetPred(self, root, userdata, XML_files):
        if  XML_files in self._XML_files_items:
            obj_index = self._XML_files_items.index(XML_files)
            prev_index = obj_index - 1
            if prev_index > 0:
                return self._XML_files_items[prev_index]

    def GetName(self, root, userdata, XML_files):
        return XML_files.name
    
    def IsOpened(self, root, userdata, XML_files):
        return XML_files.opened    
        
    def IsSelected(self, root, userdata, XML_files):
        """Returns:(bool): True if *obj* is selected, False if not."""
        return XML_files.selected

    def Select(self, root, userdata, XML_files, mode):
        """Called when the user selects an element."""
        if mode == c4d.SELECTION_NEW:
            for item in self._XML_files_items:
                item.selected = False
                if item == XML_files:
                    item.selected = True
        elif mode == c4d.SELECTION_ADD:
            XML_files.selected = True
        elif mode == c4d.SELECTION_SUB:
            XML_files.selected = False

    def DoubleClick(self, root, userdata, XML_files, col, mouseinfo):
        """Called when the user double-clicks on an entry in the TreeView."""
        #Remove_And_Share_Popup_Menu(f_Name=XML_files.name, F_ext=self.SplitEXT)
        return True

# Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
class Image_Function_Data(c4d.gui.GeUserArea):
    
    def __init__(self, image, width, height, info_text):
        self.bmp = c4d.bitmaps.BaseBitmap()
        self.image_path = image
        self.W = width
        self.H = height
        self.text = info_text

    def GetMinSize(self):
        self.width = self.W        # eg: 245  /  WIDTH OF YOUR IMAGE FILE
        self.height = self.H       # eg: 60  /  HEIGHT OF YOUR IMAGE FILE
        return (self.width, self.height)

    def DrawMsg(self, x1, y1, x2, y2, msg):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.bmp.GetBw(), self.bmp.GetBh(), x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)

    def Redraw(self):
        result, ismovie = self.bmos.path.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.path.GetBw()
        y2 = self.bmp.path.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.os.path.GetBw(), self.bmp.GetBh(), x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)
            
    def Timer(self, msg):
        self.Redraw()
def Add_Image_GUI(ui_instance, image, width, height, info_text):
    """
    Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
    """
    # Get the GeUserArea class and pass the data to it.
    ui_instance.Image_Class = Image_Function_Data(image, width, height, info_text)
    # This is were your plugin Image Banner in group go.
    ui_instance.AddUserArea(1, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ui_instance.Image_Class, 1)
    ui_instance.Image_Class.LayoutChanged()
    return True
def Inject_Image_GUI(func_instance, injected_group_id, image, width, height, info_text):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    Add_Image_GUI(ui_instance=func_instance, image=image, width=width, height=height, info_text=info_text)
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True
class Set_Area_Image_Function_Data(c4d.gui.GeUserArea):
    
    def __init__(self, image, width, height, info_text):
        self.bmp = c4d.bitmaps.BaseBitmap()
        self.image_path = image
        self.W = width
        self.H = height
        self.text = info_text

    def GetMinSize(self):
        self.width = self.W        # eg: 245  /  WIDTH OF YOUR IMAGE FILE
        self.height = self.H       # eg: 60  /  HEIGHT OF YOUR IMAGE FILE
        return (self.width, self.height)

    def DrawMsg(self, x1, y1, x2, y2, msg):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.W, self.H, x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)



    def Redraw(self):
        result, ismovie = self.bmp.InitWith(self.image_path)
        x1 = 0
        y1 = 0
        x2 = self.bmp.GetBw()
        y2 = self.bmp.GetBh()
        if result == c4d.IMAGERESULT_OK:
            self.DrawBitmap(self.bmp, 0, 0, self.W, self.H, x1, y1, x2, y2, c4d.BMP_NORMALSCALED | c4d.BMP_ALLOWALPHA)
            self.DrawSetTextCol(c4d.COLOR_TEXT,c4d.COLOR_TRANS)
            self.DrawSetFont(c4d.FONT_BOLD)            
            self.DrawText(self.text, 130, 45)

    def Timer(self, msg):
        self.Redraw()
def Add_SizeFit_Image_GUI(ui_instance, id, image, width, height, info_text, auto_store=True):
    """
    Create a full custom image to add to UI, like a banner, logo, and etc.. and register it. Then add to UI dialog.
    """
    # Get the GeUserArea class and pass the data to it.
    ui_instance.Image_Class = Set_Area_Image_Function_Data(image, width, height, info_text)


    if auto_store:
        if not hasattr(ui_instance, '_icon_buttons'):
            ui_instance._icon_buttons = []
        ui_instance._icon_buttons.append(ui_instance.Image_Class)

    # This is were your plugin Image Banner in group go.
    ui_instance.AddUserArea(id, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ui_instance.Image_Class, id)
    ui_instance.Image_Class.LayoutChanged()
    return True

# Create a full customize GUI Button with text str and image icon.
class IconButton(c4d.gui.GeUserArea):
    
    VERSION = (1, 4)
 
    M_NOICON = 0
    M_ICONLEFT = 1
    M_ICONRIGHT = 2
    M_FULL = 3
    M_ICONUP = 4
 
    C_TEXT = c4d.COLOR_TEXT
    C_BG = c4d.COLOR_BGEDIT
    C_HIGHLIGHT = c4d.COLOR_BGFOCUS
    C_BGPRESS = c4d.COLOR_BG
 
    S_ICON = 32
    S_PADH = 1
    S_PADV = 1
 
    def __init__(self, paramid, text, icon, mode=M_ICONLEFT):
        super(IconButton, self).__init__()
        self.paramid = paramid
        self.text = text
        self.icon = icon
        self.mode = mode
        self.pressed = False
        #self.col_BG = bg_col

        self.last_t = -1
        self.mouse_in = False
        self.interval = 0.2
 
    def _CalcLayout(self):
        text_x = self.S_PADH
        text_w = self.DrawGetTextWidth(str(self.text))
        text_h = self.DrawGetFontHeight()
        icon_x = self.S_PADH
        
        
        
        width = text_w + self.S_PADH * 2
        height = max([text_h, self.S_ICON]) + self.S_PADV * 2
 
        draw_icon = True
        if self.mode == self.M_ICONLEFT:
            icon_x = self.S_PADH
            text_x = self.S_PADH + self.S_ICON + self.S_PADH
            width += self.S_ICON + self.S_PADH
            
        elif self.mode == self.M_ICONRIGHT:
            icon_x = self.GetWidth() - (self.S_PADH + self.S_ICON)
            text_x = self.S_PADH
            width += self.S_ICON + self.S_PADH
            
        elif self.mode == self.M_ICONUP:
            self.S_PADH = self.GetWidth()/2
            text_x = self.S_PADH - (text_w/2)
            icon_x = (text_x + (text_w/2)) - (self.S_ICON/2)
            width += self.S_ICON + self.S_PADH
            height += self.GetHeight() +  self.S_ICON
        else:
            draw_icon = False
 
        return locals()
 
    def _DrawIcon(self, icon, x1, y1, x2, y2):
        # Determine if the icon is a simple color.
        if not icon:
            pass
        if isinstance(icon, (int, c4d.Vector)):
            self.DrawSetPen(icon)
            self.DrawRectangle(x1, y1, x2, y2)
        # or if it is a bitmap icon.
        elif isinstance(icon, c4d.bitmaps.BaseBitmap):
            self.DrawBitmap(icon, x1, y1, (x2 - x1), (y2 - y1),
                            0, 0, icon.GetBw(), icon.GetBh(), c4d.BMP_ALLOWALPHA)
        else:
            return False
 
        return True
 
    def _GetHighlight(self):
        delta = time.time() - self.last_t
        return delta / self.interval
 
    def _GetColor(self, v):
        if isinstance(v, c4d.Vector):
            return v
        elif isinstance(v, int):
            d = self.GetColorRGB(v)
            return c4d.Vector(d['r'], d['g'], d['b']) ^ c4d.Vector(1.0 / 255)
        else:
            raise TypeError('Unexpected value of type %s' % v.__class__.__name__)
 
    def _InterpolateColors(self, x, a, b):
        if x < 0: x = 0.0
        elif x > 1.0: x = 1.0
 
        a = self._GetColor(a)
        b = self._GetColor(b)
        return a * x + b * (1 - x)

    ###################################################
    #  GeUserArea Overrides
    ###################################################
    def DrawMsg(self, x1, y1, x2, y2, msg):
        self.DrawSetFont(c4d.FONT_BOLD)
        self.OffScreenOn() # Double buffering
 
        # Draw the background color.
        bgcolor = c4d.COLOR_BG
        if self.pressed:
            bgcolor = self.C_BGPRESS
        elif self.mode == self.M_FULL and self.icon:
            bgcolor = self.icon
        else:
            h = self._GetHighlight()
            ca, cb = self.C_HIGHLIGHT, c4d.COLOR_BG
            if not self.mouse_in:
                ca, cb = cb, ca
 
            # Interpolate between these two colors.
            bgcolor = self._InterpolateColors(h, ca, cb)
 
        w, h = self.GetWidth(), self.GetHeight()
        self._DrawIcon(bgcolor, 0, 0, w, h)
        
        # Determine the drawing position and size of the
        # colored icon and the text position.
        layout = self._CalcLayout()
 
       
        if layout['draw_icon']:
            x = layout['icon_x']
            y = min([h / 2 - self.S_ICON / 2, self.S_PADV])
 
            # Determine if the icon_DrawIcon
            self._DrawIcon(self.icon, x, y, x + self.S_ICON, y + self.S_ICON)
 
        if 'draw_text':
            self.DrawSetTextCol(self.C_TEXT, c4d.COLOR_TRANS)
            x = layout['text_x']
            y = max([h / 2 - layout['text_h'] / 2, self.S_PADV])

            if self.mode == self.M_ICONUP:
                #=========================================================================================================================
                y = y + 10
            self.DrawText(str(self.text), x, y)
        vec = c4d.Vector(0.99609375, 0, 0)
        self.DrawSetPen(vec)
        self.DrawSetFont(c4d.FONT_BOLD)
        #self.DrawSetTextCol(fg, bg)
        #self.DrawBorder(c4d.BORDER_BLACK|c4d.BORDER_ROUND, x1,y1,x2-1,y2-1)

    def GetMinSize(self):
        layout = self._CalcLayout()
        return layout['width'], layout['height']
 
    def InputEvent(self, msg):
        device = msg.GetLong(c4d.BFM_INPUT_DEVICE)
        channel = msg.GetLong(c4d.BFM_INPUT_CHANNEL)
 
        catched = False
        if device == c4d.BFM_INPUT_MOUSE and channel == c4d.BFM_INPUT_MOUSELEFT:
            self.pressed = True
            catched = True
 
            # Poll the event.
            tlast = time.time()
            while self.GetInputState(device, channel, msg):
                if not msg.GetLong(c4d.BFM_INPUT_VALUE): break
 
                x, y = msg.GetLong(c4d.BFM_INPUT_X), msg.GetLong(c4d.BFM_INPUT_Y)
                map_ = self.Global2Local()
                x += map_['x']
                y += map_['y']
 
                if x < 0 or y < 0 or x >= self.GetWidth() or y >= self.GetHeight():
                    self.pressed = False
                else:
                    self.pressed = True
 
                # Do not redraw all the time, this would be useless.
                tdelta = time.time() - tlast
                if tdelta > (1.0 / 30): # 30 FPS
                    tlast = time.time()
                    self.Redraw()
 
            if self.pressed:
                # Invoke the dialogs Command() method.
                actionmsg = c4d.BaseContainer(msg)
                actionmsg.SetId(c4d.BFM_ACTION)
                actionmsg.SetLong(c4d.BFM_ACTION_ID, self.paramid)
                self.SendParentMessage(actionmsg)
 
            self.pressed = False
            self.Redraw()
 
        return catched
 
    def Message(self, msg, result):
        if msg.GetId() == c4d.BFM_GETCURSORINFO:
            if not self.mouse_in:
                self.mouse_in = True
                self.last_t = time.time()
                self.SetTimer(30)
                self.Redraw()
        return super(IconButton, self).Message(msg, result)
 
    def Timer(self, msg):
        self.GetInputState(c4d.BFM_INPUT_MOUSE, c4d.BFM_INPUT_MOUSELEFT, msg)
        g2l = self.Global2Local()
        x = msg[c4d.BFM_INPUT_X] + g2l['x']
        y = msg[c4d.BFM_INPUT_Y] + g2l['y']
 
        # Check if the mouse is still inside the user area or not.
        if x < 0 or y < 0 or x >= self.GetWidth() or y >= self.GetHeight():
            if self.mouse_in:
                self.mouse_in = False
                self.last_t = time.time()
 
        h = self._GetHighlight()
        if h < 1.0:
            self.Redraw()
        elif not self.mouse_in:
            self.Redraw()
            self.SetTimer(0)
def Add_Icon_Button_GUI(ui_instance, paramid, text, icon, mode=IconButton.M_ICONLEFT, auto_store=True):
    """
    Creates an Icon Button on the passed dialog.
    """
    ua = IconButton(paramid, text, icon, mode)
 
    if auto_store:
        if not hasattr(ui_instance, '_icon_buttons'):
            ui_instance._icon_buttons = []
        ui_instance._icon_buttons.append(ua)
 
    ui_instance.AddUserArea(paramid, c4d.BFH_CENTER)
    ui_instance.AttachUserArea(ua, paramid)
    return ua            

# Create Our Status Bar GUI
def Add_StatusBar_GUI(ui_instance, ui_id, state_col, str_id, message):
    """A module-level docstring

    Notice the comment above the docstring specifying the encoding.
    Docstrings do appear in the bytecode, so you can access this through
    the ``__doc__`` attribute. This is also what you'll see if you call
    help() on a module or any other Python object.
    """    
    ui_instance.GroupBegin(ui_id, c4d.BFH_SCALEFIT, 1, 0, "")
    ui_instance.SetDefaultColor(ui_id, c4d.COLOR_BG, state_col)
    ui_instance.AddStaticText(str_id, c4d.BFH_SCALEFIT, 0, 15, " Status : " + message, c4d.BORDER_WITH_TITLE_BOLD)
    ui_instance.GroupEnd()
    return True
def Status_GUI_Updater(ui_instance, ui_id, str_id, state_col, message):
    ui_instance.SetDefaultColor(ui_id, c4d.COLOR_BG, state_col)
    ui_instance.SetString(str_id," Status : " + message)
    return True

# Create our GUI Bottom Company Banner with Button
def Bottom_FCS_Web_GUI_Banner(ui_instance, button_id):
    ui_instance.GroupBegin(10, c4d.BFH_SCALEFIT, 1, 0)
    ui_instance.AddSeparatorH(0, flags=c4d.BFH_SCALEFIT)
    ui_instance.SetDefaultColor(10, c4d.COLOR_BG, BG_DEEP_DARKER)
    Add_Icon_Button_GUI(ui_instance, button_id, " Select N'Go by FCS ", None, IconButton.M_NOICON)
    ui_instance.GroupEnd() 
    return True

# To Inject UI layout to a empty group.
def Inject_GUI(func_instance, injected_group_id, gui_element):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    gui_element
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True
def Inject_GUI_Icon(func_instance, injected_group_id, data):
    """ Inject your UI layout or GUI element to a empty group. """
    func_instance.LayoutFlushGroup(injected_group_id) # Refresh Group UI
    Add_Image_Button_GUI(ui_instance=func_instance, btn_settings=data)
    func_instance.LayoutChanged(injected_group_id) # Update Group UI
    return True

# Enables a UIElement
def EnableElement(self, element):
    """ Enables a UI Widget Element """
    self.Enable(element, True)
    return True

# Disables a UIElement
def DisableElement(self, element):
    """ Disables a UI Widget Element """
    self.Enable(element, False)
    return True

def IfWindowUIdlgOpen(guiWin):
    windowDialogOpen = None
    if windowDialogOpen == None:
        windowDialogOpen = guiWin       
    return windowDialogOpen


def OpenSelectedDirectoryWindow(ld_title, suffix):
    path = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, 
                                  title=ld_title, 
                                  flags=c4d.FILESELECT_DIRECTORY, 
                                  force_suffix=suffix)
    return path

# ---------------------------------------------------------------------
#        Instance Functions Operations Hepler Method Functions 
# ---------------------------------------------------------------------

# Get C4D  Project Doc 
def GetActiveProjectDocument():
    """ Get the Active C4D Project Document. (``c4d.documents.GetActiveDocument()``)  """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    return doc

# Get C4D Project Data Settings 
def GetProjectDocumentDataSettings(doc):
    """  doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT) """
    SettingsData = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
    return SettingsData

# Get C4D Object Data
def GetC4DObjectData(obj):
    """ Get C4D Object Data  """
    
    # Get Object Id ( A int ).
    Id = obj.GetGUID()
    
    # Get Object Name ( A String ).
    Name = obj.GetName()
    
    # Get All Object Tags.
    Tags = obj.GetTags()
    
    # Get Object and Check if Object is a Polygon ( An Editable Mesh ).
    if obj.CheckType(c4d.Opolygon):
        # Get Object polygons amounts. ( A int ).
        PolygonAmount = obj.GetPolygonCount()
        # Get the selected polygon which is BaseSelect( bs ).
        PolygonSelect = obj.GetPolygonS()
    else:
        PolygonAmount = None
        PolygonSelect = None
        pass
        
    return Id, Name, Tags, PolygonAmount, PolygonSelect

def GetC4dObjectGlobalPostion(doc, obj):
    """ 
    Get Global World Postion of a C4d Object.
    This Method Function Returns 3 values of 
    X, Y, Z postion location data of the c4d object.
    """
    OBJ_Pos = obj.GetMg() # Globe position from the matrix
    globalPosData = OBJ_Pos.off  # Get the position from the matrix
    #print str(globalPosData)
    # Split up the GlobalPosData Vector that is Store
    removeOpenBracket = str(globalPosData).split('(')
    removeCloseBracket = removeOpenBracket[1].split(')')
    values = removeCloseBracket[0].split(',') 
    xLoc = values[0]
    yLoc = values[1]
    zLoc = values[2]  
    return xLoc, yLoc, zLoc 

# 
def GetSelectionOfActiveObjects(doc):
    """ Get Selection of Objects from the Object Manager ( ``C4D: doc.GetActiveObjects(1)`` ) """     
    SelectionListOfObjects = doc.GetActiveObjects(1)
    if not SelectionListOfObjects:
        c4d.gui.MessageDialog("Select an Object!")
        #g_lib.fcsLog.WARNING("Select an Object!", True)
        return
    return SelectionListOfObjects   

# 
def GetC4DTextureTagName(tag):
    """ Get the C4D Texture Tag Material Name. """
    tagMatLink = tag[c4d.TEXTURETAG_MATERIAL]
    # Turn the (tagMatLink) data to a string. 
    makstr = str(tagMatLink)
    # Get string by spliting a long string uos.path.
    str_1 = makstr.split("'")[1]
    finalstr = str_1.split('/')[0] 
    outputData = finalstr
    return outputData # Return the output which is the Name of the material.

def LoadImageIcon(i):
    """ 
    Get Icon Image from Plugin Icons Folder
    Add your icon image to GUI_Icons{..} (dict) on above.
    eg. GUI_Icons = {
                    'myicon1':"xxx_icon.png",
                    . . . .
                    } 
    Then later in code:
    output = I(i="myicon1")
    """
    GetImagePath = os.path.join(plugDIR.pluginImages, plugDIR.GUI_Icons[i])
    return GetImagePath 

# Get a Icon from that are a group of Icons one image.
def GetCustomImageIcon(image, size, locCordX, locCordY):
    """ 
    Cinema4D Get Texture Atlas Image and Get Image At Coordinate.
    Get a Icon from a group of Icons that area in one image.
    """
    bmp = c4d.bitmaps.BaseBitmap()
    bmp.InitWith(image)
    w = size
    h = size
    x = locCordX
    y = locCordY
    selectIcon = bmp.GetClonePart(x, y, w, h)
    return selectIcon

# Remove Object 
def RemoveObjectFromScene(objName):
    """ Remove Object From Scene """
    doc = c4d.documents.GetActiveDocument()
    if not isinstance(objName, str):
        if objName:
            objName.Remove()
    else:
        doc = c4d.documents.GetActiveDocument()
        get_merge_obj = doc.SearchObject(objName)
        if get_merge_obj:
            get_merge_obj.Remove()
    return True

# Makes each object editable
def ObjectToCurrentState(obj):
    """
    Makes each object editable.
    The object stays in the document
    """
    commandid = c4d.MCOMMAND_MAKEEDITABLE
    objdoc = obj.GetDocument()
    flags = c4d.MODELINGCOMMANDFLAGS_CREATEUNDO
    mode = c4d.MODELINGCOMMANDMODE_ALL
    
    converted = utils.SendModelingCommand(command=commandid, list=[obj], doc=objdoc, flags=flags, mode=mode)
    return converted

# Set the Plugin Icon Toggle State System.
def SetIconToggleState(pluginid, bmp, overwrite_alpha=True):
    icon = c4d.gui.GetIcon(pluginid)
    if not icon and bmp:
        return c4d.gui.RegisterIcon(pluginid, bmp)
    elif icon and not bmp:
        return c4d.gui.UnregisterIcon(pluginid, bmp)
    elif not icon:
        return

    ref = icon['bmp']
    w, h = icon['w'], icon['h']

    temp = c4d.bitmaps.BaseBitmap()
    if temp.Init(w, h, bmp.GetBt()) != c4d.IMAGERESULT_OK:
        return False

    bmp.ScaleIt(temp, 256, True, True)

    a1 = a2 = None
    if overwrite_alpha:
        a1 = ref.GetInternalChannel()
        a2 = temp.GetInternalChannel()
    for x in xrange(w):
        rx = x + icon['x']
        for y in xrange(h):
            ry = y + icon['y']
            ref[rx, ry] = temp[x, y]
            if a1:
                if a2:
                    alpha = temp.GetAlphaPixel(a2, x, y)
                else:
                    alpha = 255
                ref.SetAlphaPixel(a1, rx, ry, alpha)
    return True 

def LoadC4DFile(folder, c4dfile):
    """ This Function Loads c4d file. (Example: .l4d, .c4d files). """
    openfile = os.path.join(folder, c4dfile) # To open up folder Path 
    c4d.documents.LoadFile(openfile)    
    return True

# Open Preset Data folder for Tools 
def OpenDataFolder(FolderName):
    c4d.storage.ShowInFinder(FolderName, False)
    #self.Fold_Bar(openbar="o", close="None")
    return True  

# Open FCS Web Link Function 
def OpenFCSWebLink(page):
    url = "http://www.fieldcreatorsstudios.com/"+ page
    webbrowser.open(url, new=2, autoraise=True)  
    return True    

# Open Web Link Function 
def OpenWebLink(page):
    url = page
    webbrowser.open(url, new=2, autoraise=True)  
    return True   

# These Operations Functions helps the tools and scripts tools.
def Run_Tool_ID(Tool_ID):
    plug = plugins.FindPlugin(Tool_ID, c4d.PLUGINTYPE_COMMAND)     
    if plug is None:
        c4d.gui.MessageDialog("Error 404:\nSorry! You don't have this tool installed or\nThis is feature coming soon.")
        return   
    else:
        c4d.CallCommand(Tool_ID) #Call on Tool
        return True    
def Load_Character2Doc_Scene(P_MergeFile, path):
    doc = c4d.documents.GetActiveDocument()
    fileName_Path = os.path.join(path, P_MergeFile)
    file_Load = c4d.documents.MergeDocument(doc, fileName_Path, c4d.SCENEFILTER_OBJECTS | c4d.SCENEFILTER_MATERIALS | c4d.SCENEFILTER_MERGESCENE)
    c4d.documents.BaseDocument(file_Load)
    c4d.EventAdd()      
    return True      
def CheckTool_and_Add_Tool_Menu(menu, menu_ID, plug_i, plug_data):

    plug_icon = plug_i
    plug_id = plug_data["ID"]
    plug_Name = plug_data["Name"]

    plug = plugins.FindPlugin(plug_id, c4d.PLUGINTYPE_COMMAND)     
    if plug is None:
        return
    else:
        menu.SetString(menu_ID, plug_icon + plug_Name)
    return True
def RemoveEmptyNulls(obj):
    doc = c4d.documents.GetActiveDocument()
    doc.StartUndo() 
    if not obj:
        return
    RemoveEmptyNulls(obj.GetDown())
    RemoveEmptyNulls(obj.GetNext())
    if not obj.GetDown():
        if obj.GetType()== c4d.Onull:
            if not obj.GetFirstTag():
                obj.Remove()
                return
    doc.EndUndo()                 
    return True 

# Loading a build in C4D preset file. / 
def Load_C4D_Preset_File(C4D_File):
    "Load BUILD-IN C4D Set Preset File."
    c4d.documents.LoadFile(C4D_File)
    c4d.EventAdd()
    return True

# To add and create random colors to UI elements and objects.
def Add_Vector_Random_Colors():
    """
    To add and create random colors to UI elements and objects.
    """
    r = randint(0,255) / 256.0
    g = randint(0,255) / 256.0
    b = randint(0,255) / 256.0
    return c4d.Vector(r, g, b)

# Send Instructions to the Instructions Dialog of a c4d [.MessageDialog()].
def SendInstructions(Message):
    """
    Send Instructions to the Instructions Dialog of a c4d [.MessageDialog()].
    """
    # Set Read Message Error Log Text Doc.
    # Then Open Instructions_Dialog
    gui.MessageDialog(Message)
    return True 

# Toggle System function for hiding GUI element ON and OFF.    
def Toggle_On_and_Off_GUI(func_instance, ui_group, toggle_id, update_main_grp): 
    # This Method function works like this, to pass data or info to this function:
    # eg: [ QuickTab_FoldIcon_BTN_Toggle(func_instance=self, ui_group=[ID INFO], toggle_id=[ID INFO], update_main_grp=[ID INFO]) ]

    self_ui = func_instance

    self_ui.GET_TOGGLE_ID = toggle_id
    self_ui.GET_GROUP_ID = ui_group
    self_ui.GET_Overall_GRP_ID = update_main_grp

    if self_ui.GetString(self_ui.GET_TOGGLE_ID) == "Hide_UI":
        self_ui.HideElement(self_ui.GET_GROUP_ID, False) # Show
        self_ui.SetString(self_ui.GET_TOGGLE_ID, "Show_UI")
        #print "Open"

    else:
        self_ui.HideElement(self_ui.GET_GROUP_ID, True) # Hide
        self_ui.SetString(self_ui.GET_TOGGLE_ID, "Hide_UI")
        #print "Close"

    self_ui.LayoutChanged(self_ui.GET_Overall_GRP_ID) # Update UI
    return True        

def Toggle_Image_GUI_BTN(func_instance, toggle_mode, btn_id, ui_icon):
    self_ui = func_instance
    self_ui.GET_BUTTON_ID = btn_id
    # Change Button to default play icon.
    Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
    Button.SetToggleState(toggle_mode)   
    Button.SetImage(ui_icon, True)
    self_ui.LayoutChanged(10042)     
    return True

# Toggle System function for GUI Icon/image Button with Enable or Disable.
def Toggle_Image_BTN_GUI(func_instance, btn_id, toggle_id, default_icon, enable_icon, default_str_state):
    """
    Toggle System function for GUI Icon/image Button with Enable or Disable GUI Image.
    This Method function works like this, to pass data or info to this function:
    eg: [ self.GUI_BTN_Toggle(func_instance=self, btn_id=[ID INFO], toggle_id=[ID INFO], default_icon=[INFO], enable_icon=[INFO], default_str_state=[INFO])
    """
    self_ui = func_instance

    self_ui.GET_BUTTON_ID = btn_id
    self_ui.GET_TOGGLE_ID = toggle_id

    if self_ui.GetString(self_ui.GET_TOGGLE_ID) == default_str_state:
        # Change Button to enable icon.
        Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
        Button.SetToggleState(True)   
        Button.SetImage(enable_icon, True)
        self_ui.LayoutChanged(10042)                
    else:
        # Change Button to default icon.
        Button = self_ui.FindCustomGui(self_ui.GET_BUTTON_ID, c4d.CUSTOMGUI_BITMAPBUTTON)
        Button.SetToggleState(False)   
        Button.SetImage(default_icon, True)
        self_ui.LayoutChanged(10042) 
    return True

# Gettngs Layers and Layers Names From Cinema 4D Layer TreeView Manger.
MatchedLayer = None
def GetAll_Childen_Of_Layers(AllLayers, layername):
        #print "Find Layer: " + layername
        global MatchedLayer
        for layer in AllLayers:
            name = layer.GetName()
            #print "Current Layer: " + name
            
            if name == layername:
                print "Returning Layer: " + name
                MatchedLayer = layer
                print MatchedLayer
            
            #print "Current Layer " + layername + " doesnt match checking for chiildren"
            
            # Recursive Checking System
            if(layer.GetChildren > 0):
                #print "Current Layer " + layer.GetName() + "has children processing....."
                GetAll_Childen_Of_Layers(layer.GetChildren(), layername)
        return True
def GetLayer(layername):
    doc = c4d.documents.GetActiveDocument()
    root = doc.GetLayerObjectRoot() #Gets the layer manager
    LayersList = root.GetChildren()
    GetAll_Childen_Of_Layers(LayersList, layername)  
    print "In GetLayer matched value is: " + str(MatchedLayer)
    return True

# Set Object Name
def SetObjectName(name, obj, amount):
    """ 
    Set Object Name
    This will allow you to set name object with a digit 
    number at the end of the string.
    eg:
        Cube_M.1
        Cube_M.2
    eg: 
        SetObjectName(name="Cube_M",  # String
                      obj=Object,     # Select Object
                      amount=100      # Range Amount
                      )
        In Code:
        SetObjectName(name="Cube_M", obj=Object, amount=100)                  
    """
    doc = c4d.documents.GetActiveDocument() 
    baseName = doc.SearchObject(name +".100000000000")
    if baseName == None:
        baseName = name + ".0"
    degit = +1
    obj.SetName(name + '.' + str(degit))
    for N in list(reversed(range(amount))):
        findobj = doc.SearchObject(name + '.' + str(N)) 
        if findobj == None:            
            obj.SetName(name+ '.' + str(N)) #+ '.' + str(N)
    return obj

# Remove Children from Parent Object
def removeChildren(obj_children):
    """
    Remove Child Object
    This will allow you to remove children from Parent Object
    """
    if obj_children:
        for child in obj_children:
            child.Remove()
    return True

# Create custom Null group object and add to object manger.
def Generate_Null_Group(str_name):
    doc = c4d.documents.GetActiveDocument()
    doc.StartUndo()           
    Insert_Null = c4d.BaseObject(c4d.Onull)
    doc.AddUndo(c4d.UNDOTYPE_CHANGE, Insert_Null)
    Insert_Null[c4d.ID_BASEOBJECT_USECOLOR]=1
    Insert_Null[c4d.ID_BASEOBJECT_COLOR] = Add_Vector_Random_Colors()
    #Insert_Null[c4d.NULLOBJECT_ICONCOL]=True
    Insert_Null.SetName(str_name)
    doc.InsertObject(Insert_Null)
    doc.AddUndo(c4d.UNDOTYPE_NEW, Insert_Null)
    doc.EndUndo()        
    c4d.EventAdd()
    return True

# Add Settings Item.
def add_item_(ui, ui_id, str_info, config_file):
    add_item = ui
    CHECKBOX = '&c&'
    DISABLE_UI = '&d&' # Disable this item    

    jsonEdit = FCS_JsonSystem_Editor()

    xml_data = jsonEdit.GetItemProperties(config_file, "SweepSettings", str_info)

    if xml_data == True:
        add_item.InsData(ui_id, str_info + CHECKBOX)

    #elif xml_mode == "Disable":
    #    add_item.InsData(ui_id, str_info + DISABLE_UI)
        
    else:
        add_item.InsData(ui_id, str_info)
        
    return True
# Create Settings Checklist Menu.     
def PopupSettings_Checklist(config_file, list):

    # Main Menu
    menu = c4d.BaseContainer()

    for item_list in list:
        add_item_(ui=menu, ui_id=item_list['IDM'], str_info=item_list['StrName'], config_file=config_file)
        menu.InsData(0, '') # Append separator

    # Sub Menu 
    #submenu1 = c4d.BaseContainer()     
    #menu.InsData(ui_id, str_info + CHK)
    
    #menu.SetContainer(IDM_MENU3, submenu1)

    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='Cap_Start', store_state="CBC", store_mode="CBC_Mode")
    #menu.InsData(0, '')     # Append separator
    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='Cap_End', store_state="CBC", store_mode="CBC_Mode")
    #menu.InsData(0, '')     # Append separator
    #self.add_item_(ui=menu, ui_id=IDM_MENU5, str_info='CreateSingleObject', store_state="CBC", store_mode="CBC_Mode")

    # Finally show popup dialog
    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)
    print result
    
    #if result == IDM_MENU1:
    #    self.Checked_Toggle(store_state="Random")
        
    #if result == IDM_MENU2:
    #    self.Checked_Toggle(store_state="AF")

    return True

# Get and Check or Remove Material.
def GetAndCheckMaterial(doc, MatName, remove_mat):
    """ 
    Get and check or remove material you are searching for. 
    """
    # Search material and Get it.
    Mat = doc.SearchMaterial(MatName)
    # Check if material is there and delete it.
    if remove_mat == True: 
        if Mat:
            Mat.Remove()
    return Mat 

# Add a Bitmap Material    
def ApplyBitmapMaterial(doc, obj, mat_name, bitmap_image, add_col, col, check_mat, mat_prez_res, add_tex_tag):
    """ Adding a Bitmap Material and a Material Texture Tag to object """

    # // Create and Set Material //
    MatName = mat_name
    Mat = check_mat

    if add_tex_tag == True:
        Material_TexTag = obj.MakeTag(c4d.Ttexture)
        Material_TexTag[c4d.TEXTURETAG_PROJECTION]=6
        Material_TexTag[c4d.TEXTURETAG_TILE]=True

    if not Mat:
        shdBitmap = c4d.BaseShader(c4d.Xbitmap)
        if shdBitmap is None:
            print("Error: Shader allocation failed.")
            return
        shdBitmap[c4d.BITMAPSHADER_FILENAME] = bitmap_image
        Material_Tex = c4d.BaseMaterial(5703)               # Generate Material
        Material_Tex[c4d.ID_BASELIST_NAME] = MatName        # Generate Material Name
        if add_col == True:
            Material_Tex[c4d.MATERIAL_USE_COLOR] = True
            Material_Tex[c4d.MATERIAL_COLOR_COLOR] = col
        Material_Tex[c4d.MATERIAL_PREVIEWSIZE]=mat_prez_res  # Set Material Resulation  Size
        Material_Tex[c4d.MATERIAL_USE_REFLECTION]=False
        Material_Tex[c4d.MATERIAL_COLOR_SHADER] = shdBitmap # Set Material Shader Tex
        Material_Tex.InsertShader(shdBitmap)                # Insert Material Shader 
        doc.InsertMaterial(Material_Tex)                    # Insert Material  
        if add_tex_tag == True:
            Material_TexTag[c4d.TEXTURETAG_MATERIAL]=Material_Tex
    else:
        if add_tex_tag == True:
            Material_TexTag[c4d.TEXTURETAG_MATERIAL]=Mat

    c4d.EventAdd()        
    return True  

# Getting Shader
def GetShader(ShaderIdType):
    """ Get and Check Shader If There """
    shader = c4d.BaseShader(ShaderIdType)
    if shader is None:
        print "Error: Shader allocation failed"
        return
    return shader  


def CreateAndSaveFile(winTile, suffix):
    """ cLib.CreateAndSaveFile(winTile="FlightSim X File", suffix="x") """
    filename = c4d.storage.SaveDialog(type=c4d.FILESELECTTYPE_ANYTHING, title=winTile, force_suffix=suffix)
    if not filename:
        raise RuntimeError("Couldn't export, due to Directory field is empty.")
    else:
        pass #print(filename)
    return filename


###########################################
# Settings UI Popup Menu with a Checklist
###########################################

# Create .XML config.. settings file.
def CreatConfigFile(self):
    if not os.path.exists(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS):
        # //  Create Xml Structure. //
        MakeXml_root = minidom.Document()
        AddToXML = MakeXml_root.createElement('FCS_RP_CONIG')
        MakeXml_root.appendChild(AddToXML)
        
        rpData = MakeXml_root.createElement('Settings_Data')
        # Toggle Data
        rpData.setAttribute('Random', "True")
        rpData.setAttribute('AF', "False")
        rpData.setAttribute('RR', "False")
        rpData.setAttribute('GS', "False")
        rpData.setAttribute('CBC', "False")

        rpData.setAttribute('R_Mode', "Enable")
        rpData.setAttribute('AF_Mode', "Enable")
        rpData.setAttribute('RR_Mode', "Enable")
        rpData.setAttribute('GS_Mode', "Enable")
        rpData.setAttribute('CBC_Mode', "Enable")

        AddToXML.appendChild(rpData)
        
        # Save XML CONIG
        SaveXml = MakeXml_root.toprettyxml(indent="\t")
        with open(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS, 'w') as f:
            f.write(SaveXml)
            f.close()
    return True  
# Toggle the .xml file by editing it function.
def Toggle(self, State, store_state):
    # Parse the xml file and get the data (Creates an object of the xml file to minipulate)
    tree = etree.parse(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS)
    # Now Edit the xml attribute by providing the tree elementName, attribute value and the new value in a string
    EditXmlAttribute(tree,"Settings_Data", store_state, str(State))
    # Now lets get the root of the xml
    root = tree.getroot()
    # Fix the xml structure from the root level
    indent(root)
    # and finally save the xml to the file
    with open(PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS, 'w') as f:
        f.write(etree.tostring(root))
    returnc
# Check item when toggle.
def Checked_Toggle(store_state):
    XML_DATA_FILES = PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    for rp_data in XmlRoot.findall('Settings_Data'):
        
        xml_data = rp_data.get(store_state)
        if xml_data == "True":
            Toggle(State=False, store_state=store_state)

        else:
            Toggle(State=True, store_state=store_state)
    return True
def Checked_ModeToggle(store_mode):
    XML_DATA_FILES = PLUGIN_DIR().FCS_RRP_CONFIG_SETTINGS
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    for rp_data in XmlRoot.findall('Settings_Data'):

        xml_mode = rp_data.get(store_mode)
        if xml_mode == "Enable":
            Toggle(State="Disable", store_state=store_mode)
        else:
            Toggle(State="Enable", store_state=store_mode)
    return True


# ---------------------------------------------------------------------
#                  XML System 
# ---------------------------------------------------------------------
def GetXmlData(xml_conig, rootData, data):
    """
    g_lib.GetXmlData(xml_conig="xml.file", rootData="RootName", data="PathRoot" )
    """
    # To look in both files 
    XML_DATA_FILES = xml_conig
    XmlFileTree = etree.parse(XML_DATA_FILES) 
    XmlRoot = XmlFileTree.getroot()
    # C4D To MCX XML Data
    # Set Data
    for get_C4D2MCX_data in XmlRoot.findall(rootData):

        GetMCXData = get_C4D2MCX_data.get(data)

    return GetMCXData

# ---------------------------------------------------------------------
#                Create a Thumbnail Image
# ---------------------------------------------------------------------
def WriteBitmap(bmp, format_filter=c4d.FILTER_PNG, settings=c4d.BaseContainer()):
    mfs = storage.MemoryFileStruct()
    mfs.SetMemoryWriteMode()
    hf = storage.HyperFile()
    if hf.Open(0, mfs, c4d.FILEOPEN_WRITE, c4d.FILEDIALOG_NONE):
        if not hf.WriteImage(bmp, format_filter, settings):  
            return None
        hf.Close()
    return mfs
def RenderSystemCustomSettings(imagePath, imageWidth, imageHeight, imageFormat):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    c4d.StatusSetText("Adding Object to Storage please wait......")


    c4d.StatusSetSpin()
     
    #c4d.CallCommand(431000060, 431000060)                 # Viewport Solo Hierarchy
    
    # Get the User Render Data.
    GET_User_rd = doc.GetActiveRenderData().GetData()
    GET_RENDERENGINE = GET_User_rd[c4d.RDATA_RENDERENGINE]
    GET_xres = int(GET_User_rd[c4d.RDATA_XRES])
    GET_yres = int(GET_User_rd[c4d.RDATA_YRES])
    GET_FilePath = GET_User_rd[c4d.RDATA_PATH]
    GET_FileFormat = GET_User_rd[c4d.RDATA_FORMAT]
    
    #c4d.CallCommand(12151, 12151)                        # Center Object to Camera

    # Set the Render Data.   
    set_renderData = doc.GetFirstRenderData()             # c4d.documents.RenderData()

    # ------------------------------#
    # Set Render c4d.RDATA_RENDERENGINE 
    """
    RDATA_RENDERENGINE  ( LONG ) - ( Render Engine (external renderers pass their plugin ID). )

        RDATA_RENDERENGINE_STANDARD

        Full Render.

        RDATA_RENDERENGINE_PREVIEWSOFTWARE     /  ID : 1

        Software Preview.

        RDATA_RENDERENGINE_PREVIEWHARDWARE     /  ID : 300001061

        Hardware Preview.
    """

    set_renderData[c4d.RDATA_RENDERENGINE]=c4d.RDATA_RENDERENGINE_PREVIEWHARDWARE
    set_renderData[c4d.RDATA_XRES] = float(imageWidth)           # Set Dimensions 
    set_renderData[c4d.RDATA_YRES] = float(imageHeight)          # Set Dimensions
    set_renderData[c4d.RDATA_FORMAT] = imageFormat
    set_renderData[c4d.RDATA_PATH] = imagePath
    doc.SetActiveRenderData(set_renderData)

    # Get Render Data / Get Image Dimensions in the Render Data.
    rd_temp = doc.GetActiveRenderData().GetData()
    xres = int(rd_temp[c4d.RDATA_XRES])
    yres = int(rd_temp[c4d.RDATA_YRES])

    #rd_temp.SetFilename(c4d.RDATA_PATH, FileName)
    """
    Initialize the bitmap with the result size
    The resolution/Image Dimensions must match with 
    the output size of the render settings.
    """
    bmp = bitmaps.BaseBitmap()
    bmp.Init(x=xres, y=yres, depth=24)
    #bmp.Save(FileName, c4d.FILTER_PNG)
    res = documents.RenderDocument(doc, rd_temp, bmp, c4d.RENDERFLAGS_EXTERNAL) 
    if res == c4d.RENDERRESULT_OK:
        print "done"
        #bitmaps.ShowBitmap(bmp)

    #c4d.CallCommand(12148, 12148)                         # Frame Geometry

    # Set User Render Data Back.
    rd_two = doc.GetFirstRenderData()
    rd_two[c4d.RDATA_RENDERENGINE] = GET_RENDERENGINE  
    rd_two[c4d.RDATA_XRES] = float(GET_xres)
    rd_two[c4d.RDATA_YRES] = float(GET_yres)
    rd_two[c4d.RDATA_PATH] = GET_FilePath
    rd_two[c4d.RDATA_FORMAT]= GET_FileFormat
    doc.SetActiveRenderData(rd_two)
        
    #c4d.CallCommand(431000058, 431000058)                 # Viewport Solo Off
    c4d.StatusSetSpin()

    c4d.EventAdd()        
    return True
def C4D_CreateThumbnail_Image(imagePath, imageFormat):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    c4d.documents.SaveDocument(doc, fcsDIR.fcsTempFile, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, 1001026)
    c4d.EventAdd()
       
    doc = c4d.documents.GetActiveDocument() # Get Doc again.
    doc[c4d.DOCUMENT_PREVIEW_COMMAND]=0
    c4d.EventAdd(c4d.EVENT_FORCEREDRAW)
    dst = bitmaps.BaseBitmap()
    dst = doc.GetDocPreviewBitmap()
    dst.Save(imagePath, imageFormat)
    c4d.EventAdd()
    return True
def C4D_DefaultLockThumbnail_Image(imageTempPath, imagePath, path, imageName):
    """ Create a Thumbnail Image. """
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    Temp_Image = path + "/" + imageName

    shutil.copy2(imageTempPath, path)

    os.rename(Temp_Image, imagePath)

    c4d.EventAdd()
    return True


# ---------------------------------------------------------------------
#        Utils C4D Operations Hepler Method Functions 
# ---------------------------------------------------------------------
def C4dRunApp(AppNameEXE,  AppPath, Msg, AppArgumentsData):
    """ C4D Run Application Software with data """
    if Msg == None:
        Msg = "application software"
    print ("Attempting to execute {0}").format(Msg)
    result = c4d.storage.GeExecuteProgram(os.path.join(AppPath, AppNameEXE), AppArgumentsData)
    if result:
        print("{0} executed successfully").format(Msg)
    else:
        print("{0} failed to execute successfully").format(Msg)
    return True