"""
Select N Go Core Hepler Library Module
and
Pluigin Register ID and Name + GUI Enums ID's
"""

import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from random import randint
from os import urandom
import weakref


# Cinema 4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.utils import GeRayCollider
from c4d.plugins import CommandData, TagData, ObjectData
# Regex Import
import re


from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib 
from snglib.CoreTools.GUIs import ExportedPopupWindowUi
from snglib.CoreTools.GUIs import ExportFileNameWindowUi
from snglib.CoreTools.GUIs import ExportedTakesPopupWindowUi
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()
jEdit = JsonSysHelperLib.FCS_JsonSystem_Editor()

FCSWEBLINKS= { 
                "OnlineManual":"https://docs.google.com/document/d/e/2PACX-1vTGCu4gX5fmblEzyE7hpyH1wxTLFgZkot_1AWVqoUGw4wv5KM71m1USABDukJxOb3vCVb-lb38N6ErK/pub",
                "DIS":"https://discord.gg/CQQn33W",  
                "GUM":"https://gumroad.com/fcstudios",
                "YT":"https://www.youtube.com/channel/UC_5YTyCTAFyNut_QR0YZqsg"
             }

VerData = " v3.3.0 | RC "
ID_VER = 178

StudioName = "F.C.S - "
Toolset_State = "-Beta v"
__version_data__ = "2.40.09"
DataInfo = "InHouse" + Toolset_State + __version_data__
PLUGIN_Version = "RC - F.C.S C4D ToolKit | "+ DataInfo # Your Version Our Plugin
Initialized = "Select N'Go v3 Beta is Initialized. Tool by F.C.S"

AboutTool = """ Version : Select N'Go v3.3 RC | 2020 \n Deleveloper: Ashton Rolle aka ApAshtonTheCreator \n Studio : Field Creators Studios (FCS) """


""" 
GUI Enums ID's Tpyes
_________________________________________________________________ 
"""
class GlobalEnumsAndMainRegisterIDs(object):
    """
    Main C4D Plugin Tool Register ID's

    ``Eg.:``
    SNG_PLUGIN_ID = 0000000

    WARNING! Please obtain your own plugin ID 
    from http://www.plugincafe.com by Maxon and 
    Its free to get a ID.

    and

    GLOBAL STRINGS TEXT in the c4d_strings.str // 
    Calling on the IDs to get Text String

    GLOBAL TOOLS UI ID's in the c4d_symbols.h file

    ``Eg.:``

     In the c4d_symbols.h file :

     IDS_ID  = 1109

     In the c4d_strings.str file :

     IDS_ID   "HelloWorld";

    """
    SNG_PLUGIN_ID = 1055455

    ID_SNG_MENU_EXTENSIONS = 2074
    ID_SNG_MENU_ADD_EXPORTFILTER = 2075
    ID_SNG_MENU_ADD_IMPORTFILTER = 2076
    ID_SNG_MENU_OPENPRESETS_DIR = 2073
    ID_SNG_MENU_OPENTAKES_DIR = 2072
    ID_SNG_MENU_EXP_L_DEF_OPT = 2071
    ID_SNG_MENU_EXP_L_DEFOPT_ONE = 2070
    ID_SNG_MENU_EXP_L_DEFOPT_TWO = 2069
    ID_SNG_MENU_SAVEPRESET = 2068
    ID_SNG_MENU_FILE_INFO = 2066
    ID_SNG_MENU_FILE = 2065

    ID_SNG_OPT_CHK_BATCH_IMPORT_WMAT = 2077


    ID_SNG_FCSWEBBANNER = 2044
    ID_SNG_STATUS_BAR_GRP = 2043
    ID_SNG_BOTTOM_GRP = 2042
    ID_SNG_TAKES_TAB_GRP = 204
    ID_SNG_OPT_EXPORT_IMPORT_BTN = 2040
    ID_SNG_OPT_PATH_DIR_BTN_GRP = 2039
    ID_SNG_OPT_PATH_EDITBOX = 2038
    ID_SNG_OPT_PATH_TXT = 2037
    ID_SNG_OPT_PATH_RF_BTN = 2036
    ID_SNG_OPT_PATH_PANEL_GRP = 2035
    ID_SNG_OPT_PATH_TITLE = 2034
    ID_SNG_OPT_PATH_GRP = 2033
    ID_SNG_OPT_BATCH_LIST = 2032
    ID_SNG_OPT_BATCH_FILETYPE_TXT = 2031
    ID_SNG_OPT_BATCH_FILENAME_TXT = 2030
    ID_SNG_OPT_BATCH_CHK_ALL = 2047
    ID_SNG_OPT_BATCH_LIST_GRP = 2029
    ID_SNG_OPT_SEARCH_DIR_BTN = 2028
    ID_SNG_OPT_SEARCH_EDITBOX = 2027
    ID_SNG_OPT_SEARCH_TXT = 2026
    ID_SNG_OPT_BATCH_SEARCHICON_GRP = 2048
    ID_SNG_OPT_BATCH_SEARCH_GRP = 2025
    ID_SNG_OPT_BATCHFITER_MENU = 2049
    ID_SNG_OPT_BATCH_DIR_BTN_GRP = 2024
    ID_SNG_OPT_BATCH_EDITBOX = 2023
    ID_SNG_OPT_BATCH_TXT = 2022
    ID_SNG_OPT_BATCH_TITLE_GRP = 2046
    ID_SNG_OPT_BATCH_DIR_GRP = 2021
    ID_SNG_OPT_BATCH_IMPORT_GRP = 2020
    ID_SNG_OPT_CHK_BATCH_IMPORT = 2019
    ID_SNG_OPT_CHK_OPENFOLDER = 2018
    ID_SNG_OPT_CHK_CENTER_OBJ = 2017
    ID_SNG_OPT_CHK_UNITY_AXIS = 2051
    ID_SNG_OPT_CHK_SINGLE_OBJ = 2016
    ID_SNG_OPT_OPTIONS_EXPORT_GRP = 2045
    ID_SNG_OPT_OPTIONS_TITLE_GRP = 2015
    ID_SNG_OPT_OPTIONS_GRP = 2014
    ID_SNG_OPT_FORMAT_GEAR_SETTINGS = 2013
    ID_SNG_OPT_FORMAT_DROPLIST_MENU = 2012
    ID_SNG_OPT_FORMAT_SETTINGS_TITLE_GRP = 2011
    ID_SNG_OPT_FORMAT_SETTINGS_GRP = 2010
    ID_SNG_OPT_MODE_IMPORT = 2009
    ID_SNG_OPT_MODE_EXPORT = 2008
    ID_SNG_OPT_MODE_TITLE_GRP = 2007
    ID_SNG_OPT_MODE_SETTINGS_GRP = 2006
    ID_SNG_OPTIONS_TAB_GRP = 2005
    ID_SNG_EXPORT_OPTIONS_TAB_GRP = 200532
    ID_SNG_IMPORT_OPTIONS_TAB_GRP = 200531  
    ID_SNG_OPTIONS_TABS = 200530  
    ID_SNG_TABS_GRP = 2004
    ID_SNG_OVERALL_GRP = 2003
    ID_SNG_DIALOG_TITLE = 2002
    fcs_select_n_go_gui_dlg = 2001

    ID_SNG_MENU_OffManual = 2064
    ID_SNG_MENU_OnManual = 2063

    ID_SNG_OPT_CHK_EXPORT_SEPARATE = 2061
    ID_SNG_OPT_CHK_EXPORT_POPUP = 2062

    ID_SNG_TAKES_RECENT_DIR_BTN     = 2090
    ID_SNG_MENU_FILTERS_IMP_SAVES   = 2089
    ID_SNG_MENU_FILTERS_EXP_SAVES	= 2088
    ID_SNG_MENU_FILTERS_SAVES		= 2087
    ID_SNG_TAKES_SET_SOLO_PREVIEW	= 2086
    ID_SNG_TAKES_SETTINGS			= 2085
    ID_SNG_TAKES_CLIPS_LOAD			= 2084
    ID_SNG_TAKES_CLIPS_SAVE			= 2083
    ID_SNG_TAKES_EXPORT_DIR_BTN 	= 2082
    ID_SNG_TAKES_EXPORT_PATH 		= 2081
    ID_SNG_TAKES_EXPORT_BTN 		= 2080
    ID_SNG_TAKES_MENU_BTN 			= 2079
    ID_SNG_TAKES_ADD_CLIPS_BTN 		= 2078


fcsID = GlobalEnumsAndMainRegisterIDs()

# [ Create FCS Tools Prefs Directory ]
def Create_SNG_Prefs_Folder(c4d_prefs_folder):
    """ Create F.C.S Select N'Go Preferences Folder, If not in the Maxon Cinema 4D Preferences Folder. """
    plugin_prefs = os.path.join(c4d_prefs_folder, 'sngPrefs')
    if os.path.exists(plugin_prefs):
        pass
    else:
        os.mkdir(plugin_prefs)
        os.mkdir(os.path.join(plugin_prefs, 'SelectNGoPresetsSaves'))
        os.mkdir(os.path.join(plugin_prefs, 'SelectNGoTakesSaves'))
        os.mkdir(os.path.join(plugin_prefs, 'SelectNGoExtensions'))
        os.mkdir(os.path.join(plugin_prefs, 'SelectNGoExtensions', 'ExportFilters'))
        os.mkdir(os.path.join(plugin_prefs, 'SelectNGoExtensions', 'ImportFilters'))
    return True


def AddVersonToMenuBar(ui_instance):
    self = ui_instance
    self.GroupBeginInMenuLine()
    self.AddStaticText(ID_VER, 0)
    self.SetString(ID_VER, VerData)
    self.GroupEnd()
    self.SetDefaultColor(ID_VER, c4d.COLOR_TEXT, cLib.DARK_BLUE_TEXT_COL)  
    return True


# This function is check for export filter IDs and then add it to the combox formats lic4d.storage.
def AddExportFilter(ComboID, ChildID, FilterString, FilterID, popupmenu):
    plug = plugins.FindPlugin(FilterID, c4d.PLUGINTYPE_SCENESAVER)     
    if plug is None:
        #print FilterString + " This Plugin Is Not Install."
        pass   
    else:
        popupmenu.InsData(c4d.FIRST_POPUP_ID+ChildID, '&i1055455&'+FilterString)

        #else:
        #    popupmenu.AddChild(ComboID, ChildID, FilterString)

    return True   

def ObjFilter(get_ver):
    """ // Check if which Obj install  // """
    OBJ1 = {'export_id':1030178, 'import_id':1030177}  # Obj for R17/R19 
    OBJ2 = {'export_id':1001040, 'import_id':1001039} # Obj for R14/R16 

    if get_ver == "export":
        obj = OBJ1['export_id'] or OBJ2['export_id']
    if get_ver == "import":
        obj = OBJ1['import_id'] or OBJ2['import_id']
        
    plug = c4d.plugins.FindPlugin(obj, c4d.PLUGINTYPE_SCENESAVER)     
    if plug is None:
        pass
    return obj

def glbfilter(extNeed):
    ext = None
    # Get Alembic export plugin, 1028082 is its ID
    plug = plugins.FindPlugin(1041129, c4d.PLUGINTYPE_SCENESAVER)
    #print(plug)
    if plug is None:
        return
    op = {}
    # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
    if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, op):
        #print op
        if "imexporter" not in op:
            return
        
        # BaseList2D object stored in "imexporter" key hold the settings
        fExport = op["imexporter"]
        if fExport is None:
            return

        if extNeed == True:
            if fExport[c4d.GLTFEXPORTER_FILEFORMAT] == 1022:
                ext = ".glb"
                #print("*.glb")
            else:
                if fExport[c4d.GLTFEXPORTER_FILEFORMAT] == 1021:
                    ext = ".gltf"
        else:
            if fExport[c4d.GLTFEXPORTER_FILEFORMAT] == 1022:
                ext = "glb"
            else:
                if fExport[c4d.GLTFEXPORTER_FILEFORMAT] == 1021:
                    ext = "gltf"            

    return ext


class c4dSceneFilters(object):
    """ The Main C4D Scene Filters, Export & Import Formats ID's eg.(fbx, 3ds, dae, etc) """


    """
    -----------------------
    for p in c4d.plugins.FilterPluginList(c4d.PLUGINTYPE_SCENESAVER,True):
            print p.GetID(), " = ", p.GetName()
    -----------------------
    1001038  =  3D Studio (*.3ds)
    1028082  =  Alembic (*.abc)
    1016440  =  Allplan (*.xml)
    180000105  =  Bullet (*.bullet)
    1001026  =  Cinema 4D (*.c4d)
    1022316  =  COLLADA 1.4 (*.dae)
    1025755  =  COLLADA 1.5 (*.dae)
    1001047  =  Direct 3D (*.x)
    1001036  =  DXF (*.dxf)
    1026370  =  FBX (*.fbx)
    1041129  =  glTF (*.gltf/*.glb)
    1025281  =  IES Meta (*.txt)
    1012074  =  Illustrator (*.ai)
    1001021  =  STL (*.stl)
    1039865  =  Volume (*.vdb)
    1001034  =  VRML 2 (*.wrl)
    1030178  =  Wavefront OBJ (*.obj)
    -----------------------
    for p in c4d.plugins.FilterPluginList(c4d.PLUGINTYPE_SCENELOADER,True):
            print p.GetID(), " = ", p.GetName()
    -----------------------
    1001037  =  3D Studio (*.3ds)
    1028081  =  Alembic (*.abc)
    1001048  =  BVH (*.bvh)
    1039778  =  CATIA (*.CATPart *.CATProduct *.cgr)
    1001025  =  Cinema 4D (*.c4d)
    1022315  =  COLLADA 1.4 (*.dae)
    1025754  =  COLLADA 1.5 (*.dae)
    1001046  =  DEM (*.dem)
    1021372  =  DWG (*.dwg)
    1001035  =  DXF (*.dxf)
    1026369  =  FBX (*.fbx)
    1053589  =  Goz Importer
    1039779  =  IGES (*.igs *.iges)
    1001045  =  Illustrator (*.ai)
    1039780  =  JT (*.jt)
    1040819  =  MeshObject JSON Asset
    1033845  =  SKP (*.skp)
    1039781  =  Solidworks (*.SLDPrt *.SLDAsm *.SLDDrw)
    1039777  =  STEP (*.stp *.step *.p21)
    1001020  =  STL (*.stl)
    450000233  =  Updater loader
    1039864  =  Volume (*.vdb)
    1001033  =  VRML 2 (*.wrl)
    1030177  =  Wavefront OBJ (*.obj)
    -----------------------
    """

    FBX_Filter = {'id':122, 'export_id':1026370, 'import_id':1026369, 'str':"FBX (*.fbx)", 'ext':"fbx", 'extstr':".fbx"}
    DAE14_Filter = {'id':101, 'export_id':1022316, 'import_id':1022315, 'str':"COLLADA 1.4 (*.dae)", 'ext':"dae", 'extstr':".dae"}
    ABC_Filter = {'id':102, 'export_id':1028082, 'import_id':1028081, 'str':"Alembic (*.abc)", 'ext':"abc", 'extstr':".abc"}
    C4D3Ds_Filter = {'id':103, 'export_id':1001038, 'import_id':1001037, 'str':"3D Studio (*.3ds)", 'ext':"3ds", 'extstr':".3ds"}
    OBJ_Filter = {'id':104, 'export_id':ObjFilter(get_ver="export"), 'import_id':ObjFilter(get_ver="import"), 'str':"Wavefront (*.obj)", 'ext':"obj", 'extstr':".obj"}
    C4D_Filter = {'id':105, 'export_id':1001026, 'import_id':1001025, 'str':"CINEMA 4D Project(*.c4d)", 'ext':"c4d", 'extstr':".c4d"}
    X3D_Filter = {'id':106, 'export_id':1001028, 'str':"CINEMA 4D XML (*.xml)", 'ext':"xml", 'extstr':".xml"}
    C4DX_Filter = {'id':107, 'export_id':1001047, 'str':"Direct 3D (*.x)", 'ext':"x", 'extstr':".x"}
    DAE15_Filter = {'id':108, 'export_id':1025755, 'import_id':1025754, 'str':"COLLADA 1.5 (*.dae)", 'ext':"dae", 'extstr':".dae"}
    AI_Filter = {'id':109, 'export_id':1012074, 'import_id':1001045, 'str':"Illustrator (*.ai)", 'ext':"ai", 'extstr':".ai"}
    STL_Filter = {'id':110, 'export_id':1001021, 'import_id':1001020, 'str':"STL (*.stl)", 'ext':"stl", 'extstr':".stl"}
    WRL_Filter = {'id':111, 'export_id':1001034, 'import_id':1001033, 'str':"VRML 2 (*.wrl)", 'ext':"wrl", 'extstr':".wrl"}
    DXF_Filter = {'id':112, 'export_id':1001028, 'import_id':1001027, 'str':"DXF (*.dxf)", 'ext':"dxf", 'extstr':".dxf"}
    IPACS_TGI_Filter = {'id':113, 'export_id':1001959, 'str':"IPACS Scenery TGI Export", 'ext':"tgi", 'extstr':".tgi"}
    IPACS_TGC_Filter = {'id':114, 'export_id':1001957, 'str':"IPACS Scenery TSC Export", 'ext':"tsc", 'extstr':".tsc"}
    IPACS_TGC1cm_Filter = {'id':115, 'export_id':1001957, 'str':"IPACS TGI Export 1cm", 'ext':"tgi", 'extstr':".tgi"}
    IPACS_TGC1mm_Filter = {'id':116, 'export_id':1002961, 'str':"IPACS TGI Export 1mm", 'ext':"tgi", 'extstr':".tgi"}
    Corona_Filter = {'id':117, 'export_id':1036396, 'str':"Corona Proxy (*.cgeo)", 'ext':"cgeo", 'extstr':".cgeo"}
    Redshift_Filter = {'id':118, 'export_id':1038650, 'str':"Redshift Proxy (*.rs)", 'ext':"rs", 'extstr':".rs"}
    gITF_Filter = {'id':119, 'export_id':1041129, 'str':"glTF (*.gltf/*.glb)", 'ext':glbfilter(extNeed=False), 'extstr':glbfilter(extNeed=True)}
    VOL_Filter = {'id':120, 'export_id':1039865, 'import_id':1039864, 'str':"Volume (*.vdb)", 'ext':"vdb", 'extstr':".vdb"}
    APAN_Filter = {'id':121, 'export_id':1016440, 'str':"Allplan (*.xml)", 'ext':"xml", 'extstr':".xml"}

    CATIA1_Filter = {'id':124, 'export_id':0, 'import_id':1039778, 'str':"CATIA (*.CATPart)", 'ext':"CATPart", 'extstr':".CATPart"}
    CATIA2_Filter = {'id':125, 'export_id':0, 'import_id':1039778, 'str':"CATIA (*.CATProduct)", 'ext':"CATProduct", 'extstr':".CATProduct"}
    CATIA3_Filter = {'id':126, 'export_id':0, 'import_id':1039778, 'str':"CATIA (*.cgr)", 'ext':"cgr", 'extstr':".cgr"}

    BVH_Filter = {'id':127, 'export_id':0, 'import_id':1001048, 'str':"BVH (*.bvh)", 'ext':"bvh", 'extstr':".bvh"}

    DEM_Filter = {'id':128, 'export_id':0, 'import_id':1001046, 'str':"DEM (*.dem)", 'ext':"dem", 'extstr':".dem"}

    DWG_Filter = {'id':129, 'export_id':0, 'import_id':1021372, 'str':"DWG (*.dwg)", 'ext':"dwg", 'extstr':".dwg"}

    SKP_Filter = {'id':130, 'export_id':0, 'import_id':1033845, 'str':"SKP (*.skp)", 'ext':"skp", 'extstr':".skp"}

    JT_Filter = {'id':131, 'export_id':0, 'import_id':1039780, 'str':"JT (*.jt)", 'ext':"jt", 'extstr':".jt"}

    IGES1_Filter = {'id':132, 'export_id':0, 'import_id':1039779, 'str':"IGES (*.igs)", 'ext':"igs", 'extstr':".igs"}
    IGES2_Filter = {'id':134, 'export_id':0, 'import_id':1039779, 'str':"IGES (*.iges)", 'ext':"iges", 'extstr':".iges"}

    STEP1_Filter = {'id':135, 'export_id':0, 'import_id':1039777, 'str':"STEP (*.stp)", 'ext':"stp", 'extstr':".stp"}
    STEP2_Filter = {'id':136, 'export_id':0, 'import_id':1039777, 'str':"STEP (*.step)", 'ext':"step", 'extstr':".step"}
    STEP3_Filter = {'id':137, 'export_id':0, 'import_id':1039777, 'str':"STEP (*.p21)", 'ext':"p21", 'extstr':".p21"}


    FBX_Filter2 = {'id':0, 'export_id':1026370, 'import_id':1026369, 'str':"FBX (*.fbx)", 'ext':"fbx", 'extstr':".fbx"}
    C4D_Filter2 = {'id':1, 'export_id':1001026, 'import_id':1001025, 'str':"CINEMA 4D Project(*.c4d)", 'ext':"c4d", 'extstr':".c4d"}
    ABC_Filter2 = {'id':2, 'export_id':1028082, 'import_id':1028081, 'str':"Alembic (*.abc)", 'ext':"abc", 'extstr':".abc"}
    gITF_Filter2 = {'id':3, 'export_id':1041129, 'str':"glTF (*.gltf/*.glb)", 'ext':glbfilter(extNeed=False), 'extstr':glbfilter(extNeed=True)}
    OBJ_Filter2 = {'id':4, 'export_id':ObjFilter(get_ver="export"), 'import_id':ObjFilter(get_ver="import"), 'str':"Wavefront (*.obj)", 'ext':"obj", 'extstr':".obj"}


    ExportFilters = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter, C4D_Filter, gITF_Filter, X3D_Filter, APAN_Filter, C4DX_Filter, DAE15_Filter, AI_Filter, STL_Filter, 
                        WRL_Filter, DXF_Filter, IPACS_TGI_Filter, IPACS_TGC_Filter, IPACS_TGC1cm_Filter, IPACS_TGC1mm_Filter, Corona_Filter, Redshift_Filter, VOL_Filter ]
    
    ExportFiltersC1 = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter, C4D_Filter, X3D_Filter ]
    ExportFiltersC2 = [ gITF_Filter, VOL_Filter, C4DX_Filter, APAN_Filter, DAE15_Filter,  AI_Filter, STL_Filter, WRL_Filter, DXF_Filter] 
    ExportFiltersC3 = []

    ExportFiltersC4 = [FBX_Filter2, C4D_Filter2, ABC_Filter2, gITF_Filter2, OBJ_Filter2]

    # IPACS_TGC_Filter, IPACS_TGC1cm_Filter, IPACS_TGC1mm_Filter, Corona_Filter, Redshift_Filter

    ImportFilters = [ FBX_Filter, DAE14_Filter, ABC_Filter, C4D3Ds_Filter, OBJ_Filter, C4D_Filter, AI_Filter, STL_Filter, WRL_Filter, CATIA1_Filter, 
                      CATIA2_Filter, CATIA3_Filter, DXF_Filter, BVH_Filter, DEM_Filter, DWG_Filter, SKP_Filter,  JT_Filter, IGES1_Filter, IGES2_Filter,
                      STEP1_Filter, STEP2_Filter, STEP3_Filter ]

    ImportFiltersC1 = []

    def CheckForExtensionsFilters(self, path):
        ExportFiltersC3 = []
        ImportFiltersC1 = []

        ImportFilters = os.path.join(path, 'sngPrefs', 'SelectNGoExtensions', 'ImportFilters')
        for i in DirHelper.GetAllFilesInDirectoryAsList(DIR=ImportFilters, Extension="*.json"):
            self.ImportFilters.append({'id':int(jEdit.GetItemProperty(i, "id")), 
                                         'export_id':0, 
                                         'import_id':int(jEdit.GetItemProperty(i, "id")), 
                                         'str':str(jEdit.GetItemProperty(i, "str")), 
                                         'ext':str(jEdit.GetItemProperty(i, "ext")), 
                                         'extstr':str(jEdit.GetItemProperty(i, "extstr"))}
                                        )
        #print(self.ImportFilters)

        ExportFilters = os.path.join(path, 'sngPrefs', 'SelectNGoExtensions', 'ExportFilters')
        for i in DirHelper.GetAllFilesInDirectoryAsList(DIR=ExportFilters, Extension="*.json"):
            self.ExportFiltersC3.append({'id':int(jEdit.GetItemProperty(i, "id")), 
                                         'export_id':int(jEdit.GetItemProperty(i, "id")), 
                                         'import_id':0, 
                                         'str':str(jEdit.GetItemProperty(i, "str")), 
                                         'ext':str(jEdit.GetItemProperty(i, "ext")), 
                                         'extstr':str(jEdit.GetItemProperty(i, "extstr"))}
                                        )
        print(self.ExportFiltersC3)
        return True

filterlib = c4dSceneFilters()

# [ Opening Export Folder After File Export ]
def OpenExportFolderAfterExport(ui_instance):
    """ Opening Export Folder After File Export """
    self = ui_instance
    if self.GetBool(fcsID.ID_SNG_OPT_CHK_OPENFOLDER)==True:
        Dir = self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX)
        openEF = os.path.join(Dir)
        if openEF is False:
            cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added.")
            return
        else:
            c4d.storage.ShowInFinder(openEF, False)
    return True


""" 
Template Json Saves
_________________________________________________________________ 
"""
class ExportPresetSaveFileTemplate(object):
    """ Prefs Tool Json ConfigFile """
    def __init__(self, CenterExportedObjCheck, SeparateCheck, OpenExportPathCheck, ShowExportedFileCheck, ModelExportPath, ExportFormat, ExportFormatExt):
        self.CenterExportedObjCheck     = CenterExportedObjCheck
        self.SeparateCheck              = SeparateCheck
        self.OpenExportPathCheck        = OpenExportPathCheck
        self.ShowExportedFileCheck      = ShowExportedFileCheck
        self.ModelExportPath            = ModelExportPath
        self.ExportFormat               = ExportFormat
        self.ExportFormatExt            = ExportFormatExt

class CustomFormatFilterFileTemplate(object):
    """ Prefs Tool Json ConfigFile """
    def __init__(self, ui_id, export_id, import_id, strtext, ext, extstr):
        self.id        = ui_id
        self.export_id = export_id
        self.import_id = import_id
        self.str       = strtext
        self.ext       = ext
        self.extstr    = extstr


""" 
MianWindow UI Editing 
_________________________________________________________________ 
"""
# Adding Custom UI Button
def AddUiBtn(ui_instance, btnData):
    ImgButton = cLib.CustomImageButtonGUI(ui_instance)
    return ImgButton.GUI(btnData['ID'], icon=btnData['I'], tip=btnData['Tip']) 

# Editing the AnimTimeClip Method Function
def EditAnimTimeClip(self, ids):
    """ Editing the AnimTimeClip [ Start Time ] and [ End Time ] System Method Function """
    startT = self.GetString(ids["timeBTN"]["STARTID"])
    endT = self.GetString(ids["timeBTN"]["ENDID"])

    """ Update Node Data Structure """
    d1={
        "timeBTN":{ 'ID':ids["timeBTN"]["ID"],
                    'UT':"BUTTON",
                    'IV':ids["timeBTN"]["GRPSTATE"],
                    'I':ids["timeBTN"]["I"],
                    'BTNGRP':ids["timeBTN"]["BTNGRP"],
                    'GRPID':ids["timeBTN"]["GRPID"],
                    'GRPSTATE':ids["timeBTN"]["GRPSTATE"],
                    'STARTID':ids["timeBTN"]["STARTID"],
                    'ENDID':ids["timeBTN"]["ENDID"],
                    'startT':startT,
                    'endT':endT,
                    'Tip':"<b>Add Clip Time Frame</b>\nAdding the Start and End frames.\n<b>Start :{0}\nEnd :{1}</b>".format(startT, endT)
                    }
        }
    ids.update(d1)
    # Update Time Button [This how the Tip is update by refresh the group the icon is in].
    self.LayoutFlushGroup(ids["timeBTN"]["BTNGRP"])
    AddUiBtn(self, ids['timeBTN'])
    self.LayoutChanged(1098) # Update UI
    return True

# Editing and Geting the AnimClip Name Method Function
def EditAnimClipName(self, ids):
    """ Editing the AnimTimeClip [ Start Time ] and [ End Time ] System Method Function """
    """ Update Node Data Structure """
    d1={
        "editBOX"    :{ 'ID':ids["editBOX"]["ID"],
                        'UT':"STRING", 
                        'IV':self.GetString(ids["editBOX"]["ID"]) 
                        },
        }
    ids.update(d1)
    return True

# Editing and Geting the AnimClip Name Method Function
def EditAnimClipChecked(self, ids):
    """ Editing the AnimTimeClip [ Start Time ] and [ End Time ] System Method Function """
    """ Update Node Data Structure """
    d1={
        "takeCHK":{ 'ID':ids["takeCHK"]["ID"],
                    'UT':"BOOL", 
                    'IV':self.GetBool(ids["takeCHK"]["ID"]) 
                    },
        }
    ids.update(d1)
    return True

# Link Button Toggle Method Function
def GetLinkObject(self, ids, defIcon, toggleIcon):
    """ W.I.P Method """
    linkObjName = None
    GUID = None
    timeInfo = "<b>Link Object : Empty </b>"
    icon = ids["linkBTN"]["I"]

    # Get Linked Object
    linkObj = self.FindCustomGui(ids["linkBTN"]["LID"], c4d.CUSTOMGUI_LINKBOX).GetLink()
    if linkObj:
        GUID = linkObj.GetGUID()
        linkObjName = linkObj.GetName()
        timeInfo = "<b>Link Object : </b>{0}\n<b>Object ID : </b>{1}".format(linkObj.GetName(), GUID)
        icon = toggleIcon
             
    else:
        icon = defIcon
        pass

    d1={ "linkBTN":{'ID':ids["linkBTN"]["ID"],
                    'UT':"BUTTON",
                    'IV':ids["linkBTN"]["IV"],
                    'I':icon,
                    'BTNGRP':ids["linkBTN"]["BTNGRP"],
                    'GRPID':ids["linkBTN"]["GRPID"],
                    'GRPSTATE':ids["linkBTN"]["GRPSTATE"],
                    'LID':ids["linkBTN"]["LID"],
                    'LOBJ':linkObjName,
                    'LOBJGUID':GUID,
                    'Tip':timeInfo},
        }
    ids.update(d1)

    # Update Time Button [This how the Tip is update by refresh the group the icon is in].
    self.LayoutFlushGroup(ids["linkBTN"]["BTNGRP"])
    AddUiBtn(self, ids['linkBTN'])
    self.LayoutChanged(1098) # Update UI

    return True


""" 
Exporting / Importing
_________________________________________________________________ 
"""

def newSolo_docTemp(doc, docData):
    
    # Get Models form the Object Manager.
    objs = doc.GetSelection()

    # Solo the selected object.
    docTemp = c4d.documents.IsolateObjects(doc, objs)
    if docTemp == None:
        return False
    
    # Set Document Data.
    docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, docData) 
    
    #get_docTemp_Data = c4d.documents.GetActiveDocument()   
    
    return docTemp #get_docTemp_Data


    StartFrame = c4d.BaseTime(self.GetLong(Start), self.GetLong(self.fps))
    EndFrame = c4d.BaseTime(self.GetLong(END), self.GetLong(self.fps))
    new_doc[c4d.DOCUMENT_MINTIME] = StartFrame
    new_doc[c4d.DOCUMENT_MAXTIME] = EndFrame
    new_doc[c4d.DOCUMENT_LOOPMINTIME] = StartFrame
    new_doc[c4d.DOCUMENT_LOOPMAXTIME] = EndFrame 

def newSolo_docTemp_ClipTakes(doc, docData, fps, startTime, endTime):
    
    # Get Models form the Object Manager.
    objs = doc.GetSelection()

    # Solo the selected object.
    docTemp = c4d.documents.IsolateObjects(doc, objs)
    if docTemp == None:
        return False
    
    docTemp[c4d.DOCUMENT_FPS]

    StartFrame = c4d.BaseTime(int(startTime), fps)
    EndFrame = c4d.BaseTime(int(endTime), fps)

    docTemp[c4d.DOCUMENT_MINTIME] = StartFrame
    docTemp[c4d.DOCUMENT_MAXTIME] = EndFrame
    docTemp[c4d.DOCUMENT_LOOPMINTIME] = StartFrame
    docTemp[c4d.DOCUMENT_LOOPMAXTIME] = EndFrame 

    # Set Document Data.
    docTemp.SetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT, docData) 
    
    #get_docTemp_Data = c4d.documents.GetActiveDocument()   
    
    return docTemp #get_docTemp_Data

def ConvertingtoUnityAxis(self, obj, obj_name, SNG_COMPLIER):
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False

    # Get Objects form the Object Manager     
    list_obj = obj

    list_obj[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=3.141592653589793

    Obj_name = list_obj.GetName()
    
    # Check object for children
    ChildrenObjs = list_obj.GetChildren()

    get_amount = len(ChildrenObjs)
    #print(str(get_amount))

    if get_amount > 0:
        
        list_obj.InsertUnder(SNG_COMPLIER)
        
        # Make Temp Null Group.
        UnityAxisNull = c4d.BaseObject(c4d.Onull)
        UnityAxisNull.SetName("UnityAxisNull")
        doc.InsertObject(UnityAxisNull)
        UnityAxisNull.InsertUnder(SNG_COMPLIER)        
        
        childrenHolderNull = c4d.BaseObject(c4d.Onull)
        childrenHolderNull.SetName("obj_Children_holder")
        doc.InsertObject( childrenHolderNull)
        childrenHolderNull.InsertUnder(SNG_COMPLIER)
        for e in ChildrenObjs:
            e.InsertUnder(childrenHolderNull)

        list_obj.InsertUnder(UnityAxisNull)
        
        doc.SetActiveObject(UnityAxisNull)
        
        c4d.CallCommand(1036583)

        get_obj_sys = doc.GetActiveObject()
        get_obj_sys.SetName(Obj_name+"_sng")
        
        doc.SetActiveObject(childrenHolderNull)
        get_obj = doc.GetActiveObject()
        get_obj[c4d.ID_BASEOBJECT_REL_ROTATION,c4d.VECTOR_X]=3.141592653589793
        c4d.EventAdd()
        
        doc.SetActiveObject(get_obj_sys)
        
        doc.SetActiveObject(childrenHolderNull)
        get_objs2 = doc.GetActiveObject()
        get_objs2.InsertUnder(get_obj_sys)
        c4d.CallCommand(1019951, 1019951) # Delete Without Children
        #get_objs2.Remove()

        doc.SetActiveObject(get_obj_sys)

    else:
        list_obj.InsertUnder(SNG_COMPLIER)
        
        # Make Temp Null Group.
        UnityAxisNull = c4d.BaseObject(c4d.Onull)
        UnityAxisNull.SetName("UnityAxisNull")
        doc.InsertObject(UnityAxisNull)
        UnityAxisNull.InsertUnder(SNG_COMPLIER)             

        list_obj.InsertUnder(UnityAxisNull)
        
        doc.SetActiveObject(UnityAxisNull)
        
        c4d.CallCommand(1036583)

        get_obj_sys = doc.GetActiveObject()
        get_obj_sys.SetName(Obj_name+"_sng")

        doc.SetActiveObject(get_obj_sys)

    return True

def UiExporting(self):
    """ For File Exporting Function """

    if self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX) == "":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return    

    if self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX) == "None":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return  

    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
    # Get Objects form the Object Manager     
    list_objs = doc.GetActiveObjects(1)
    if not list_objs:
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "Please select an Object!")
        return


    popupWin = ExportFileNameWindowUi.FileNameMainWindow()
    popupWin.openWindowUI(cLib.DLG_TYPE4)
    if ExportFileNameWindowUi.FILENAME == None:
        return


    # Create the complier group.
    SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
    SNG_COMPLIER.SetName(ExportFileNameWindowUi.FILENAME)
    doc.InsertObject(SNG_COMPLIER)  

    #global fileSeparator 
    ###########################################################
    # Get Path and Make File Extention of Export File
    ###########################################################
    for E in list_objs:
        # Get Object and make a clone of the object
        CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
        doc.InsertObject(CloneTemp)
        CloneTemp.InsertUnder(SNG_COMPLIER)

        # Get Oject Name, Get Object and Set Object as Active.
        #get_E = E.GetName()

        # Center object on export is Checked.
        if self.GetBool(fcsID.ID_SNG_OPT_CHK_CENTER_OBJ)==True:
            doc.SetActiveObject(CloneTemp) 
            c4d.CallCommand(1019940) # Reset PSR

        #if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==True:
        #    self.ConvertingtoUnityAxis(obj=CloneTemp, obj_name=get_E, SNG_COMPLIER=SNG_COMPLIER)
        #if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==False:
        #    doc.SetActiveObject(CloneTemp)
        #    get_obj_sys = doc.GetActiveObject()
        #    get_obj_sys.SetName(get_E+"_sng")

    ################################################################################
    # Check for Export ID , has been chosse in the list function.
    # Set FileName with Get Object Name and Make File Extention of Export File. 
    ################################################################################
    for SelFilter in filterlib.ExportFilters:

        if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:

            #print(SelFilter['id'])

            ExportPLUGIN_ID = SelFilter['export_id']

            filename = ExportFileNameWindowUi.FILENAME + SelFilter['extstr']

            doc.SetActiveObject(SNG_COMPLIER)
            l_obj = doc.GetActiveObject()

            docTemp = newSolo_docTemp(doc=doc, docData=docActiveUnit)

            # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
            plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
            if plug is None:
                raise RuntimeError("Failed to retrieve the format scene_saver_filter.")

            # # Get User file export folder path from Directory for file.
            userpath = os.path.join(self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX), filename)
            if not userpath:
                # Check and Remove Temp Clone Object.
                doc.SetActiveObject(SNG_COMPLIER)
                ActObj = doc.GetActiveObject()
                ActObj.Remove()                
                return

            data = dict()
            # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
            if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                #print data
                if "imexporter" not in data:
                    return

                if c4d.GetC4DVersion() > 17016:
                    # BaseList2D object stored in "imexporter" key hold the settings
                    fExport = data["imexporter"]
                    if fExport is None:
                        return        
                    fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
                    fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False

                # Finally export the document
                if documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                    print "Document successfully exported to:"
                    print userpath
                    # Check and Remove Temp Clone Object.
                    doc.SetActiveObject(SNG_COMPLIER)
                    ActObj = doc.GetActiveObject()
                    ActObj.Remove()
                    cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, filename +" Export Finish successfully")
                    OpenExportFolderAfterExport(self)

                else:
                    # Check and Remove Temp Clone Object.
                    doc.SetActiveObject(SNG_COMPLIER)
                    ActObj = doc.GetActiveObject()
                    ActObj.Remove()
                    cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "File Export failed!")
                    raise RuntimeError("Couldn't export, due to Directory field is empty.")

    c4d.EventAdd()
    return True

def UiExportingSelectionSeparate(self):
    """ For File Exporting Function """

    if self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX) == "":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return    

    if self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX) == "None":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return  

    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)

    # Get Objects form the Object Manager     
    list_objs = doc.GetActiveObjects(1)
    if not list_objs:
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "Please select an Object!")
        return

    #global fileSeparator 
    ###########################################################
    # Get Path and Make File Extention of Export File
    ###########################################################
    for E in list_objs:

        # Create the complier group.
        SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
        SNG_COMPLIER.SetName(E.GetName())
        doc.InsertObject(SNG_COMPLIER)          

        # Get Object and make a clone of the object
        CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
        doc.InsertObject(CloneTemp)
        CloneTemp.InsertUnder(SNG_COMPLIER)

        # Get Oject Name, Get Object and Set Object as Active.
        #get_E = E.GetName()

        # Center object on export is Checked.
        if self.GetBool(fcsID.ID_SNG_OPT_CHK_CENTER_OBJ)==True:
            doc.SetActiveObject(CloneTemp) 
            c4d.CallCommand(1019940) # Reset PSR

        #if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==True:
        #    self.ConvertingtoUnityAxis(obj=CloneTemp, obj_name=get_E, SNG_COMPLIER=SNG_COMPLIER)
        #if self.GetBool(fcsID.ID_SNG_OPT_CHK_UNITY_AXIS)==False:
        #    doc.SetActiveObject(CloneTemp)
        #    get_obj_sys = doc.GetActiveObject()
        #    get_obj_sys.SetName(get_E+"_sng")

        ################################################################################
        # Check for Export ID , has been chosse in the list function.
        # Set FileName with Get Object Name and Make File Extention of Export File. 
        ################################################################################
        for SelFilter in filterlib.ExportFilters:

            if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:

                #print(SelFilter['id'])

                ExportPLUGIN_ID = SelFilter['export_id']

                filename = SNG_COMPLIER.GetName() + SelFilter['extstr']

                doc.SetActiveObject(SNG_COMPLIER)
                l_obj = doc.GetActiveObject()

                docTemp = newSolo_docTemp(doc=doc, docData=docActiveUnit)

                # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
                plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
                if plug is None:
                    raise RuntimeError("Failed to retrieve the format scene_saver_filter.")

                # # Get User file export folder path from Directory for file.
                userpath = os.path.join(self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX), filename)
                if not userpath:
                    return

                data = dict()
                # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
                if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                    #print data
                    if "imexporter" not in data:
                        return

                    if c4d.GetC4DVersion() > 17016:
                        # BaseList2D object stored in "imexporter" key hold the settings
                        fExport = data["imexporter"]
                        if fExport is None:
                            return        
                        fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
                        fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False
                    
                    # Finally export the document
                    if documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                        # Check and Remove Temp Clone Object.
                        doc.SetActiveObject(SNG_COMPLIER)
                        ActObj = doc.GetActiveObject()
                        ActObj.Remove()
                    else:
                        # Check and Remove Temp Clone Object.
                        doc.SetActiveObject(SNG_COMPLIER)
                        ActObj = doc.GetActiveObject()
                        ActObj.Remove()
                        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "File Export failed!")
                        raise RuntimeError("Couldn't export, due to Directory field is empty.")

    cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "All Files Export Finish successfully") 

    if self.GetBool(fcsID.ID_SNG_OPT_CHK_EXPORT_POPUP) == True: 
        popupWin = ExportedPopupWindowUi.MsgPopupMainWindow("All files successfully exported.")
        popupWin.openWindowUI(cLib.DLG_TYPE4) 

    OpenExportFolderAfterExport(self)
    c4d.EventAdd()
    return True

def UiBatchImporting(self, fn, filterId, extensionStr):
    """ For Batch Import Function """
    doc = c4d.documents.GetActiveDocument()
    path = self.GetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX)
    fileName = fn + extensionStr

    getImportFile = os.path.join(path, fileName)

    importPLUGIN_ID = int(filterId)

    plug = plugins.FindPlugin(importPLUGIN_ID, c4d.PLUGINTYPE_SCENELOADER)
    if plug is None:
        return

    SCENEFILTER = None
    if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT_WMAT) == True:
        SCENEFILTER = c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_MATERIALS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE
    else:
        SCENEFILTER = c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE

    c4d.documents.MergeDocument(doc, getImportFile, SCENEFILTER, None)

    c4d.EventAdd()
    return True

def UiImporting(self):
    """ Basic File Import Function """
    doc = c4d.documents.GetActiveDocument()
    # Check for Export ID , has been chosse in the list function 
    #for SelFilter in filterlib.ImportFilters:
    #    if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:
    #        importPLUGIN_ID = SelFilter['import_id']
    #        plug = plugins.FindPlugin(importPLUGIN_ID, c4d.PLUGINTYPE_SCENELOADER)
    #        if plug is None:
    #            return

    # Get a path to load the imported file
    selectedFile = c4d.storage.LoadDialog(title="Load File for Import", type=c4d.FILESELECTTYPE_ANYTHING, force_suffix="")
    if selectedFile is None:
        return

    SCENEFILTER = None
    if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT_WMAT) == True:
        SCENEFILTER = c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_MATERIALS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE
    else:
        SCENEFILTER = c4d.SCENEFILTER_OBJECTS|c4d.SCENEFILTER_PROGRESSALLOWED|c4d.SCENEFILTER_MERGESCENE
    
    c4d.documents.MergeDocument(doc, selectedFile, SCENEFILTER, None)
    c4d.EventAdd()
    return True

def ExportingDefaultC4DExportFilters(SelFilter):
    """ For File Exporting Function """
    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
    
    # Get Objects form the Object Manager     
    list_objs = doc.GetActiveObjects(1)
    if not list_objs:
        #g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_RED_COL, "Please select an Object!")
        return

    ################################################################################
    # Check for Export ID , has been chosse in the list function.
    # Set FileName with Get Object Name and Make File Extention of Export File. 
    ################################################################################
    ExportPLUGIN_ID = SelFilter['export_id']

    # Get Alembic export plugin, 1028082 is its ID
    plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
    if plug is None:
        return
    
    # Get a path to save the exported file
    filePath = storage.LoadDialog(title="Save File for Export", flags=c4d.FILESELECT_SAVE, force_suffix=SelFilter['ext'])
    if filePath is None:
        return


    # Create the complier group.
    SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
    SNG_COMPLIER.SetName(os.path.basename(filePath).split(".")[0])
    doc.InsertObject(SNG_COMPLIER)     

    #global fileSeparator 
    ###########################################################
    # Get Path and Make File Extention of Export File
    ###########################################################
    for E in list_objs:
        # Get Object and make a clone of the object
        CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
        doc.InsertObject(CloneTemp)
        CloneTemp.InsertUnder(SNG_COMPLIER)

        # Get Oject Name, Get Object and Set Object as Active.
        #get_E = E.GetName()
        
    doc.SearchObject(SNG_COMPLIER)
    doc.SetActiveObject(SNG_COMPLIER)
    doc.GetActiveObject()

    docTemp = newSolo_docTemp(doc=doc, docData=docActiveUnit)

    data = dict()
    # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
    if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
        #print data
        if "imexporter" not in data:
            return

        if c4d.GetC4DVersion() > 17016:
            # BaseList2D object stored in "imexporter" key hold the settings
            fExport = data["imexporter"]
            if fExport is None:
                return        
            fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
            fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False
        
        # Finally export the document
        if c4d.documents.SaveDocument(docTemp, filePath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
            print "Document successfully exported to:"
            print filePath
            # Check and Remove Temp Clone Object.
            doc.SetActiveObject(SNG_COMPLIER)
            ActObj = doc.GetActiveObject()
            print(ActObj)
            ActObj.Remove()
            if ExportedPopupWindowUi.opendlg == True:
                popupWin = ExportedPopupWindowUi.MsgPopupMainWindow(os.path.basename(filePath) + " file successfully exported.")
                popupWin.openWindowUI(cLib.DLG_TYPE4)

        else:
            # Check and Remove Temp Clone Object.
            doc.SetActiveObject(SNG_COMPLIER)
            ActObj = doc.GetActiveObject()
            ActObj.Remove() 
            popupWin = ExportedPopupWindowUi.MsgPopupMainWindow("File Export failed!")
            popupWin.openWindowUI(cLib.DLG_TYPE4)

    c4d.EventAdd()
    return True

def ExportingPresetsExportFilters(PRESETS_PATH, i):
    """ For File Exporting Function """

    p = os.path.join(PRESETS_PATH, i['presetFile'])

    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)

    # Get Objects form the Object Manager     
    list_objs = doc.GetActiveObjects(1)
    if not list_objs:
        #cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "Please select an Object!")
        return

    exportPath = None
    exportfileName = None

    if jEdit.GetItemProperty(p, "ModelExportPath") == "None":
        filePath = storage.LoadDialog(title="Save File for Export", flags=c4d.FILESELECT_SAVE, force_suffix=jEdit.GetItemProperty(p, "ExportFormatExt"))
        if filePath is None:
            return 
        exportPath = os.path.dirname(filePath)  
        exportfileName = os.path.basename(filePath).split(".")[0]

    else:
        exportPath = jEdit.GetItemProperty(p, "ModelExportPath")
        if exportPath:
            popupWin = ExportFileNameWindowUi.FileNameMainWindow()
            popupWin.openWindowUI(cLib.DLG_TYPE4)
            if ExportFileNameWindowUi.FILENAME == None:
                return
            exportfileName = ExportFileNameWindowUi.FILENAME


    # Create the complier group.
    SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
    SNG_COMPLIER.SetName(exportfileName)
    doc.InsertObject(SNG_COMPLIER)  

    ###########################################################
    # Get Path and Make File Extention of Export File
    ###########################################################
    for E in list_objs:
        # Get Object and make a clone of the object
        CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
        doc.InsertObject(CloneTemp)
        CloneTemp.InsertUnder(SNG_COMPLIER)

        # Center object on export is Checked.
        d = jEdit.GetItemProperty(p, "CenterExportedObjCheck")
        if jEdit.BoolCheckerHelper(d) == True:
            doc.SetActiveObject(CloneTemp) 
            c4d.CallCommand(1019940) # Reset PSR


    ################################################################################
    # Check for Export ID , has been chosse in the list function.
    # Set FileName with Get Object Name and Make File Extention of Export File. 
    ################################################################################
    for SelFilter in filterlib.ExportFilters:

        if jEdit.GetItemProperty(p, "ExportFormat") == SelFilter['str']:

            ExportPLUGIN_ID = SelFilter['export_id']

            filename = exportfileName + SelFilter['extstr']

            doc.SetActiveObject(SNG_COMPLIER)
            l_obj = doc.GetActiveObject()

            docTemp = newSolo_docTemp(doc=doc, docData=docActiveUnit)

            # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
            plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
            if plug is None:
                raise RuntimeError("Failed to retrieve the format scene_saver_filter.")

            # # Get User file export folder path from Directory for file.
            userpath = os.path.join(exportPath, filename)
            if not userpath:
                return

            data = dict()
            # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
            if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                #print data
                if "imexporter" not in data:
                    return

                if c4d.GetC4DVersion() > 17016:
                    # BaseList2D object stored in "imexporter" key hold the settings
                    fExport = data["imexporter"]
                    if fExport is None:
                        return        
                    fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
                    fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False
                
                # Finally export the document
                if documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                    print "Document successfully exported to:"
                    print userpath
                    # Check and Remove Temp Clone Object.
                    doc.SetActiveObject(SNG_COMPLIER)
                    ActObj = doc.GetActiveObject()
                    ActObj.Remove()

                    if jEdit.BoolCheckerHelper(jEdit.GetItemProperty(p, "ShowExportedFileCheck")) == True:
                        popupWin = ExportedPopupWindowUi.MsgPopupMainWindow(filename + " file successfully exported.")
                        popupWin.openWindowUI(cLib.DLG_TYPE4)    

                    if jEdit.BoolCheckerHelper(jEdit.GetItemProperty(p, "OpenExportPathCheck")) == True:
                        c4d.storage.ShowInFinder(exportPath, False)                                    

                else:
                    # Check and Remove Temp Clone Object.
                    doc.SetActiveObject(SNG_COMPLIER)
                    ActObj = doc.GetActiveObject()
                    ActObj.Remove()
                    popupWin = ExportedPopupWindowUi.MsgPopupMainWindow("File Export failed!")
                    popupWin.openWindowUI(cLib.DLG_TYPE4)
                    raise RuntimeError("Couldn't export, due to Directory field is empty.")

    c4d.EventAdd()
    return True

def ExportingPresetsExportFiltersSelectionSeparate(PRESETS_PATH, i):
    """ For File Exporting Function """

    p = os.path.join(PRESETS_PATH, i['presetFile'])

    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)

    # Get Objects form the Object Manager     
    list_objs = doc.GetActiveObjects(1)
    if not list_objs:
        #cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "Please select an Object!")
        return

    exportPath = None

    if jEdit.GetItemProperty(p, "ModelExportPath") == "None":
        filePath = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Export Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
        if filePath is None:
            return 
        exportPath = filePath
    else:
        exportPath = jEdit.GetItemProperty(p, "ModelExportPath")

    ###########################################################
    # Get Path and Make File Extention of Export File
    ###########################################################
    for E in list_objs:

        # Create the complier group.
        SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
        SNG_COMPLIER.SetName(E.GetName())
        doc.InsertObject(SNG_COMPLIER)

        # Get Object and make a clone of the object
        CloneTemp = E.GetClone(c4d.COPYFLAGS_0)
        doc.InsertObject(CloneTemp)
        CloneTemp.InsertUnder(SNG_COMPLIER)

        # Center object on export is Checked.
        d = jEdit.GetItemProperty(p, "CenterExportedObjCheck")
        if jEdit.BoolCheckerHelper(d) == True:
            doc.SetActiveObject(CloneTemp) 
            c4d.CallCommand(1019940) # Reset PSR


        ################################################################################
        # Check for Export ID , has been chosse in the list function.
        # Set FileName with Get Object Name and Make File Extention of Export File. 
        ################################################################################
        for SelFilter in filterlib.ExportFilters:

            if jEdit.GetItemProperty(p, "ExportFormat") == SelFilter['str']:

                ExportPLUGIN_ID = SelFilter['export_id']

                filename =  SNG_COMPLIER.GetName() + SelFilter['extstr']

                doc.SetActiveObject(SNG_COMPLIER)
                l_obj = doc.GetActiveObject()

                docTemp = newSolo_docTemp(doc=doc, docData=docActiveUnit)

                # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
                plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
                if plug is None:
                    raise RuntimeError("Failed to retrieve the format scene_saver_filter.")

                # # Get User file export folder path from Directory for file.
                userpath = os.path.join(exportPath, filename)
                if not userpath:
                    return

                data = dict()
                # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
                if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                    #print data
                    if "imexporter" not in data:
                        return

                    if c4d.GetC4DVersion() > 17016:
                        # BaseList2D object stored in "imexporter" key hold the settings
                        fExport = data["imexporter"]
                        if fExport is None:
                            return        
                        fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
                        fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False
                    
                    # Finally export the document
                    if documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                        print "Document successfully exported to:"
                        print userpath
                        # Check and Remove Temp Clone Object.
                        doc.SetActiveObject(SNG_COMPLIER)
                        ActObj = doc.GetActiveObject()
                        ActObj.Remove()

                        if jEdit.BoolCheckerHelper(jEdit.GetItemProperty(p, "ShowExportedFileCheck")) == True:
                            popupWin = ExportedPopupWindowUi.MsgPopupMainWindow(filename + " file successfully exported.")
                            popupWin.openWindowUI(cLib.DLG_TYPE4)                    

                    else:
                        # Check and Remove Temp Clone Object.
                        doc.SetActiveObject(SNG_COMPLIER)
                        ActObj = doc.GetActiveObject()
                        ActObj.Remove()
                        popupWin = ExportedPopupWindowUi.MsgPopupMainWindow("File Export failed!")
                        popupWin.openWindowUI(cLib.DLG_TYPE4)
                        raise RuntimeError("Couldn't export, due to Directory field is empty.")
    
    if jEdit.BoolCheckerHelper(jEdit.GetItemProperty(p, "OpenExportPathCheck")) == True:
        c4d.storage.ShowInFinder(exportPath, False)

    c4d.EventAdd()
    return True

def UiTakesExporting(self, clipTakesData):
    """ Exporting Anim Clip Takes Function """

    if self.GetString(fcsID.ID_SNG_TAKES_EXPORT_PATH) == "":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return    

    if self.GetString(fcsID.ID_SNG_TAKES_EXPORT_PATH) == "None":
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "You dont have a directory added")
        return  

    ###########################################################
    # Start Export Function 
    ###########################################################
    doc = c4d.documents.GetActiveDocument()
    if doc == None:
        return False
    # Get Active Document Data.     
    docActiveUnit = doc.GetDocumentData(c4d.DOCUMENTSETTINGS_DOCUMENT)
    fps = doc[c4d.DOCUMENT_FPS]

    if clipTakesData:

        for clip in clipTakesData:

            if self.GetBool(clip["takeCHK"]["ID"]) == True:

                # Create the complier group.
                SNG_COMPLIER = c4d.BaseObject(c4d.Onull)
                SNG_COMPLIER.SetName(clip["editBOX"]["IV"])
                doc.InsertObject(SNG_COMPLIER)          

                # Get Linked Object
                linkObj = self.FindCustomGui(clip["linkBTN"]["LID"], c4d.CUSTOMGUI_LINKBOX).GetLink()
                if linkObj:
                    GUID = linkObj.GetGUID()
                    linkObjName = linkObj.GetName()

                # Get Object and make a clone of the object
                CloneTemp = linkObj.GetClone(c4d.COPYFLAGS_0)
                doc.InsertObject(CloneTemp)
                CloneTemp.InsertUnder(SNG_COMPLIER)


                ################################################################################
                # Check for Export ID , has been chosse in the list function.
                # Set FileName with Get Object Name and Make File Extention of Export File. 
                ################################################################################
                for SelFilter in filterlib.ExportFiltersC4:

                    if self.GetLong(clip["format"]["ID"])==SelFilter['id']:

                        #print(SelFilter['id'])

                        ExportPLUGIN_ID = SelFilter['export_id']

                        filename = SNG_COMPLIER.GetName() + SelFilter['extstr']

                        doc.SetActiveObject(SNG_COMPLIER)
                        l_obj = doc.GetActiveObject()

                        docTemp = newSolo_docTemp_ClipTakes(doc=doc, docData=docActiveUnit, fps=fps, startTime=clip["timeBTN"]["startT"], endTime=clip["timeBTN"]["endT"])

                        # Get export plugin, For Example : DAR1.4:1022316 or FBX:1026370 is its ID
                        plug = plugins.FindPlugin(ExportPLUGIN_ID, c4d.PLUGINTYPE_SCENESAVER)
                        if plug is None:
                            raise RuntimeError("Failed to retrieve the format scene_saver_filter.")

                        # # Get User file export folder path from Directory for file.
                        userpath = os.path.join(self.GetString(fcsID.ID_SNG_TAKES_EXPORT_PATH), filename)
                        if not userpath:
                            return

                        data = dict()
                        # Send MSG_RETRIEVEPRIVATEDATA to Alembic export plugin
                        if plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, data):
                            #print data
                            if "imexporter" not in data:
                                return

                            if c4d.GetC4DVersion() > 17016:
                                # BaseList2D object stored in "imexporter" key hold the settings
                                fExport = data["imexporter"]
                                if fExport is None:
                                    return        
                                fExport[c4d.FBXEXPORT_SELECTION_ONLY]=False
                                fExport[c4d.ABCEXPORT_SELECTION_ONLY]=False
                            
                            # Finally export the document
                            if documents.SaveDocument(docTemp, userpath, c4d.SAVEDOCUMENTFLAGS_DONTADDTORECENTLIST, ExportPLUGIN_ID):
                                # Check and Remove Temp Clone Object.
                                doc.SetActiveObject(SNG_COMPLIER)
                                ActObj = doc.GetActiveObject()
                                ActObj.Remove()
                            else:
                                # Check and Remove Temp Clone Object.
                                doc.SetActiveObject(SNG_COMPLIER)
                                ActObj = doc.GetActiveObject()
                                ActObj.Remove()
                                cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "File Export failed!")
                                raise RuntimeError("Couldn't export, due to Directory field is empty.")

            cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "All Files Export Finish successfully") 

        popupWin = ExportedTakesPopupWindowUi.MsgPopupMainWindow("All files successfully exported.", self.GetString(fcsID.ID_SNG_TAKES_EXPORT_PATH))
        popupWin.openWindowUI(cLib.DLG_TYPE4) 
    else:
        pass

    c4d.EventAdd()
    return True
