"""
Directory & Files Hepler Library Module
"""

import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime

class DirectoryHepler(object):

    def IsDirectoryVaild(self, DIR):
        """ Check If Directory is There or Not. And (rv) means ``ReturnValue.`` """
        rv = None
        if not os.path.exists(DIR):
            rv = None
        else:
            rv = DIR
        return rv

    def GetAllFilesInDirectory(self, DIR, LISTOFFILES, Extension=None):
        """ 
        Getting and Looking for all files in folder. and this method returns the list of files ``LISTOFFILES``.
        For Extension Att.. is for what type of file extentsion you only want to collect out of the dir folder. ``Eg: "*".txt``.
        ``CodeEg: GetAllFilesInDirectory(DIR=[YourPath], LISTOFFILES=[YourList], Extension=["*".txt])``
        """   
        os.chdir(DIR)
        for each_file in glob.glob(Extension):
            LISTOFFILES.append(each_file)
        return True

    def GetAllFilesInDirectoryAsList(self, DIR, Extension=None):
        """ 
        V2 (More Cleaner)
        Getting and Looking for all files in folder. and this method returns the list of files ``LISTOFFILES``.
        For Extension Att.. is for what type of file extentsion you only want to collect out of the dir folder. ``Eg: "*".txt``.
        ``CodeEg: GetAllFilesInDirectory(DIR=[YourPath], Extension=["*".txt])``
        """
        LISTOFFILES = []   
        os.chdir(DIR)
        for each_file in glob.glob(Extension):
            LISTOFFILES.append(each_file)
        return LISTOFFILES


    