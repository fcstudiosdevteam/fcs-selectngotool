"""
Sat To G-Poly
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys
import math 
import glob 
import time
import datetime
# FCS Imports Modules .py Files.
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib
from snglib.CoreTools.Utilites.Helpers import SelectNGoCoreLib as snglib
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()
jEdit = JsonSysHelperLib.FCS_JsonSystem_Editor()


"""
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
class AddCustomFormatMainWindow(cLib.BaseWindowDialogUI):
    """ MSG Popup GUI Window """
    
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = None
    windowDialogWidthSize = 100
    windowDialogHeightSize = 15

    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_EDIT_ID = 1008
    UI_EDIT_ID1 = 1009
    UI_EDIT_ID2 = 1010
    UI_EDIT_ID3 = 1011

    UI_BTN_ID = 1012
    UI_CNL_ID = 1013

    """
    Main GeDialog GUI Window Overrides.
    _________________________________________________
    """
    def __init__(self, path, formatTpye):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(AddCustomFormatMainWindow, self).__init__()
        self.presetPath = path
        self.formatTile = formatTpye
        self.TileBar = cLib.AddCustomQuickTab_GUI(self)

    def BuildUI(self):

        self.windowMainTileName = "Create Custom " + self.formatTile + " Scene Format Filter"

        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")


        self.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")

        self.GroupBorderSpace(3, 3, 3, 3)
        
        self.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_TOP, 1, 0, "")
        self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name="Format Filename : ", width=290, height=0, ui_color=cLib.BG_LightDarker_COL)
        self.AddEditText(self.UI_EDIT_ID, c4d.BFH_SCALEFIT, 90, 15)
        self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name="Format Display Name : ", width=290, height=0, ui_color=cLib.BG_LightDarker_COL)
        self.AddEditText(self.UI_EDIT_ID1, c4d.BFH_SCALEFIT, 90, 15)        
        self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name="Format ID : ", width=290, height=0, ui_color=cLib.BG_LightDarker_COL)
        self.AddEditText(self.UI_EDIT_ID2, c4d.BFH_SCALEFIT, 90, 15)    
        self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name="Format Extension : ", width=290, height=0, ui_color=cLib.BG_LightDarker_COL)
        self.AddEditText(self.UI_EDIT_ID3, c4d.BFH_SCALEFIT, 90, 15)             
        self.GroupEnd()

        self.AddSeparatorV(0, c4d.BFV_SCALEFIT) # Separator

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name="All Formats Installed :", width=290, height=0, ui_color=cLib.BG_LightDarker_COL)
        self.AddMultiLineEditText(108, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 360, 200, c4d.DR_MULTILINE_READONLY|c4d.DR_MULTILINE_MONOSPACED)
        self.GroupEnd()

        self.GroupEnd()


        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.GroupBegin(0, c4d.BFH_CENTER, 2, 0, "")
        self.AddButton(self.UI_BTN_ID, c4d.BFH_CENTER, 0, 15, name="Add")
        self.AddButton(self.UI_CNL_ID, c4d.BFH_CENTER, 0, 15, name="Cancel")
        self.GroupEnd()


        self.GroupEnd()

        return super(AddCustomFormatMainWindow, self).BuildUI()

    def UIsettings(self):

        if self.formatTile == "Export":
            for p in c4d.plugins.FilterPluginList(c4d.PLUGINTYPE_SCENESAVER, True):
                GetData = self.GetString(108)
                self.SetString(108, GetData + "\n" + str(p.GetID()) + " = " + p.GetName())

        if self.formatTile == "Import":
            for p in c4d.plugins.FilterPluginList(c4d.PLUGINTYPE_SCENELOADER, True):
                GetData = self.GetString(108)
                self.SetString(108, GetData + "\n" + str(p.GetID()) + " = " + p.GetName())            

        return super(AddCustomFormatMainWindow, self).UIsettings()

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id == self.UI_BTN_ID:

            if self.formatTile == "Export":
                FormatFileName = self.GetString(self.UI_EDIT_ID)
                data = snglib.CustomFormatFilterFileTemplate(ui_id=self.GetString(self.UI_EDIT_ID2), 
                                                            export_id=self.GetString(self.UI_EDIT_ID2), 
                                                            import_id=0, 
                                                            strtext=self.GetString(self.UI_EDIT_ID1), 
                                                            ext=self.GetString(self.UI_EDIT_ID3), 
                                                            extstr="." + str(self.GetString(self.UI_EDIT_ID3))
                                                            )
                jEdit.SaveFile(os.path.join(self.presetPath, 'sngPrefs', 'SelectNGoExtensions', 'ExportFilters', FormatFileName+".json"), data.__dict__ )

            if self.formatTile == "Import":
                FormatFileName = self.GetString(self.UI_EDIT_ID)
                data = snglib.CustomFormatFilterFileTemplate(ui_id=self.GetString(self.UI_EDIT_ID2), 
                                                            export_id=0, 
                                                            import_id=self.GetString(self.UI_EDIT_ID2), 
                                                            strtext=self.GetString(self.UI_EDIT_ID1), 
                                                            ext=self.GetString(self.UI_EDIT_ID3), 
                                                            extstr="." + str(self.GetString(self.UI_EDIT_ID3))
                                                            )
                jEdit.SaveFile(os.path.join(self.presetPath, 'sngPrefs', 'SelectNGoExtensions', 'ImportFilters', FormatFileName+".json"), data.__dict__ )                

            self.Close()
            c4d.EventAdd()  

        if id == self.UI_CNL_ID:
            self.Close()
            c4d.EventAdd()             

        return True

