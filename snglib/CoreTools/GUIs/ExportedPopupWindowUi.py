"""
Sat To G-Poly
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys
import math 
import glob 
import time
import datetime
# FCS Imports Modules .py Files.
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib

config_data = None
#fcsID = cLib.fcsID
#jsonEdit = cLib.jsonEdit

opendlg = True

"""
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
class MsgPopupMainWindow(cLib.BaseWindowDialogUI):
    """ MSG Popup GUI Window """
    
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Model Exported..."
    windowDialogWidthSize = 200
    windowDialogHeightSize = 10
    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_CHK_ID = 1008
    UI_BTN_ID = 1009

    """
    Main GeDialog GUI Window Overrides.
    _________________________________________________
    """
    def __init__(self, msg):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(MsgPopupMainWindow, self).__init__()
        self.msg = msg

    def BuildUI(self):

        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")

        self.GroupBorderSpace(3, 3, 3, 3)

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, self.msg, c4d.BORDER_WITH_TITLE_BOLD)

        self.AddCheckbox(self.UI_CHK_ID, c4d.BFH_MASK, 0, 0, "Don't show this message again after export.")
        
        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.AddButton(self.UI_BTN_ID, c4d.BFH_CENTER, 0, 15, name="OK")  

        self.GroupEnd()

        return super(MsgPopupMainWindow, self).BuildUI()

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id == self.UI_BTN_ID:
            self.Close()
            c4d.EventAdd()  

        if id == self.UI_CHK_ID:
            global opendlg
            opendlg = self.GetBool(self.UI_BTN_ID)
            c4d.EventAdd() 

        return True

