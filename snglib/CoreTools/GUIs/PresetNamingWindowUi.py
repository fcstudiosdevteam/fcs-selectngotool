"""
Sat To G-Poly
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys
import math 
import glob 
import time
import datetime
# FCS Imports Modules .py Files.
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()
jEdit = JsonSysHelperLib.FCS_JsonSystem_Editor()


"""
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
class FileNameMainWindow(cLib.BaseWindowDialogUI):
    """ MSG Popup GUI Window """
    
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Save Export Preset"
    windowDialogWidthSize = 100
    windowDialogHeightSize = 15

    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_EDIT_ID = 1008
    UI_BTN_ID = 1009
    UI_CNL_ID = 1010

    """
    Main GeDialog GUI Window Overrides.
    _________________________________________________
    """
    def __init__(self, path, data):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(FileNameMainWindow, self).__init__()
        self.presetPath = path
        self.data = data

    def BuildUI(self):

        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 2, 0, "")

        self.GroupBorderSpace(3, 3, 3, 3)

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, "Filename : ", c4d.BORDER_WITH_TITLE_BOLD)

        self.AddEditText(self.UI_EDIT_ID, c4d.BFH_SCALEFIT, 90, 15)

        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.GroupBegin(0, c4d.BFH_CENTER, 2, 0, "")
        self.AddButton(self.UI_BTN_ID, c4d.BFH_CENTER, 0, 15, name="Ok")
        self.AddButton(self.UI_CNL_ID, c4d.BFH_CENTER, 0, 15, name="Cancel")
        self.GroupEnd()

        self.GroupEnd()

        return super(FileNameMainWindow, self).BuildUI()

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id == self.UI_BTN_ID:
            PresetFileName = self.GetString(self.UI_EDIT_ID)
            jEdit.SaveFile(os.path.join(self.presetPath, 'sngPrefs', 'SelectNGoPresetsSaves', PresetFileName+".json"), self.data.__dict__ )
            self.Close()
            c4d.EventAdd()  


        return True

