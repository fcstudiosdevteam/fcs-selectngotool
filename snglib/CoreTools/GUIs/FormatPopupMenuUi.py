
"""
Default Imports
_____________________________________________________________________
"""
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime

"""
Cinema 4D Imports
_____________________________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as dlg
from random import randint

from snglib.CoreTools.GUIs import OptionsPopupMenuUi
from snglib.CoreTools.Utilites.Helpers import SelectNGoCoreLib as snglib
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()
jEdit = JsonSysHelperLib.FCS_JsonSystem_Editor()


def MenuPopupUI(PLUGIN_PATH):

    IDM_ITEM1 = c4d.FIRST_POPUP_ID
    PRESETS_PATH = os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoPresetsSaves')
    PRESETS = []

    # Main menu
    menu = c4d.BaseContainer() 

    num = 0

    if OptionsPopupMenuUi.enablePresetList == True:
        for i in DirHelper.GetAllFilesInDirectoryAsList(DIR=PRESETS_PATH, Extension="*.json"):
            num+=510
            item = { 'id':c4d.FIRST_POPUP_ID+num, 'presetFile':i}
            PRESETS.append(item)
            ext = jEdit.GetItemProperty(os.path.join(PRESETS_PATH, i), "ExportFormatExt")
            menu.InsData(item['id'], '&i1055455&' + i.split(".")[0] + " (*"+ str(ext) +")")
    else:
        for eachFilter in snglib.filterlib.ExportFiltersC1:##ExportFiltersC1
            snglib.AddExportFilter(ComboID=None, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], popupmenu=menu)
        menu.InsData(0, '') # Append separator   
        for eachFilter in snglib.filterlib.ExportFiltersC2:
            snglib.AddExportFilter(ComboID=None, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], popupmenu=menu)        
        menu.InsData(0, '') # Append separator   
        for eachFilter in snglib.filterlib.ExportFiltersC3:
            snglib.AddExportFilter(ComboID=None, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], popupmenu=menu)


    result = c4d.gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)


    if OptionsPopupMenuUi.enablePresetList == True:
        for i in PRESETS:
            if result == i['id']:
                if jEdit.BoolCheckerHelper(jEdit.GetItemProperty(os.path.join(PRESETS_PATH, i['presetFile']), "SeparateCheck")) == True:
                    snglib.ExportingPresetsExportFiltersSelectionSeparate(PRESETS_PATH, i)
                else:
                    snglib.ExportingPresetsExportFilters(PRESETS_PATH, i)

    else:
        for SelFilter in snglib.filterlib.ExportFilters:
            if result == c4d.FIRST_POPUP_ID+SelFilter['id']:
                snglib.ExportingDefaultC4DExportFilters(SelFilter)


    print(result)

    return True