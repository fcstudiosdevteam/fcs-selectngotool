"""
Select N'Go Tool Window GUI Dialog and GameDev Pipline Tools
"""
#  Imports for Cinema 4D
import c4d
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as WindowDialog        # eg:( class My_Dialog(c4d.gui.GeDialog): ) to ( class My_Dialog(WindowDialog): ) Just keeping code line short.
from c4d.plugins import CommandData, TagData, ObjectData
# XML Imports is for Saving and Loading Project Data for Plugin UI / Xml good but long functions to make.
from xml.dom import minidom
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as etree

# FCS Imports Modules .py Files.
from snglib.CoreTools.Utilites.Helpers import SelectNGoCoreLib as snglib
from snglib.CoreTools.Utilites.Helpers.SelectNGoCoreLib import fcsID as fcsID
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib
from snglib.CoreTools.GUIs import PresetNamingWindowUi
from snglib.CoreTools.GUIs import ExportFileNameWindowUi
from snglib.CoreTools.GUIs import ExportedPopupWindowUi
from snglib.CoreTools.GUIs import AddCustomFormatWindowUi
from snglib.CoreTools.GUIs import SavingUserAnimTakesWindowUi
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()
jEdit = JsonSysHelperLib.FCS_JsonSystem_Editor()

g_lib = cLib
PLUGIN_PATH = None

playIcon = None
stopIcon = None
timerIcon = None
linkIcon = None
linkedIcon = None
formatsetIcon = None
menuIcon = None
addIcon = None
SelNGOIcon = None
searchIcon = None
folderIcon = None
recentfolderIcon = None
soloplayIcon = None
soloplayEnableIcon = None

AnimTakeClips = []
RecentList = []
uiIDS = 0
sngRecentDirList = None
soloPreviewState = False

"""
Select N'Go Tool GUI MainWindow User Interface Dialog.
_____________________________________________________________________
"""
class Select_N_Go_Dialog(WindowDialog):
    """ Select N'Go Tool / Our first 3d plugin tool. """

    """
    Main Properties
    _________________________________________________
    """
    BatchList = []
    BatchListItemsUIs = []
    CheckedOnBatchList = []

    OpenDir_UI_BTN = 46554
    AddDir_UI_BTN = 19897
    Center_Obj = 195654
    CHK_ExportTogether = 19877
    OBJName = 19866
    UI_FormatsDropDownMenu = 1001
    UI_Execute_ExportButton = 1002
    UI_FormatsDropDownMenu2 = 1003
    UI_FormatSettingsGearButton = 1004
    UI_BTN_DirLoad = 178337
    Path_Group_ID = 90096
    STATUS_UI_BAR_ID = 9009845
    STATUS_UI_TXT_ID = 9009945
    ID_SNG_OPT_BATCH_CUSTOM_BTN = 9009846
    ID_SNG_TAKES_TAB_GRP = 2041
    ID_SNG_BOTTOM_GRP = 2042
    ID_SNG_STATUS_BAR_GRP = 2043
    ID_SNG_FCSWEBBANNER = 2044
    ID_SNG_OPT_BATCHFITER_MENU_ALL = 10202
    FCS_WEBSITE_GUI_BANNER_ID = 10201



    def SearchFilterBatchFileSystem(self):
        DataAndExtension = None
        SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()
        # Get file 3D format filter extension from the (fcs_Global_EnumsTypes_LIB).
        for filter_extension in snglib.filterlib.ImportFilters:
            itemId = filter_extension['id']
            itemId_ShowAll = self.ID_SNG_OPT_BATCHFITER_MENU_ALL

            if self.GetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU) == itemId_ShowAll:
                DataAndExtension = {'File':SearchBar+"*"+filter_extension['extstr'], 'Ext':filter_extension['extstr'], 'Str':filter_extension['extstr'], 'ID':filter_extension['import_id']}
                GetFilesFromDirectory(ui_instance=self, FileData=DataAndExtension)

            if self.GetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU) == itemId:
                DataAndExtension = {'File':SearchBar+"*"+filter_extension['extstr'], 'Ext':filter_extension['extstr'], 'Str':filter_extension['extstr'], 'ID':filter_extension['import_id']}
                GetFilesFromDirectory(ui_instance=self, FileData=DataAndExtension)

        return DataAndExtension
    def LoadFiles_To_BatchListViewGUI(self):

        SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()

        self.LayoutFlushGroup(fcsID.ID_SNG_OPT_BATCH_LIST) # Refresh Group UI

        if self.BatchList:

            amountOfBathFiles = len(self.BatchList)
            currentNum = 1

            for batchfile in self.BatchList:

                percent = float(currentNum)/amountOfBathFiles*100
                currentNum+=1

                chk_id = GuiIdGenerator(7000000)
                FileNodeGUI_ItemLayout(ui_instance=self, itemData={'GRP_ID':GuiIdGenerator(7000000), 'GRP2_ID':GuiIdGenerator(7000000), 'CHK_ID':chk_id, 'FILE_STR':batchfile['fName'], 'EXT_STR':batchfile['fStrExt']})
                self.BatchListItemsUIs.append({'chkId':chk_id, 'fName':batchfile['fName'], 'fExt':batchfile['fExt'], 'fID':batchfile['fID']})

                g_lib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, g_lib.BG_GREEN_COL, "Loading Batch Files...."+str(percent)+"%")

        self.LayoutChanged(fcsID.ID_SNG_OPT_BATCH_LIST) # Update Group UI
        return True

    """
    Main C4D GUI Dialog Operations Functions which are Methods to 
    Override for GeDialog class.
    _____________________________________________________________________
    """
    def __init__(self, global_strings, res_dlg, plug_PATH):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """    
        super(Select_N_Go_Dialog, self).__init__()
        self.IDS = global_strings # ( c4d_strings.str )
        self.RESDLG = res_dlg

        global PLUGIN_PATH
        PLUGIN_PATH = plug_PATH

        LoadAllUiIcons(plug_PATH)

        global sngRecentDirList
        sngRecentDirList = os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'sngUserRecentDirs.json')
        if os.path.exists(sngRecentDirList):
            jsonFileData = jEdit.LoadFile(sngRecentDirList)
            for user_Dir in jsonFileData:
                RecentList.append(user_Dir)

    def CreateLayout(self):
        """
        Window GUI elements layout that display to the User.
        """
        def AddTopBarMenus():
            self.MenuFlushAll()
            self.MenuSubBegin(self.IDS.ID(fcsID.ID_SNG_MENU_FILE))          # File Menu
            self.MenuAddString(fcsID.ID_SNG_MENU_SAVEPRESET, self.IDS.ID(fcsID.ID_SNG_MENU_SAVEPRESET))
            self.MenuAddSeparator()
            self.MenuSubBegin(self.IDS.ID(fcsID.ID_SNG_MENU_EXP_L_DEF_OPT))
            self.MenuAddString(fcsID.ID_SNG_MENU_EXP_L_DEFOPT_ONE, self.IDS.ID(fcsID.ID_SNG_MENU_EXP_L_DEFOPT_ONE))
            self.MenuAddString(fcsID.ID_SNG_MENU_EXP_L_DEFOPT_TWO, self.IDS.ID(fcsID.ID_SNG_MENU_EXP_L_DEFOPT_TWO))
            self.MenuSubEnd() 
            self.MenuAddSeparator()
            self.MenuAddString(fcsID.ID_SNG_MENU_OPENPRESETS_DIR, self.IDS.ID(fcsID.ID_SNG_MENU_OPENPRESETS_DIR))
            self.MenuAddString(fcsID.ID_SNG_MENU_OPENTAKES_DIR, self.IDS.ID(fcsID.ID_SNG_MENU_OPENTAKES_DIR))
            self.MenuSubBegin(self.IDS.ID(fcsID.ID_SNG_MENU_FILTERS_SAVES))
            self.MenuAddString(fcsID.ID_SNG_MENU_FILTERS_EXP_SAVES, self.IDS.ID(fcsID.ID_SNG_MENU_FILTERS_EXP_SAVES))
            self.MenuAddString(fcsID.ID_SNG_MENU_FILTERS_IMP_SAVES, self.IDS.ID(fcsID.ID_SNG_MENU_FILTERS_IMP_SAVES))
            self.MenuSubEnd() 
            self.MenuSubEnd() 
            # -------------------------#
            self.MenuSubBegin(self.IDS.ID(fcsID.ID_SNG_MENU_EXTENSIONS))     # Extension Menu
            self.MenuAddString(fcsID.ID_SNG_MENU_ADD_EXPORTFILTER, self.IDS.ID(fcsID.ID_SNG_MENU_ADD_EXPORTFILTER))
            self.MenuAddString(fcsID.ID_SNG_MENU_ADD_IMPORTFILTER, self.IDS.ID(fcsID.ID_SNG_MENU_ADD_IMPORTFILTER))        
            self.MenuSubEnd()
            # -------------------------#
            self.MenuSubBegin(self.IDS.ID(fcsID.ID_SNG_MENU_FILE_INFO))     # Help Menu
            self.MenuAddString(fcsID.ID_SNG_MENU_OnManual, self.IDS.ID(fcsID.ID_SNG_MENU_OnManual))
            self.MenuAddString(fcsID.ID_SNG_MENU_OffManual, self.IDS.ID(fcsID.ID_SNG_MENU_OffManual)) 
            self.MenuAddSeparator()
            self.MenuAddString(64559, "FCS Discord")
            self.MenuAddString(64560, "FCS Gumroad")
            self.MenuAddString(64561, "FCS YouTube") 
            self.MenuAddSeparator()
            self.MenuAddString(64562, "About")        
            self.MenuSubEnd()
            self.MenuFinished()
            # -------------------------#
            snglib.AddVersonToMenuBar(ui_instance=self)           
        
        AddTopBarMenus()

        self.RESDLG.ResGUI(self, fcsID.fcs_select_n_go_gui_dlg)

        AddandInjectGUIsWidgets(ui_instance=self)

        return True

    def InitValues(self):
        """ Load UI on Loading Startup as Default """
        
        self.SetDefaultColor(fcsID.ID_SNG_OVERALL_GRP, c4d.COLOR_BG, cLib.BG_DARKER_COL)
        self.SetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)
        self.SetLong(fcsID.ID_SNG_OPT_BATCHFITER_MENU, self.ID_SNG_OPT_BATCHFITER_MENU_ALL)
        self.SetString(self.OBJName, "File Name")
        self.Enable(self.OBJName, False)
        self.SetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX, "Search....")
        self.SetBool(fcsID.ID_SNG_OPT_MODE_EXPORT, True)
        self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, False)  
        return True

    def Command(self, id, msg):

        if (id == fcsID.ID_SNG_OPT_BATCHFITER_MENU):
            self.BatchList = []
            self.BatchListItemsUIs = []
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()
            self.LayoutChanged(fcsID.ID_SNG_OPT_BATCH_LIST)
            self.LayoutChanged(fcsID.ID_SNG_OVERALL_GRP)            

        if (id == fcsID.ID_SNG_OPT_SEARCH_EDITBOX):
            SearchBar = self.GetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX).lower()
            self.BatchList = []
            self.BatchListItemsUIs = []
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()
            self.LayoutChanged(fcsID.ID_SNG_OPT_BATCH_LIST)
            self.LayoutChanged(fcsID.ID_SNG_OVERALL_GRP)

        if (id == self.ID_SNG_OPT_BATCH_CUSTOM_BTN):
            self.BatchList = []
            self.BatchListItemsUIs = []
            self.CheckedOnBatchList = []
            self.SetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX, AddingBatchDirectory())
            self.SetString(fcsID.ID_SNG_OPT_SEARCH_EDITBOX, "")
            self.SearchFilterBatchFileSystem()
            self.LoadFiles_To_BatchListViewGUI()
            self.LayoutChanged(fcsID.ID_SNG_OPT_BATCH_LIST)
            self.LayoutChanged(fcsID.ID_SNG_OVERALL_GRP)

        if (id == self.UI_BTN_DirLoad):
            AddDirPathToUi(ui_widget=self, ui_widgetId=fcsID.ID_SNG_OPT_PATH_EDITBOX)

        if (id == self.AddDir_UI_BTN):
            self.SetString(fcsID.ID_SNG_OPT_PATH_EDITBOX, ShowMenuAndAppendRecentFolder())

        # [ Opening the Export/Import Format Filter Settings ]    
        if (id == self.UI_FormatSettingsGearButton):
            OpenExportOrImportSettings(ui_instance=self)

        # [ Use Chosse Mode between Export or Import. ]     
        if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP:
            self.SetString(fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN, "Export") # print("Export Tab")
        if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
            self.SetString(fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN, "Import") # print("Import Tab")

        if (id == fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT):
            if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT)==False:
                self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, False)
            else:
                self.Enable(fcsID.ID_SNG_OPT_BATCH_IMPORT_GRP, True)

        # [ Exporting Objects or Importing Objects ]
        if (id == fcsID.ID_SNG_OPT_EXPORT_IMPORT_BTN):

            if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
                if self.GetBool(fcsID.ID_SNG_OPT_CHK_BATCH_IMPORT)==True:
                    if self.BatchListItemsUIs:
                        amountOfBathFiles = len(self.BatchList)
                        currentNum = 1
                        for itemfile in self.BatchListItemsUIs:
                            percent = float(currentNum)/amountOfBathFiles*100
                            currentNum+=1                                                    
                            if self.GetBool(itemfile['chkId'])==True:
                                snglib.UiBatchImporting(self, fn=itemfile['fName'], filterId=itemfile['fID'], extensionStr=itemfile['fExt'])
                            else:
                                pass
                            cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_GREEN_COL, "Importing Batch Files....."+str(percent)+"%")

                else:
                    snglib.UiImporting(self)

            if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP: 
                    if self.GetBool(fcsID.ID_SNG_OPT_CHK_EXPORT_SEPARATE)==True:
                        snglib.UiExportingSelectionSeparate(self)
                    else:          
                        snglib.UiExporting(self)

        # [ Adding a Custom Exporter  ]
        if (id == fcsID.ID_SNG_MENU_ADD_EXPORTFILTER):
            win = AddCustomFormatWindowUi.AddCustomFormatMainWindow(path=PLUGIN_PATH['PREFS'], formatTpye="Export")
            win.openWindowUI(cLib.DLG_TYPE4)
            filterlib = snglib.c4dSceneFilters()
            filterlib.CheckForExtensionsFilters(path=PLUGIN_PATH['PREFS'])            

        # [ Adding a Custom Importer  ]
        if (id == fcsID.ID_SNG_MENU_ADD_IMPORTFILTER):
            win = AddCustomFormatWindowUi.AddCustomFormatMainWindow(path=PLUGIN_PATH['PREFS'], formatTpye="Import")
            win.openWindowUI(cLib.DLG_TYPE4)
            filterlib = snglib.c4dSceneFilters()
            filterlib.CheckForExtensionsFilters(path=PLUGIN_PATH['PREFS'])            

        # [ Web Links ]
        if (id == fcsID.ID_SNG_MENU_OnManual):
            OpenWebLinks(webLink="OnlineManual")
        if (id == fcsID.ID_SNG_MENU_OffManual):
            c4d.storage.ShowInFinder(os.path.join(PLUGIN_PATH['LIB'], 'Resources', 'manual_docs'), False)
        if (id == 64559):
            OpenWebLinks(webLink="DIS")
        if (id == 64560):
            OpenWebLinks(webLink="GUM")            
        if (id == 64561):
            OpenWebLinks(webLink="YT")  
        if (id == 64562):
            gui.MessageDialog(snglib.AboutTool)
    
        # [ Open Preset, Saves, Folder ]
        if (id == fcsID.ID_SNG_MENU_OPENPRESETS_DIR):
            c4d.storage.ShowInFinder(os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoPresetsSaves'), False)
        if (id == fcsID.ID_SNG_MENU_OPENTAKES_DIR):
            c4d.storage.ShowInFinder(os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoTakesSaves'), False)
        if (id == fcsID.ID_SNG_MENU_FILTERS_EXP_SAVES):
            c4d.storage.ShowInFinder(os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoExtensions', 'ExportFilters'), False)
        if (id == fcsID.ID_SNG_MENU_FILTERS_IMP_SAVES):
            c4d.storage.ShowInFinder(os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoExtensions', 'ImportFilters'), False)

        # [ Enable Exported Popup Window Ui ]
        if (id == fcsID.ID_SNG_MENU_EXP_L_DEFOPT_ONE): # [ Enable ]
            ExportedPopupWindowUi.opendlg = False
        if (id == fcsID.ID_SNG_MENU_EXP_L_DEFOPT_TWO): # [ Disable ]
            ExportedPopupWindowUi.opendlg = True

        # [ Create a Export Preset ]
        if (id == fcsID.ID_SNG_MENU_SAVEPRESET):
            CreateExportOptionsPreset(ui_widget=self)
  
        # [ Anim Take Clips Main Menu ]
        if (id == fcsID.ID_SNG_TAKES_MENU_BTN):
            AnimTakesMainMenuPopupUI(ui_widget=self, ids=None)

        # [ Adding Anim Take Clips ]
        if (id == fcsID.ID_SNG_TAKES_ADD_CLIPS_BTN):
            AnimTakeClips.append(NodeClipDataStructure())
            AddAnimNodeClips(self)

        # [ Add User Anim Takes Export Path ]
        if (id == fcsID.ID_SNG_TAKES_EXPORT_DIR_BTN):
            AddDirPathToUi(ui_widget=self, ui_widgetId=fcsID.ID_SNG_TAKES_EXPORT_PATH)

        # [ Add User Recent Folder to Takes Export Path ]
        if (id == fcsID.ID_SNG_TAKES_RECENT_DIR_BTN):
            self.SetString(fcsID.ID_SNG_TAKES_EXPORT_PATH, ShowMenuAndAppendRecentFolder())            

        # [ Enable or Disable Solo Preview Mode ]
        if (id == fcsID.ID_SNG_TAKES_SET_SOLO_PREVIEW):
            PreviewSoloMode(self=self)

        # [ Enable or Disable Solo Preview Mode ]
        if (id == fcsID.ID_SNG_TAKES_EXPORT_BTN):
            snglib.UiTakesExporting(self=self, clipTakesData=AnimTakeClips)

        else:

            if AnimTakeClips:
                for ids in AnimTakeClips:
                    # Play Button Pressed
                    if id == ids["playBTN"]["ID"]:
                        AnimClipTakePlayButtonPreview(ui_widget=self, ids=ids)                 

                    # Time Button Pressed
                    if id == ids["timeBTN"]["ID"]:
                        GroupToggleInAndOut(self, ids["timeBTN"])

                    # Time Edited
                    if id == ids["timeBTN"]["STARTID"] or ids["timeBTN"]["ENDID"]:
                        snglib.EditAnimTimeClip(self, ids)

                    # Link Button Pressed
                    if id == ids["linkBTN"]["ID"]:
                        GroupToggleInAndOut(self, ids["linkBTN"])

                    # Linked Object 
                    if id == ids["linkBTN"]["LID"]:
                        snglib.GetLinkObject(self, ids=ids, defIcon=linkIcon, toggleIcon=linkedIcon)

                    # Edit Anim Take Clip Name
                    if id == ids["editBOX"]["ID"]:
                        snglib.EditAnimClipName(self, ids)

                    # Anim Take Clip Checked
                    if id == ids["takeCHK"]["ID"]:
                        snglib.EditAnimClipChecked(self, ids)                

                    # Anim Take Clip Format Type
                    if id == ids["format"]["ID"]:
                        ids["format"]["IV"] = self.GetLong(ids["format"]["ID"])                         

                    # Format Settings Button Pressed
                    if id == ids["formatS"]["ID"]:
                        for filter_extension in snglib.c4dSceneFilters.ExportFiltersC4:
                            if self.GetLong(ids["format"]["ID"]) == filter_extension["id"]:
                                c4d.PrefsLib_OpenDialog(filter_extension['export_id'])
                                print(filter_extension["str"])

                    # Menu Popup Pressed 
                    if id == ids["menuBTN"]["ID"]:
                        AnimNodeMenuPopupUI(ui_widget=self, ids=ids)

        return True

    def CoreMessage(self, id, msg):
        if id == c4d.EVMSG_CHANGE:
            pass
        return True

    def DestroyWindow(self):
        """
        DestroyWindow Override this method - this function is called when the dialog is
        about to be closed temporarily, for example for layout switching.
        """
        global AnimTakeClips
        AnimTakeClips = []
        RecentList = []
        self.BatchList = []
        self.BatchListItemsUIs = []
        self.CheckedOnBatchList = []
        pass

"""
UI MainWindow Heplers Methods
_____________________________________________________________________
"""
class UserNodeClipTakeData(object):
    """ User Node Anim Clip Take Data Structure """
    def __init__(self, takeData):
        self.SelectNGoUserTakesData = takeData

# [ Loading Tool Icons ]
def LoadAllUiIcons(PLUGIN_PATH):
    """ Load UI Icons Images on Startup """

    imagespath = os.path.join(PLUGIN_PATH['LIB'], 'Resources', 'images')

    global playIcon
    global stopIcon
    global timerIcon
    global linkIcon
    global linkedIcon
    global formatsetIcon
    global menuIcon
    global addIcon
    global searchIcon
    global folderIcon
    global recentfolderIcon
    global soloplayIcon
    global soloplayEnableIcon

    playIcon = os.path.join(imagespath, "icon_play_btn.png")
    stopIcon = os.path.join(imagespath, "icon_stop_btn.png")
    timerIcon = os.path.join(imagespath, "icon_timer_btn.png")
    linkIcon = os.path.join(imagespath, "icon_link_btn.png")
    linkedIcon = os.path.join(imagespath, "icon_link_enable_btn.png")
    formatsetIcon = os.path.join(imagespath, "icon_format_set_btn.png")
    menuIcon = os.path.join(imagespath, "icon_menu_btn.png")
    addIcon = os.path.join(imagespath, "icon_add_btn.png")
    searchIcon = os.path.join(imagespath, "icon_search.png")
    folderIcon = os.path.join(imagespath, "icon_folder.png")
    recentfolderIcon = os.path.join(imagespath, "icon_Refolder.png") 
    soloplayIcon = os.path.join(imagespath, "icon_soloplay_btn.png")
    soloplayEnableIcon = os.path.join(imagespath, "icon_soloplay_enable_btn.png")

    return True

# [ Get files from directory path, by their file extension ]
def GetFilesFromDirectory(ui_instance, FileData):
    """ Get files from directory path, by their file extension."""
    self = ui_instance
    path = self.GetString(fcsID.ID_SNG_OPT_BATCH_EDITBOX) #"C:/Users/AP_Ashton_TheCreator/Desktop/0_TestFilesZone/NOOBS MODELS" # self.GetString(self.ID_SNG_OPT_BATCH_EDITBOX)
    if os.path.exists(path):
        # Looking for all files in folder.    
        os.chdir(path)
        # Get all files with extension from the folder.
        for each_file in glob.glob(FileData["File"]):
            self.BatchList.append({'fID':FileData["ID"], 'fStrExt':FileData["Str"], 'fExt':FileData["Ext"], 'fName':each_file.split(FileData["Ext"])[0] })
    return True

def FileNodeGUI_ItemLayout(ui_instance, itemData):
    """ 
    Item slot UI layout.
    ui_data = { 'GRP_ID':data, 'GRP2_ID':data, 'CHK_ID':data, 'FILE_STR':data, 'EXT_STR':data }
    self.FileNodeGUI_ItemLayout(itemData=ui_data)
    """
    self = ui_instance

    self.GroupBegin(itemData["GRP_ID"], c4d.BFH_SCALEFIT, 3, 0, "") # c4d.BFV_CMD_EQUALCOLUMNS,
    #self.GroupSpace(0, 0)
    Vect = c4d.Vector(0.125, 0.125, 0.125)
    self.SetDefaultColor(itemData["GRP_ID"], c4d.COLOR_BG, Vect)

    self.AddCheckbox(itemData["CHK_ID"], c4d.BFH_SCALEFIT, 30, 0, "") # File Check

    self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 15, itemData["FILE_STR"]+"  ", c4d.BORDER_WITH_TITLE_BOLD) # File Name

    self.GroupBegin(itemData["GRP2_ID"], c4d.BFH_LEFT, 1, 0, "")
    self.SetDefaultColor(itemData["GRP2_ID"], c4d.COLOR_BG, g_lib.BG_GREEN_COL)
    self.AddStaticText(0, c4d.BFH_LEFT, 80, 15, "  "+itemData["EXT_STR"]+"  ", c4d.BORDER_WITH_TITLE_BOLD) # File Type
    self.GroupEnd()

    self.GroupEnd()
    return True

# [ Inject GUIs]
def AddandInjectGUIsWidgets(ui_instance):

    self = ui_instance
    ImgButton = cLib.CustomImageButtonGUI(self)
    TileBar = cLib.AddCustomQuickTab_GUI(self)
    SCENESAVER = c4d.PLUGINTYPE_SCENESAVER
    SCENELOADER = c4d.PLUGINTYPE_SCENELOADER

    def AddFilter(ComboID, ChildID, FilterString, FilterID, FilterType):
        """ This function is check for export/import filter IDs and then add it to the combox formats lic4d.storage. """
        plug = plugins.FindPlugin(FilterID, FilterType)     
        if plug is None:
            pass #print(FilterString + " This Plugin Is Not Install.")   
        else:
            self.AddChild(ComboID, ChildID, FilterString)
        return True  

    def Inject_GUI_Title(injected_group_id, str_title):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(injected_group_id) # Refresh Group UI
        TileBar.Add_BarTitle_GUI(bar_id=0, bar_name=str_title, width=290, height=0, ui_color=None)
        self.LayoutChanged(injected_group_id) # Update Group UI
        return True

    def Inject_GUI_Icon(injected_group_id, data):
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(injected_group_id) # Refresh Group UI
        ImgButton.GUI(data['id'], icon=data['icon'], tip=data['tip']) 
        self.LayoutChanged(injected_group_id) # Update Group UI
        return True

    def Inject_GUI_Status():
        """ Inject your UI layout or GUI element to a empty group. """
        self.LayoutFlushGroup(fcsID.ID_SNG_STATUS_BAR_GRP) # Refresh Group UI
        g_lib.Add_StatusBar_GUI(ui_instance=self, ui_id=self.STATUS_UI_BAR_ID, state_col=g_lib.BG_DARK, str_id=self.STATUS_UI_TXT_ID, message="OK")
        self.LayoutChanged(fcsID.ID_SNG_STATUS_BAR_GRP) # Update Group UI
        return True

    def Inject_GUI_FCS_Web_Banner():
        self.LayoutFlushGroup(fcsID.ID_SNG_FCSWEBBANNER) # Refresh Group UI
        g_lib.Bottom_FCS_Web_GUI_Banner(ui_instance=self, button_id=self.FCS_WEBSITE_GUI_BANNER_ID)
        self.LayoutChanged(fcsID.ID_SNG_FCSWEBBANNER)    # Update Group UI
        return True

    def AnimClipsTakesUi():

        self.LayoutFlushGroup(2041) # Refresh Group UI

        self.GroupBegin(108, c4d.BFH_SCALEFIT, 3, 0, "")

        self.GroupBegin(0, c4d.BFH_LEFT, 2, 0, "")

        ImgButton.GUI(fcsID.ID_SNG_TAKES_MENU_BTN, clickable=True, icon=menuIcon, tip="<b>Menu</b>") 

        ImgButton.GUI(fcsID.ID_SNG_TAKES_SET_SOLO_PREVIEW, clickable=True, icon=soloplayIcon, tip="<b>"+self.IDS.ID(fcsID.ID_SNG_TAKES_SET_SOLO_PREVIEW)+"</b>")                   

        self.GroupEnd()

        self.AddStaticText(0, c4d.BFH_SCALEFIT, 0, 15, "", c4d.BORDER_WITH_TITLE_BOLD) # Static UI Text

        self.GroupBegin(0, c4d.BFH_RIGHT, 1, 0, "")

        ImgButton.GUI(fcsID.ID_SNG_TAKES_ADD_CLIPS_BTN, clickable=True, icon=addIcon, tip="<b>Add a Animation Take Clip</b>") 

        self.GroupEnd()

        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.ScrollGroupBegin(id=96, flags=c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, scrollflags=c4d.SCROLLGROUP_VERT|c4d.SCROLLGROUP_AUTOVERT|c4d.SCROLLGROUP_BORDERIN, initw=200, inith=200)

        self.GroupBegin(1098, c4d.BFH_SCALEFIT|c4d.BFV_TOP, 1, 0, "")
        self.GroupEnd()

        self.GroupEnd()

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT)

        self.GroupBegin(0, c4d.BFH_SCALEFIT, 6, 0, "")
        self.AddStaticText(0, c4d.BFH_MASK, 0, 15, self.IDS.ID(fcsID.ID_SNG_TAKES_EXPORT_PATH), c4d.BORDER_WITH_TITLE_BOLD)
        self.AddEditText(fcsID.ID_SNG_TAKES_EXPORT_PATH, c4d.BFH_SCALEFIT, 100, 0)
        ImgButton.GUI(fcsID.ID_SNG_TAKES_RECENT_DIR_BTN, clickable=True, icon=recentfolderIcon, tip=self.IDS.ID(fcsID.ID_SNG_TAKES_RECENT_DIR_BTN))
        ImgButton.GUI(fcsID.ID_SNG_TAKES_EXPORT_DIR_BTN, clickable=True, icon=folderIcon, tip="<b>Add export path directory.</b>")
        self.AddSeparatorV(0, c4d.BFV_SCALEFIT)
        self.AddButton(id=fcsID.ID_SNG_TAKES_EXPORT_BTN, flags=c4d.BFH_MASK, initw=100, inith=15, name=self.IDS.ID(fcsID.ID_SNG_TAKES_EXPORT_BTN))
        self.GroupEnd()

        self.LayoutChanged(2041) # Update Group UI
        return True

    # Set GUI Elements to UI.
    Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_MODE_TITLE_GRP, str_title=" Export Settings")
    Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_FORMAT_SETTINGS_TITLE_GRP, str_title=" Format Settings")
    Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_OPTIONS_TITLE_GRP, str_title=" Option Settings")
    Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_BATCH_TITLE_GRP, str_title=" Batch Importing")
    Inject_GUI_Title(injected_group_id=fcsID.ID_SNG_OPT_PATH_TITLE, str_title="Export Path")

    for eachFilter in snglib.c4dSceneFilters.ExportFiltersC1:##ExportFiltersC1
        AddFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], FilterType=SCENESAVER)
    
    self.AddChild(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, -1, "---------------------------------") # Separator
    
    for eachFilter in snglib.c4dSceneFilters.ExportFiltersC2:
        AddFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], FilterType=SCENESAVER)        
    
    self.AddChild(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, -1, "---------------------------------") # Separator
    
    for eachFilter in snglib.c4dSceneFilters.ExportFiltersC3:
        AddFilter(ComboID=fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU, ChildID=eachFilter['id'], FilterString=eachFilter['str'], FilterID=eachFilter['export_id'], FilterType=SCENESAVER)


    self.AddChild(fcsID.ID_SNG_OPT_BATCHFITER_MENU, self.ID_SNG_OPT_BATCHFITER_MENU_ALL, " Show All ")
    self.AddChild(fcsID.ID_SNG_OPT_BATCHFITER_MENU, -1, "---------------------------------")
    for filter_extension in snglib.c4dSceneFilters.ImportFilters:
        AddFilter(ComboID=fcsID.ID_SNG_OPT_BATCHFITER_MENU, 
                  ChildID=filter_extension['id'], 
                  FilterString=filter_extension['extstr'], 
                  FilterID=filter_extension['import_id'],
                  FilterType=SCENELOADER
                )
    self.AddChild(fcsID.ID_SNG_OPT_BATCHFITER_MENU, -1, "---------------------------------")
    for filter_extension in snglib.c4dSceneFilters.ImportFiltersC1:
        AddFilter(ComboID=fcsID.ID_SNG_OPT_BATCHFITER_MENU, 
                  ChildID=filter_extension['id'], 
                  FilterString=filter_extension['extstr'], 
                  FilterID=filter_extension['import_id'],
                  FilterType=SCENELOADER
                )    

    settingsIcon_set = {'id':self.UI_FormatSettingsGearButton, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nTo open your export formats settings.", 'icon':formatsetIcon, 'clickable':True}
    Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_FORMAT_GEAR_SETTINGS, data=settingsIcon_set)
    
    dir1Icon_set = {'id':self.ID_SNG_OPT_BATCH_CUSTOM_BTN, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nTo open your export formats settings.", 'icon':folderIcon, 'clickable':True}
    Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_BATCH_DIR_BTN_GRP, data=dir1Icon_set)
    
    dir2Icon_set = {'id':self.UI_BTN_DirLoad, 'btn_look':c4d.BORDER_NONE, 'tip':"<b>Settings</b>\nAdding export path directory.", 'icon':folderIcon, 'clickable':True}
    Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_PATH_DIR_BTN_GRP, data=dir2Icon_set)
    
    searchIcon_set = {'id':0, 'btn_look':c4d.BORDER_NONE, 'icon':searchIcon, 'tip':""}
    Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_BATCH_SEARCHICON_GRP, data=searchIcon_set)

    rf1Icon_set = {'id':self.AddDir_UI_BTN, 'btn_look':c4d.BORDER_NONE, 'tip': "<b>Recent Folder</b>\nTo add your recent export folder path for exporting,\nthat you added from the browser button before.", 'icon':recentfolderIcon, 'clickable':True}
    Inject_GUI_Icon(injected_group_id=fcsID.ID_SNG_OPT_PATH_RF_BTN, data=rf1Icon_set)        

    AnimClipsTakesUi()

    Inject_GUI_Status()

    Inject_GUI_FCS_Web_Banner()

    return True

# [ AnimationTakeClips Data Structure ]
def NodeClipDataStructure():
    """Animation Take Clips Data Node Structure"""
    doc = c4d.documents.GetActiveDocument()
    #doc[c4d.DOCUMENT_FPS]
    #doc[c4d.DOCUMENT_MINTIME]
    #doc[c4d.DOCUMENT_MAXTIME]
    #doc[c4d.DOCUMENT_LOOPMINTIME]
    #doc[c4d.DOCUMENT_LOOPMAXTIME]

    NodeTake = {"AnimClip"   :{'ID':GuiIdGenerator(200000) },

                "takeCHK"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BOOL",
                                'IV':False },

                "playBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':False,
                                'I':playIcon,
                                'Tip':"<b>Play Animation</b>" },

                "linkBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':True,
                                'I':linkIcon,
                                'BTNGRP':GuiIdGenerator(200000),
                                'GRPID':GuiIdGenerator(200000),
                                'GRPSTATE':True,
                                'LID':GuiIdGenerator(200000),
                                'LOBJ':"",
                                'LOBJGUID':"",
                                'Tip':"<b>Link Object</b>" },

                "timeBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':True,
                                'I':timerIcon,
                                'BTNGRP':GuiIdGenerator(200000),
                                'GRPID':GuiIdGenerator(200000),
                                'GRPSTATE':True,
                                'STARTID':GuiIdGenerator(200000),
                                'ENDID':GuiIdGenerator(200000),
                                'startT':0,
                                'endT':0,
                                'Tip':"<b>Add Clip Time Frame</b>\nAdding the Start and End frames.\n<b>Start : 0\nEnd : 90</b>" },

                "menuBTN"    :{'ID':GuiIdGenerator(200000),
                                    'UT':"STRING",
                                    'IV':False,
                                    'I':menuIcon,
                                    'Tip':"<b>Clip Settings</b>" },

                "formatS"    :{'ID':GuiIdGenerator(200000),
                                    'UT':"BUTTON",
                                    'IV':False,
                                    'I':formatsetIcon,
                                    'Tip':"<b>Format Settings</b>" },

                "editBOX"    :{'ID':GuiIdGenerator(200000),
                                    'UT':"STRING", 
                                    'IV':""},

                "format"     :{'ID':GuiIdGenerator(200000),
                                    'UT':"LONG",
                                    'IV':0},
                }
    print(NodeTake)
    return NodeTake

# [ Animation Clip UI Layout ]
def AnimTakeClipSlot_GUI_Layout(ui_widget, nodeclip):
    ui = ui_widget

    def AddFilter(ComboID, ChildID, FilterString, FilterID, FilterType):
        """ This function is check for export/import filter IDs and then add it to the combox formats lic4d.storage. """
        plug = plugins.FindPlugin(FilterID, FilterType)     
        if plug is None:
            pass #print(FilterString + " This Plugin Is Not Install.")   
        else:
            ui.AddChild(ComboID, ChildID, FilterString)
        return True  

    # Adding Custom UI Button
    def AddUiBtn(btnData):
        ImgButton = cLib.CustomImageButtonGUI(ui)
        return ImgButton.GUI(btnData['ID'], icon=btnData['I'], tip=btnData['Tip'])     

    grpID = nodeclip['AnimClip']['ID']

    ui.GroupBegin(grpID, c4d.BFH_SCALEFIT, 1, 0, "")

    ui.GroupBorderSpace(2, 2, 2, 2)

    ui.GroupBegin(0, c4d.BFH_SCALEFIT, 14, 0, "")

    ui.AddCheckbox(nodeclip['takeCHK']['ID'], c4d.BFH_MASK, 0, 0, "")

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Play Button
    AddUiBtn(nodeclip['playBTN'])

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Take Clip String
    ui.AddEditText(nodeclip['editBOX']['ID'], c4d.BFH_SCALEFIT, 100, 0)

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Link Button
    ui.GroupBegin(nodeclip['linkBTN']['BTNGRP'], c4d.BFH_MASK, 1, 0, "")
    AddUiBtn(nodeclip['linkBTN'])
    ui.GroupEnd()

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Time Button
    ui.GroupBegin(nodeclip['timeBTN']['BTNGRP'], c4d.BFH_MASK, 1, 0, "")
    AddUiBtn(nodeclip['timeBTN'])
    ui.GroupEnd()

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Format List
    ui.AddComboBox(nodeclip['format']['ID'], c4d.BFH_MASK, 35, 10, False)
    for filter_extension in snglib.c4dSceneFilters.ExportFiltersC4:
        AddFilter(ComboID=nodeclip['format']['ID'], 
                  ChildID=filter_extension['id'], 
                  FilterString=str(filter_extension['extstr'])+" - "+str(filter_extension['str']), 
                  FilterID=filter_extension['export_id'],
                  FilterType=c4d.PLUGINTYPE_SCENESAVER
                )        

    # Gear Settings Button
    AddUiBtn(nodeclip['formatS'])

    ui.AddSeparatorV(0, c4d.BFV_SCALEFIT)

    # Menu Button
    AddUiBtn(nodeclip['menuBTN'])

    ui.GroupEnd()


    # Link Group
    ui.GroupBegin(nodeclip['linkBTN']['GRPID'], c4d.BFH_SCALEFIT, 1, 0, "")
    ui.rootLinkBaseContainer = c4d.BaseContainer()
    ui.linkBoxForName = ui.AddCustomGui(nodeclip['linkBTN']['LID'], c4d.CUSTOMGUI_LINKBOX, "", c4d.BFH_SCALEFIT, 320, 10, ui.rootLinkBaseContainer)
    ui.GroupEnd()

    # Time Group
    ui.GroupBegin(nodeclip['timeBTN']['GRPID'], c4d.BFH_SCALEFIT, 4, 0, "")
    ui.AddStaticText(0, c4d.BFH_CENTER, 0, 10, "Animation Start")
    ui.AddEditNumberArrows(nodeclip['timeBTN']['STARTID'], c4d.BFH_SCALEFIT, 100, 0)
    ui.AddStaticText(0, c4d.BFH_CENTER, 0, 10, "Animation End")
    ui.AddEditNumberArrows(nodeclip['timeBTN']['ENDID'], c4d.BFH_SCALEFIT, 100, 0)
    ui.GroupEnd()

    ui.GroupEnd()

    # [ Init UI Data To Set ]
    ui.SetDefaultColor(grpID, c4d.COLOR_BG, cLib.BG_DARK)
    ui.SetBool(nodeclip["takeCHK"]["ID"], nodeclip["takeCHK"]["IV"])
    ui.SetString(nodeclip["editBOX"]["ID"], nodeclip["editBOX"]["IV"])
    ui.SetInt32(nodeclip["timeBTN"]["STARTID"], int(nodeclip["timeBTN"]["startT"]))
    ui.SetInt32(nodeclip["timeBTN"]["ENDID"], int(nodeclip["timeBTN"]["endT"])) 
    ui.SetLong(nodeclip["format"]["ID"], nodeclip["format"]["IV"])          
    ui.HideElement(nodeclip['timeBTN']['GRPID'], nodeclip['timeBTN']['GRPSTATE'])
    ui.HideElement(nodeclip['linkBTN']['GRPID'], nodeclip['linkBTN']['GRPSTATE'])

    doc = c4d.documents.GetActiveDocument()
    link = doc.SearchObject(nodeclip["linkBTN"]["LOBJ"])
    if link:
        if link.GetGUID() == nodeclip["linkBTN"]["LOBJGUID"]:
            ui.FindCustomGui(nodeclip["linkBTN"]["LID"], c4d.CUSTOMGUI_LINKBOX).SetLink(link)
        SwitchIcon(self=ui, icon=linkedIcon, btn_id=nodeclip['linkBTN']['ID'], state=True)


    return True

# [ Group UI Toggle Method Function ]
def GroupToggleInAndOut(ui_widget, ids):
    """ Group Toggle In And Out Button Toggle System """
    ui = ui_widget

    if ids["IV"] == True:
        # Show the Time Input UI.
        ui.HideElement(ids["GRPID"], False)
        ids["IV"]=False
        ids["GRPSTATE"]=False

    else:
        # Hide the Time Input UI.
        ui.HideElement(ids["GRPID"], True)
        ids["IV"]=True
        ids["GRPSTATE"]=True
        AddAnimNodeClips(ui)

    ui.LayoutChanged(1098)  # Update UI

    return True

# [ Add Animation Clips Nodes to UI Main Layout ]
def AddAnimNodeClips(ui_widget):
    """ Adding an Updating Animation Clips Nodes to UI Main Layout """
    ui_widget.LayoutFlushGroup(1098)

    for i in AnimTakeClips:
        AnimTakeClipSlot_GUI_Layout(ui_widget, i)

    ui_widget.LayoutChanged(1098)

    return True

# [ Duplicate Animation Clips ]
def DuplicateAnimClipTake(ids):
    """ Duplicate Animation Clips Nodes in the UI Main Layout """

    NodeTake = {"AnimClip"   :{'ID':GuiIdGenerator(200000) },

                "takeCHK"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BOOL",
                                'IV':ids["takeCHK"]['IV'] 
                                },

                "playBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':ids["playBTN"]['IV'],
                                'I':playIcon,
                                'Tip':ids["playBTN"]['Tip'] 
                                },

                "linkBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':ids["linkBTN"]['IV'],
                                'I':linkIcon,
                                'BTNGRP':GuiIdGenerator(200000),
                                'GRPID':GuiIdGenerator(200000),
                                'GRPSTATE':ids["linkBTN"]['GRPSTATE'],
                                'LID':GuiIdGenerator(200000),
                                'LOBJ':ids["linkBTN"]['LOBJ'],
                                'LOBJGUID':ids["linkBTN"]['LOBJGUID'],
                                'Tip':ids["linkBTN"]['Tip']
                                },

                "timeBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':ids["timeBTN"]['IV'],
                                'I':timerIcon,
                                'BTNGRP':GuiIdGenerator(200000),
                                'GRPID':GuiIdGenerator(200000),
                                'GRPSTATE':ids["timeBTN"]['GRPSTATE'],
                                'STARTID':GuiIdGenerator(200000),
                                'ENDID':GuiIdGenerator(200000),
                                'startT':ids["timeBTN"]['startT'],
                                'endT':ids["timeBTN"]['endT'],
                                'Tip':ids["timeBTN"]['Tip'] 
                                },

                "menuBTN"    :{'ID':GuiIdGenerator(200000),
                                'UT':"STRING",
                                'IV':ids["menuBTN"]['IV'],
                                'I':menuIcon,
                                'Tip':ids["menuBTN"]['Tip'] 
                                },

                "formatS"    :{ 'ID':GuiIdGenerator(200000),
                                'UT':"BUTTON",
                                'IV':ids["formatS"]['IV'],
                                'I':formatsetIcon,
                                'Tip':ids["formatS"]['Tip'] 
                                },

                "editBOX"    :{ 'ID':GuiIdGenerator(200000),
                                'UT':"STRING",
                                'IV':ids["editBOX"]['IV']
                                },

                "format"     :{ 'ID':GuiIdGenerator(200000),
                                'UT':"LONG",
                                'IV':ids["format"]['IV']
                                },
                }
    #print(NodeTake)
    return NodeTake

# [ Anim Node Clip Popup Menu ]
def AnimNodeMenuPopupUI(ui_widget, ids):
    """ Anim Node Clip Popup Menu  """
    # Declare menu items IDs
    IDM_ITEM1 = c4d.FIRST_POPUP_ID
    IDM_ITEM2 = c4d.FIRST_POPUP_ID+1

    # Main menu
    menu = c4d.BaseContainer()
    menu.InsData(IDM_ITEM1, '&i100004820&Duplicate')    # [ C4D Copy Icon ID : 100004820 ]
    menu.InsData(0, '') # Append separator    
    menu.InsData(IDM_ITEM2, '&i100004787&Delete')       # [ C4D Copy Icon ID : 100004787 ]
    menu.InsData(0, '') # Append separator    

    result = c4d.gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    if result == IDM_ITEM1:
        AnimTakeClips.append(DuplicateAnimClipTake(ids))
        AddAnimNodeClips(ui_widget)

    if result == IDM_ITEM2:
        AnimTakeClips.remove(ids)
        AddAnimNodeClips(ui_widget)

    return True

# [ Anim Clip Takes Main Popup Menu ]
def AnimTakesMainMenuPopupUI(ui_widget, ids):
    ui = ui_widget # Its self.
    """ Anim Clip Takes Main Popup Menu  """
    # Declare menu items IDs
    IDM_ITEM1 = c4d.FIRST_POPUP_ID
    IDM_ITEM2 = c4d.FIRST_POPUP_ID+1
    IDM_ITEM3 = c4d.FIRST_POPUP_ID+2
    IDM_ITEM4 = c4d.FIRST_POPUP_ID+3
    IDM_ITEM5 = c4d.FIRST_POPUP_ID+5

    USER_FILE_SAVES = []

    # Main Takes Menu
    menu = c4d.BaseContainer()

    submenu_load = c4d.BaseContainer()     
    submenu_load.InsData(1, '&i100004820&'+ui.IDS.ID(fcsID.ID_SNG_TAKES_CLIPS_LOAD))
    num = 0
    for i in DirHelper.GetAllFilesInDirectoryAsList(DIR=os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoTakesSaves'), Extension="*.json"):
        num+=510
        item = { 'id':c4d.FIRST_POPUP_ID+num, 'jsonFile':i}
        USER_FILE_SAVES.append(item)
        submenu_load.InsData(item['id'], '&i100004820&' + i.split(".")[0])  
    menu.SetContainer(IDM_ITEM1, submenu_load)                                                                             # Set submenu as subcontainer

    menu.InsData(IDM_ITEM2, '&i12098&'+ui.IDS.ID(fcsID.ID_SNG_TAKES_CLIPS_SAVE))
    menu.InsData(0, '') # Append separator 

    result = c4d.gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    if result == IDM_ITEM2:
        SaveUserTakes()

    else:
        for i in USER_FILE_SAVES:
            if result == i['id']:
                LoadUserTakes(ui=ui, file=i['jsonFile'])        

    return True

# [ Web Link Helper ]
def OpenWebLinks(webLink):
    link = snglib.FCSWEBLINKS.get(webLink)
    if link:
        webbrowser.open(link, new=2, autoraise=True)
    return True

# [ Save Export Options as a Preset ]
def CreateExportOptionsPreset(ui_widget):
    self = ui_widget
    formatType = None
    formatExt = None    

    strData = None
    if self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX) == "":
        strData = "None"
    else:
        strData = self.GetString(fcsID.ID_SNG_OPT_PATH_EDITBOX)

    for SelFilter in snglib.filterlib.ExportFilters:
            if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:
                formatType = SelFilter['str']
                formatExt = SelFilter['extstr']

    ToolJsonSaveFile = snglib.ExportPresetSaveFileTemplate( CenterExportedObjCheck=jEdit.JsonBoolConverter(self.GetBool(fcsID.ID_SNG_OPT_CHK_CENTER_OBJ)),
                                                            SeparateCheck=jEdit.JsonBoolConverter(self.GetBool(fcsID.ID_SNG_OPT_CHK_EXPORT_SEPARATE)),
                                                            OpenExportPathCheck=jEdit.JsonBoolConverter(self.GetBool(fcsID.ID_SNG_OPT_CHK_OPENFOLDER)),
                                                            ShowExportedFileCheck=jEdit.JsonBoolConverter(self.GetBool(fcsID.ID_SNG_OPT_CHK_EXPORT_POPUP)),
                                                            ModelExportPath=strData,
                                                            ExportFormat = formatType,
                                                            ExportFormatExt = formatExt
                                                            )
    popupWin = PresetNamingWindowUi.FileNameMainWindow(PLUGIN_PATH['PREFS'], data=ToolJsonSaveFile)
    popupWin.openWindowUI(cLib.DLG_TYPE4)          
    return True

# [ Adding Folder Path to UI and To the Recent Folder List ]
def AddDirPathToUi(ui_widget, ui_widgetId):
    """ Adding Folder Path to UI and To the Recent Folder List. """
    c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Export Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
    ui_widget.SetString(ui_widgetId, c_dir)
    # Then Add too folder to Recent Folder List.
    if c_dir:
        userDir = {'dirName':os.path.split(c_dir)[1], 'dirPath':c_dir}
        if os.path.exists(sngRecentDirList):
            jsonFileData = jEdit.LoadFile(sngRecentDirList)
            if jsonFileData:
                if userDir in jsonFileData:
                    pass
                else:
                    RecentList.append(userDir)
                jEdit.SaveFile(sngRecentDirList, RecentList)
        else:
            RecentList.append(userDir)
            jEdit.SaveFile(sngRecentDirList, RecentList)      
    return True

# [ Adding Recent Folder PopMenu ]
def ShowMenuAndAppendRecentFolder():
    """
    Add Recent Folders 
    """
    Data = None

    # Get and read dirs paths from the .txt file and store to a list.
    sub_points = []
    sub_points_ids = []

    if os.path.exists(sngRecentDirList):
        jsonFileData = jEdit.LoadFile(sngRecentDirList)
        for user_Dir in jsonFileData:
            sub_points.append(user_Dir)

    # Create Menu
    menu = c4d.BaseContainer()        

    num = 0
    for item in sub_points:
        num+=510
        i_id = c4d.FIRST_POPUP_ID+num
        item_data = {'id':i_id, 'dirPath':item['dirPath']}
        sub_points_ids.append( item_data )
        menu.SetString(i_id, '&i100004820&'+item['dirName']+" - "+item_data['dirPath'])

    result = gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    for item2 in sub_points_ids:
        if result == item2['id']:
            Data = str(item2['dirPath'])

    return Data

# [ Recent Folder PopMenu ]
def AddingBatchDirectory():
    c_dir = c4d.storage.LoadDialog(type=c4d.FILESELECTTYPE_ANYTHING, title="Get Export Directory", flags=c4d.FILESELECT_DIRECTORY, force_suffix="")
    return c_dir  

# [ Switching GUI Button Icon ]
def SwitchIcon(self, icon, btn_id, state):
    """ Switching GUI Button Icon (c4d.CUSTOMGUI_BITMAPBUTTON) """
    Button = self.FindCustomGui(btn_id, c4d.CUSTOMGUI_BITMAPBUTTON) 
    Button.SetToggleState(state)    
    Button.SetImage(icon, True)
    return True

# [ Solo Object PreviewPlay Mode ]
def PreviewSoloMode(self):
    """ Solo Object PreviewPlay Mode """
    global soloPreviewState
    if soloPreviewState == True:
        soloPreviewState = False
        SwitchIcon(self, icon=soloplayIcon, btn_id=fcsID.ID_SNG_TAKES_SET_SOLO_PREVIEW, state=soloPreviewState)
    else:
        soloPreviewState = True
        SwitchIcon(self, icon=soloplayEnableIcon, btn_id=fcsID.ID_SNG_TAKES_SET_SOLO_PREVIEW, state=soloPreviewState)
    return True

# [ Solo Object on Preview Play ]
def SoloObjectPreview(self, doc, ids):
    global soloPreviewState
    if soloPreviewState == True:
        Link = self.FindCustomGui(ids["linkBTN"]["LID"], c4d.CUSTOMGUI_LINKBOX).GetLink()
        doc.SetActiveObject( doc.SearchObject(Link.GetName()) )
        c4d.CallCommand(431000060, 431000060) # Viewport Solo Hierarchy       
    else:
        c4d.CallCommand(431000058, 431000058) # Viewport Solo Off

# [ Play Anim Clip Preview ]
def AnimClipTakePlayButtonPreview(ui_widget, ids):
    """ Play Button Pressed System"""
    self = ui_widget
    doc = c4d.documents.GetActiveDocument() # Get Doc

    Link = self.FindCustomGui(ids["linkBTN"]["LID"], c4d.CUSTOMGUI_LINKBOX).GetLink()
    if Link:
        if ids["playBTN"]["IV"] == True:
            c4d.CallCommand(12002) # Stop
            c4d.CallCommand(12501) # Go to Start
            c4d.CallCommand(431000058, 431000058) # Viewport Solo Off
            doc[c4d.DOCUMENT_LOOPMINTIME] = c4d.BaseTime(doc.GetMinTime().GetFrame(doc[c4d.DOCUMENT_FPS]), doc[c4d.DOCUMENT_FPS])
            doc[c4d.DOCUMENT_LOOPMAXTIME] = c4d.BaseTime(doc.GetMaxTime().GetFrame(doc[c4d.DOCUMENT_FPS]), doc[c4d.DOCUMENT_FPS])
            ids["playBTN"]["IV"] = False
            SwitchIcon(self, icon=playIcon, btn_id=ids["playBTN"]["ID"], state=ids["playBTN"]["IV"])
            SoloObjectPreview(self, doc, ids)
            c4d.CallCommand(431000058, 431000058) # Viewport Solo Off
        else:
            c4d.CallCommand(12002) # Stop
            c4d.CallCommand(12501) # Go to Start
            c4d.CallCommand(431000058, 431000058) # Viewport Solo Off
            ids["playBTN"]["IV"] = True
            doc[c4d.DOCUMENT_LOOPMINTIME] = c4d.BaseTime(int(ids["timeBTN"]["startT"]), doc[c4d.DOCUMENT_FPS])
            doc[c4d.DOCUMENT_LOOPMAXTIME] = c4d.BaseTime(int(ids["timeBTN"]["endT"]), doc[c4d.DOCUMENT_FPS])
            SwitchIcon(self, icon=stopIcon, btn_id=ids["playBTN"]["ID"], state=ids["playBTN"]["IV"])
            SoloObjectPreview(self, doc, ids)
            c4d.CallCommand(12412) # Play Forwards
            for ids2 in AnimTakeClips:
                if ids["playBTN"]["ID"] == ids2["playBTN"]["ID"]:
                    pass
                else:
                    ids2["playBTN"]["IV"] = False
                    SwitchIcon(self, icon=playIcon, btn_id=ids2["playBTN"]["ID"], state=ids2["playBTN"]["IV"])
    else:
        gui.MessageDialog("No Link object added to the Anim Clip Take to preview animation.")
        pass

    c4d.EventAdd() 
    return True

# [ Open the Export / Import Scene Format Filter Settings Dialog Window ]
def OpenExportOrImportSettings(ui_instance):
    """ Open the Export / Import Scene Format Filter Settings Dialog Window. """
    self = ui_instance
    if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU:
        cLib.Status_GUI_Updater(self, self.STATUS_UI_BAR_ID, self.STATUS_UI_TXT_ID, cLib.BG_RED_COL, "Please select a format.")
    else:
        for SelFilter in snglib.filterlib.ExportFilters:

            if self.GetLong(fcsID.ID_SNG_OPT_FORMAT_DROPLIST_MENU)==SelFilter['id']:

                if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_IMPORT_OPTIONS_TAB_GRP:
                    importPLUGIN_ID = SelFilter['import_id']
                    c4d.PrefsLib_OpenDialog(importPLUGIN_ID)

                if self.GetInt32(fcsID.ID_SNG_OPTIONS_TABS) == fcsID.ID_SNG_EXPORT_OPTIONS_TAB_GRP:
                    ExportPLUGIN_ID = SelFilter['export_id']
                    c4d.PrefsLib_OpenDialog(ExportPLUGIN_ID)
    return True    

# [ Saving User Take Project Data  ]
def SaveUserTakes():
    """ Saving Users Takes  """
    user_data = UserNodeClipTakeData(takeData=AnimTakeClips)
    popupWin = SavingUserAnimTakesWindowUi.FileNameMainWindow(PLUGIN_PATH['PREFS'], data=user_data)
    popupWin.openWindowUI(cLib.DLG_TYPE4)          
    return True

# [ Loading User Take Project Data  ]
def LoadUserTakes(ui, file):
    """ Loading Users Takes  """
    jsonFileData = jEdit.LoadFile(os.path.join(PLUGIN_PATH['PREFS'], 'sngPrefs', 'SelectNGoTakesSaves', file))
    
    global AnimTakeClips
    AnimTakeClips = []

    for sim in jsonFileData['SelectNGoUserTakesData']:
        AnimTakeClips.append(DuplicateAnimClipTake(sim))
        AddAnimNodeClips(ui)
    
    return True


# [ ID Generator ]
def GuiIdGenerator(IDsList):
    """ ID Generator  """

    global uiIDS

    uiIDS+= 1

    ID = IDsList + uiIDS

    #print(ID)

    return ID