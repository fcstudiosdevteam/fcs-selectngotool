"""
Sat To G-Poly
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
import os 
import sys
import math 
import glob 
import time
import datetime
# FCS Imports Modules .py Files.
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib

config_data = None
#fcsID = cLib.fcsID
#jsonEdit = cLib.jsonEdit

opendlg = True

"""
Tool GUI Window User Interface Dialog.
_________________________________________________
"""
class MsgPopupMainWindow(cLib.BaseWindowDialogUI):
    """ MSG Popup GUI Window """
    
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Anim Clips Takes Exported..."
    windowDialogWidthSize = 200
    windowDialogHeightSize = 10
    """
    GUI Elements ID's.
    _________________________________________________
    """ 
    IDS_VER_ID = 1000
    UI_OVERALL_GRP_ID = 1001
    UI_CHK_ID = 1008
    UI_BTN_ID = 1009

    """
    Main GeDialog GUI Window Overrides.
    _________________________________________________
    """
    def __init__(self, msg, exportPath):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        super(MsgPopupMainWindow, self).__init__()
        self.msg = msg
        self.exportPath = exportPath

    def BuildUI(self):

        self.GroupBegin(self.UI_OVERALL_GRP_ID, c4d.BFH_SCALEFIT, 1, 0, "")

        self.GroupBorderSpace(3, 3, 3, 3)

        self.AddStaticText(0, c4d.BFH_CENTER, 0, 15, self.msg, c4d.BORDER_WITH_TITLE_BOLD)

        self.AddSeparatorH(0, c4d.BFH_SCALEFIT) # Separator
        
        self.GroupBegin(0, c4d.BFH_CENTER, 2, 0, "")
        self.AddButton(self.UI_BTN_ID, c4d.BFH_CENTER, 0, 15, name="OK")  
        self.AddButton(self.UI_CHK_ID, c4d.BFH_CENTER, 0, 15, name="Open Export Folder") 
        self.GroupEnd()

        self.GroupEnd()

        return super(MsgPopupMainWindow, self).BuildUI()

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        if id == self.UI_BTN_ID:
            self.Close()
            c4d.EventAdd()  

        if id == self.UI_CHK_ID:
            c4d.storage.ShowInFinder(os.path.join(self.exportPath), False)
            self.Close()
            c4d.EventAdd() 

        return True

