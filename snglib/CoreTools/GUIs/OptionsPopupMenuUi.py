
"""
Default Imports
_____________________________________________________________________
"""
import os 
import sys 
import subprocess 
import webbrowser 
import collections 
import math 
import random 
import urllib 
import glob 
import shutil 
import time
import datetime

"""
Cinema 4D Imports
_____________________________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils
from c4d.gui import GeDialog as dlg
from random import randint

from snglib.CoreTools.Utilites.Helpers import SelectNGoCoreLib
from snglib.CoreTools.GUIs import OptionsAndAnimClipWindowUi 
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
from snglib.CoreTools.Utilites.Helpers import JsonSysHelperLib as jsonEdit

enableDefaultList = True
enablePresetList  = False

windowUIdlg = None

def MenuPopupUI(global_strings, res_dlg, plug_PATH):
    global enableDefaultList
    global enablePresetList

    f = SelectNGoCoreLib.c4dSceneFilters 
    # Declare menu items IDs
    IDM_ITEM1 = c4d.FIRST_POPUP_ID
    IDM_ITEM2 = {'id':c4d.FIRST_POPUP_ID+1, 'str':'&i100004745& Enable Default List', 'IV':enableDefaultList }
    IDM_ITEM3 = {'id':c4d.FIRST_POPUP_ID+2, 'str':'&i100004745& Enable Presets List', 'IV':enablePresetList }

    # Main menu
    menu = c4d.BaseContainer()

    def addItemChecked(item):

        if item['IV'] == True:
            menu.InsData(item['id'], item['str']+'&c&')

        else:
            menu.InsData(item['id'], item['str'])

        return True

    menu.InsData(IDM_ITEM1, '&i12373&Options / AnimClips')
    menu.InsData(0, '') # Append separator    
    addItemChecked(item=IDM_ITEM2)
    addItemChecked(item=IDM_ITEM3)
    menu.InsData(0, '') # Append separator    

    result = c4d.gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    if result == IDM_ITEM1:
        global windowUIdlg
        print("Opening SNG Main Window")
        if windowUIdlg is None:
            windowUIdlg = OptionsAndAnimClipWindowUi.Select_N_Go_Dialog(global_strings, res_dlg, plug_PATH)
        return windowUIdlg.Open(c4d.DLG_TYPE_ASYNC, defaultw=200, defaulth=180)

    if result == IDM_ITEM2['id']:
        enableDefaultList=True
        enablePresetList=False

    if result == IDM_ITEM3['id']:
        enablePresetList=True
        enableDefaultList=False

    return True