import os
import sys
import subprocess
import webbrowser
import collections
import math
import random
import urllib
import glob
import shutil
import struct
import time
import array
import datetime
import weakref

# C4D Imports
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils

# Tool Imports
from snglib.CoreTools.Utilites import fcsCommonLibC4D as cLib
from snglib.CoreTools.GUIs import FormatPopupMenuUi
from snglib.CoreTools.GUIs import OptionsPopupMenuUi
from snglib.CoreTools.Utilites.Helpers import DirectoryHelperLib
DirHelper = DirectoryHelperLib.DirectoryHepler()

PLUGIN_PATH = None

# Exporting Files (C4D to FS .X file) (C4D to MCX (.dae file) ) Command Tool.
class CMD(c4d.plugins.CommandData):
    """
    `` An C4D Command Data Plugin Class ``
    Cinema 4D to Model Converter X (C4D to MCX) Tool.
    Export Object File as a ``COLLADA 1.4 (*.dae)`` to MCX.
    And Has a Gear Option Settings which is C4D2MCX Ui.
    _________________________________________________
    """ 

    def __init__(self, global_strings, res_dlg, plug_PATH):
        super(CMD, self).__init__()
        self.IDS = global_strings
        self.RESDLG = res_dlg

        global PLUGIN_PATH
        PLUGIN_PATH = plug_PATH
 
    """
    Main C4D GUI Dialog Operations Functions which 
    are Methods to Override for CommandData class.
    _________________________________________________
    """
    def Init(self, op):
        return True

    def Message(self, type, data):
        return True

    def Execute(self, doc):
        FormatPopupMenuUi.MenuPopupUI(PLUGIN_PATH)
        return True
    
    def ExecuteOptionID(self, doc, plugid, subid):
        OptionsPopupMenuUi.MenuPopupUI(self.IDS, self.RESDLG, PLUGIN_PATH)
        return True

 