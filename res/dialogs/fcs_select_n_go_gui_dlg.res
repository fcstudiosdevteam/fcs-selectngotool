DIALOG fcs_select_n_go_gui_dlg
{
    NAME ID_SNG_DIALOG_TITLE;
    SCALE_H;
    SCALE_V;
 
    GROUP ID_SNG_OVERALL_GRP
    {
        COLUMNS 1;
        SCALE_H;
        SCALE_V;

        TAB ID_SNG_TABS_GRP
        {
            SELECTION_TABS;
            SCALE_H;
            SCALE_V;

            GROUP ID_SNG_OPTIONS_TAB_GRP
            {
                COLUMNS 1;
                SCALE_H;
                SCALE_V;
                NAME ID_SNG_OPTIONS_TAB_GRP;

                TAB ID_SNG_OPTIONS_TABS
                {
                    SELECTION_TABS;
                    SCALE_H;
                    SCALE_V;


                    GROUP ID_SNG_EXPORT_OPTIONS_TAB_GRP
                    {
                        NAME ID_SNG_EXPORT_OPTIONS_TAB_GRP;
                        SCALE_H;
                        COLUMNS 1;


                        GROUP ID_SNG_OPT_MODE_SETTINGS_GRP
                        {
                            COLUMNS 1;
                            SCALE_H;
                            GROUP ID_SNG_OPT_MODE_TITLE_GRP
                            {
                                COLUMNS 1;
                                SCALE_H;
                            }

                        }

                        GROUP ID_SNG_OPT_FORMAT_SETTINGS_GRP
                        {
                            COLUMNS 1;
                            SCALE_H;

                            GROUP ID_SNG_OPT_FORMAT_SETTINGS_TITLE_GRP
                            {
                                COLUMNS 1;
                                SCALE_H;
                                BORDERSIZE 5,5,5,5;
                            }

                            GROUP 
                            {
                                COLUMNS 2;
                                SCALE_H;
                                BORDERSIZE 4,0,4,0;

                                COMBOBOX ID_SNG_OPT_FORMAT_DROPLIST_MENU
                                {
                                    SCALE_H;
                                    SIZE 210,10;
                                }

                                GROUP ID_SNG_OPT_FORMAT_GEAR_SETTINGS
                                {
                                    COLUMNS 1;
                                }

                            }

                        }

                        GROUP ID_SNG_OPT_OPTIONS_GRP
                        {
                            COLUMNS 1;
                            SCALE_H;
                            BORDERSIZE 5,5,5,5;

                            GROUP ID_SNG_OPT_OPTIONS_TITLE_GRP
                            {
                                COLUMNS 1; SCALE_H;
                            }

                            GROUP 
                            {
                                COLUMNS 1;
                                BORDERSIZE 5,5,5,5;
                                SCALE_H;

                                GROUP ID_SNG_OPT_OPTIONS_EXPORT_GRP
                                {
                                    COLUMNS 1;

                                    //CHECKBOX ID_SNG_OPT_CHK_UNITY_AXIS
                                    //{
                                    //    NAME ID_SNG_OPT_CHK_UNITY_AXIS;
                                    //}

                                    CHECKBOX ID_SNG_OPT_CHK_EXPORT_SEPARATE
                                    {
                                        NAME ID_SNG_OPT_CHK_EXPORT_SEPARATE;
                                    }

                                    CHECKBOX ID_SNG_OPT_CHK_CENTER_OBJ
                                    {
                                        NAME ID_SNG_OPT_CHK_CENTER_OBJ;
                                    }

                                    CHECKBOX ID_SNG_OPT_CHK_OPENFOLDER
                                    {
                                        NAME ID_SNG_OPT_CHK_OPENFOLDER;
                                    }

                                    CHECKBOX ID_SNG_OPT_CHK_EXPORT_POPUP
                                    {
                                        NAME ID_SNG_OPT_CHK_EXPORT_POPUP;
                                    }                                 

                                }
                            }
                        }

                        SEPARATOR 
                        {
                            SCALE_H;
                        }

                        GROUP ID_SNG_OPT_PATH_GRP
                        {
                            COLUMNS 1; SCALE_H;

                            GROUP ID_SNG_OPT_PATH_TITLE
                            {
                                COLUMNS 1; SCALE_H;
                            }

                            GROUP ID_SNG_OPT_PATH_PANEL_GRP
                            {
                                COLUMNS 4;
                                SCALE_H;
                                BORDERSIZE 2,2,2,2;

                                STATICTEXT ID_SNG_OPT_PATH_TXT
                                {
                                    NAME ID_SNG_OPT_PATH_TXT;
                                    ALIGN_LEFT;
                                }

                                EDITTEXT ID_SNG_OPT_PATH_EDITBOX
                                {
                                    SCALE_H;
                                    SIZE 250,0;
                                }

                                GROUP ID_SNG_OPT_PATH_RF_BTN
                                {
                                    COLUMNS 1;
                                }

                                GROUP ID_SNG_OPT_PATH_DIR_BTN_GRP
                                {
                                    COLUMNS 1;
                                }
                            }
                        }
                    }


                    GROUP ID_SNG_IMPORT_OPTIONS_TAB_GRP
                    {
                        NAME ID_SNG_IMPORT_OPTIONS_TAB_GRP;
                        SCALE_H;
                        SCALE_V;
                        COLUMNS 1;

                        CHECKBOX ID_SNG_OPT_CHK_BATCH_IMPORT_WMAT
                        {
                            NAME ID_SNG_OPT_CHK_BATCH_IMPORT_WMAT;
                        }

                        CHECKBOX ID_SNG_OPT_CHK_BATCH_IMPORT
                        {
                            NAME ID_SNG_OPT_CHK_BATCH_IMPORT;
                        }


                        GROUP ID_SNG_OPT_BATCH_IMPORT_GRP
                        {
                            COLUMNS 1;
                            SCALE_H;
                            SCALE_V;
                            BORDERSTYLE BORDER_IN;
                            BORDERSIZE 2,1,2,1;

                            GROUP ID_SNG_OPT_BATCH_TITLE_GRP
                            {
                                COLUMNS 1;
                                SCALE_H;
                            }

                            GROUP ID_SNG_OPT_BATCH_DIR_GRP
                            {
                                COLUMNS 3;
                                SCALE_H;

                                STATICTEXT ID_SNG_OPT_BATCH_TXT
                                {
                                    NAME ID_SNG_OPT_BATCH_TXT;
                                    ALIGN_LEFT;
                                }

                                EDITTEXT ID_SNG_OPT_BATCH_EDITBOX
                                {
                                    SCALE_H;
                                    SIZE 250,0;
                                }

                                GROUP ID_SNG_OPT_BATCH_DIR_BTN_GRP
                                {
                                }

                            }

                            GROUP ID_SNG_OPT_BATCH_SEARCH_GRP
                            {
                                COLUMNS 4;
                                SCALE_H;

                                EDITTEXT ID_SNG_OPT_SEARCH_EDITBOX
                                {
                                    SCALE_H;
                                    SIZE 250,0;
                                }

                                STATICTEXT ID_SNG_OPT_SEARCH_TXT
                                {
                                    NAME ID_SNG_OPT_SEARCH_TXT;
                                    ALIGN_LEFT;
                                }

                                COMBOBOX ID_SNG_OPT_BATCHFITER_MENU
                                {
                                    SCALE_H;
                                    SIZE 70,10;
                                }

                            }
                    

                            GROUP ID_SNG_OPT_BATCH_LIST_GRP
                            {
                                COLUMNS 1;
                                SCALE_H;
                                SCALE_V;
                                SPACE 1,1;

                                SEPARATOR 
                                {
                                    SCALE_H;
                                }

                                GROUP 
                                {
                                    COLUMNS 3;
                                    SCALE_H;
                                    EQUAL_COLS;

                                    CHECKBOX ID_SNG_OPT_BATCH_CHK_ALL
                                    {
                                        ALIGN_LEFT;
                                    }

                                    STATICTEXT ID_SNG_OPT_BATCH_FILENAME_TXT
                                    {
                                        SCALE_H;
                                        NAME ID_SNG_OPT_BATCH_FILENAME_TXT;
                                        ALIGN_RIGHT;
                                    }

                                    STATICTEXT ID_SNG_OPT_BATCH_FILETYPE_TXT
                                    {
                                        SCALE_H;
                                        NAME ID_SNG_OPT_BATCH_FILETYPE_TXT;
                                        ALIGN_RIGHT;
                                        SIZE 120,10;
                                    }
                                } 


                                SEPARATOR 
                                {
                                    SCALE_H;
                                }

                                SCROLLGROUP 
                                {
                                    SCROLL_H;
                                    SCROLL_V;
                                    SIZE 0,100;
                                    SCALE_H;
                                    SCALE_V;

                                    GROUP ID_SNG_OPT_BATCH_LIST
                                    {
                                        COLUMNS 1;
                                        SCALE_H;
                                        SCALE_V;
                                    }

                                } //End of SCROLLGROUP 
                            } // Enfd of ID_SNG_OPT_BATCH_LIST_GRP
                        } // End of Batch Group
                    } // End of ID_SNG_IMPORT_OPTIONS_TAB_GRP 
                } // End of

                SEPARATOR 
                {
                    SCALE_H;
                }

                BUTTON ID_SNG_OPT_EXPORT_IMPORT_BTN
                {
                    SCALE_H;
                    NAME ID_SNG_OPT_EXPORT_IMPORT_BTN;
                    SIZE 190,15;
                }
            }

            // New Tab for Takes
            GROUP ID_SNG_TAKES_TAB_GRP
            {
                NAME ID_SNG_TAKES_TAB_GRP;
                SCALE_H;
                COLUMNS 1;
            }
        } 

        GROUP ID_SNG_BOTTOM_GRP
        {

                SCALE_H;
                COLUMNS 1;

                GROUP ID_SNG_STATUS_BAR_GRP
                {
                    COLUMNS 1;
                    SCALE_H;
                }

                GROUP ID_SNG_FCSWEBBANNER
                {
                    COLUMNS 1;
                    SCALE_H;
                }
        }
        
    }
}
